# bugs/trucs à modifier

  * faire en sorte qu'un Being est un attribut d'icône (simplifie la
    prog. de PartyWindow par exemple)

  * dans la fenêtre credits quand on clique sur un lien la fenêtre du
    navigateur s'ouvre plusieurs fois (probablement un bug dans pygwin)

  * parfois les items sont dupliqués à la fin d'un combat

  * plateau non réinitialisé quand on change le leader de l'équipe

  * quand on laisse une touche de mouvement appuyée en mode
    exploration, le processus bouffe tout le CPU

  * le combat ne se termine pas quand le dernier héros meurt

  * quand on change l'apparence d'un héros quand on crée une équipe,
    et qu'on ferme la fenêtre, il faut qu'un événement survienne pour
    que l'icone du héros soit mise à jour

  * utiliser pg.key.set_repeat fait planter les combats


#  trucs à implémenter/réflechir

  * possibilité de changer le leader de l'équipe

  * refaire la fenêtre de comparaison de deux équipements

  * boucliers

  * esquive

  * clés

  * forme basique d'IA pour les ennemis, basée sur un ensemble de
    stratégies prédéfinies
