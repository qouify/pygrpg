#!/usr/bin/env python3

import typing as tp
import pygame as pg
import pygwin as pw
if tp.TYPE_CHECKING:
    from pygrpg.action import Action, Use, Cast, Attack
    from pygrpg.area import Area, AreaObject, Object, ItemObject
    from pygrpg.attr import Attr, HasAttrs
    from pygrpg.being import BeingAttr, Race, Dressing, DressingSlot
    from pygrpg.being import Being, Monster
    from pygrpg.being import Hero, Group, Status
    from pygrpg.campaign import Campaign
    from pygrpg.config import Config
    from pygrpg.context import Context, FightContext, AreaContext
    from pygrpg.effect import Effect, StatusChange, StatusChangeType
    from pygrpg.expr import Expr, Range
    from pygrpg.game import Game, Save
    from pygrpg.gui import AreaEditorNode, AreaGameNode, ShellInputText
    from pygrpg.gui import AreaNode, Main, MainEditor, MainGame, AreaPanel
    from pygrpg.item import EquipmentSlot, ItemAttr, Equipment, Item, Inventory
    from pygrpg.item import ItemAttack, ItemDefense, ItemEquip, ItemUse
    from pygrpg.item import ItemCharacteristics
    from pygrpg.media import Image, MediaSource
    from pygrpg.resource import Resource, Text
    from pygrpg.rule import Rule
    from pygrpg.skill import Skill, SkillCategory
    from pygrpg.spell import Spell, SpellAttr
    from pygrpg.util import Completer
else:
    #  dummy type definitions
    Action = int
    Area = int
    AreaContext = int
    AreaEditorNode = int
    AreaGameNode = int
    AreaNode = int
    AreaObject = int
    AreaPanel = int
    Attack = int
    Attr = int
    BeingAttr = int
    Being = int
    Campaign = int
    Cast = int
    Completer = int
    Config = int
    Context = int
    Dressing = int
    DressingSlot = int
    Effect = int
    Equipment = int
    EquipmentSlot = int
    Expr = int
    FightContext = int
    Game = int
    Group = int
    HasAttrs = int
    Hero = int
    Image = int
    Inventory = int
    Item = int
    ItemAttack = int
    ItemAttr = int
    ItemCharacteristics = int
    ItemDefense = int
    ItemEquip = int
    ItemObject = int
    ItemUse = int
    Object = int
    Main = int
    MainEditor = int
    MainGame = int
    MediaSource = int
    Monster = int
    Race = int
    Range = int
    Resource = int
    Rule = int
    Save = int
    ShellInputText = int
    Skill = int
    SkillCategory = int
    Spell = int
    SpellAttr = int
    Status = int
    StatusChange = int
    StatusChangeType = int
    Text = int
    Use = int

#  define a type for each class
action_t = Action
area_t = Area
area_context_t = AreaContext
area_editor_node_t = AreaEditorNode
area_game_node_t = AreaGameNode
area_node_t = AreaNode
area_object_t = AreaObject
area_panel_t = AreaPanel
attack_t = Attack
attr_t = Attr
being_t = Being
being_attr_t = BeingAttr
campaign_t = Campaign
cast_t = Cast
completer_t = Completer
config_t = Config
context_t = Context
dressing_t = Dressing
dressing_slot_t = DressingSlot
effect_t = Effect
equipment_t = Equipment
equipment_slot_t = EquipmentSlot
expr_t = Expr
fight_context_t = FightContext
game_t = Game
group_t = Group
has_attrs_t = HasAttrs
hero_t = Hero
image_t = Image
inventory_t = Inventory
item_t = Item
item_attack_t = ItemAttack
item_attr_t = ItemAttr
item_characteristics_t = ItemCharacteristics
item_defense_t = ItemDefense
item_equip_t = ItemEquip
item_object_t = ItemObject
item_use_t = ItemUse
main_t = Main
main_editor_t = MainEditor
main_game_t = MainGame
media_source_t = MediaSource
monster_t = Monster
object_t = Object
race_t = Race
range_t = Range
resource_t = Resource
rule_t = Rule
save_t = Save
shell_input_text_t = ShellInputText
skill_t = Skill
skill_category_t = SkillCategory
spell_t = Spell
spell_attr_t = SpellAttr
status_t = Status
status_change_t = StatusChange
status_change_type_t = StatusChangeType
text_t = Text
use_t = Use

expr_result_t = tp.Any
assignment_t = tp.Dict[str, expr_result_t]
expr_eval_fun_t = tp.Callable[[assignment_t], expr_result_t]
int_couple_t = tp.Tuple[int, int]
point_t = int_couple_t
cell_t = int_couple_t
cell_list_t = tp.List[cell_t]
direction_t = int_couple_t
size_t = int_couple_t
pos_t = int_couple_t
rect_t = tp.Tuple[int, int, int, int]
color_t = tp.Union[
    tp.Tuple[int, int, int, int],
    tp.Tuple[int, int, int]
]
serialisation_t = tp.Dict[str, tp.Any]
custom_completer_t = tp.Callable[[str, str], tp.Iterable[str]]
pickup_src_t = tp.Literal['chest', 'ground']
cell_predicate_t = tp.Callable[[cell_t], bool]
area_object_predicate_t = tp.Callable[[area_object_t], bool]
link_t = tp.Callable[[], bool]
pg_handler_t = tp.Callable[[pg.event.Event], bool]
game_mode_t = tp.Literal['editor', 'game']
game_init_location_t = tp.Tuple[str, cell_t]
check_level_t = tp.Literal['ok', 'warning', 'error']
check_msg_t = tp.Tuple[check_level_t, str]
media_type_t = tp.Literal['sound', 'image']

#  action
action_effect_t = tp.Tuple[being_t, effect_t]

#  area
area_location_t = str
area_target_t = tp.Tuple[str, str]

#  being
being_attrs_t = tp.Dict[being_attr_t, int]
being_skills_t = tp.Dict[skill_t, int]
being_spells_t = tp.Set[spell_t]
being_xps_t = tp.Dict[skill_t, int]
being_effect_t = tp.Tuple[tp.Optional[resource_t], effect_t]
being_effects_t = tp.List[being_effect_t]

#  equipment
equipment_items_t = tp.Dict[equipment_slot_t, item_t]

#  hero
hero_xps_t = tp.Dict[skill_t, int]
hero_dressings_t = tp.Dict[dressing_slot_t, dressing_t]
hero_sex_t = tp.Literal['male', 'female']

#  item
item_attrs_t = tp.Dict[item_attr_t, int]

#  race
race_attrs_t = tp.Dict[being_attr_t, int]

#  resource
resource_exprs_getter_t = tp.Iterator[tp.Tuple[expr_t, tp.Set[str]]]

#  spell
spell_attrs_t = tp.Dict[spell_attr_t, int]
spell_card_t = tp.Literal['all', 'one']
spell_effects_t = tp.List[effect_t]
spell_requirements_t = tp.List[expr_t]
spell_target_t = tp.Literal['hero', 'enemy', 'any']
spell_when_t = tp.Literal['fight', 'exploration', 'any']
spell_where_t = tp.Literal['area', 'any']

#  status change
status_change_op_t = tp.Literal['add', 'rem']

#  cast
cast_area_t = tp.Set[cell_t]
cast_targets_t = tp.Set[being_t]

#  use
use_src_t = tp.Literal['equipment', 'inventory']

#  fight
fight_xps_t = tp.Dict[hero_t, tp.Dict[skill_t, int]]
fight_levels_t = tp.Dict[hero_t, tp.Dict[skill_t, int]]

#  area panel
area_panel_resource_selection_callback_t = tp.Callable[[resource_t], None]

# to define
# effect_src_t = source of an effect

tmp_hero_t = tp.TypedDict('tmp_hero_t', {
    'name': str,
    'race': race_t,
    'sex': hero_sex_t,
    'attrs': tp.Dict[being_attr_t, int],
    'attrs_base': tp.Dict[being_attr_t, int],
    'skills': tp.Dict[skill_t, int],
    'pt_skills': int,
    'pt_attrs': int,
    'pt_attrs_init': int,
    'max_name_len': int,
    'dressings': tp.Dict[dressing_slot_t, tp.Optional[dressing_t]],
    'icon': pg.surface.Surface
}, total=False)

campaign_attr_t = tp.TypedDict('campaign_attr_t', {
    'attr-points': int,
    'heroes': int,
    'max-hero-name-len': int,
    'skill-points': int,
    'start-area': str,
    'start-location': str
}, total=False)

game_meta_data_t = tp.TypedDict('game_meta_data_t', {
    'id': str,
    'name': str,
    'version': str,
    'author': str
}, total=False)


__all__ = [
    'pg',
    'pw'
]
