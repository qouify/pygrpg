#!/usr/bin/env python3

"""Definition of class Skill."""

import typing as tp

from pygrpg.typing import types
from pygrpg.resource import Resource


class Skill(Resource):
    """Document this."""

    def __init__(
            self,
            rid: str,
            attr: types.being_attr_t,
            category: types.skill_category_t
    ):
        """Initialise self.

        Argument attr is the BeingAttr used for the skill.

        Argument category is the skill's category.

        """
        self.attr: types.being_attr_t
        self.category: types.skill_category_t

        Resource.__init__(self, rid)
        self.attr = attr
        self.category = category

    def __lt__(self, other: tp.Any) -> bool:
        if isinstance(other, Skill):
            left = self.category, self.order, self.id
            right = other.category, other.order, other.id
            return left < right
        return Resource.__lt__(self, other)

    ###########################################################################

    @classmethod
    def gui_label_style_class(cls) -> tp.Optional[str]:
        return 'skill_name'
