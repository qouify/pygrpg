#!/usr/bin/env python3

"""Definition of class Game."""

import os
import typing as tp
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.campaign import Campaign
from pygrpg.context import ExplorationContext, FightContext, PreFightContext
from pygrpg.util import Io
from pygrpg.area import Area
from pygrpg.being import Hero
from . import Data


class Game:
    """Document this."""

    def __init__(
            self,
            campaign: types.campaign_t,
            party: 'types.group_t[types.hero_t]',
            init_location: tp.Optional[types.game_init_location_t] = None
    ) -> None:
        self.area: types.area_t
        self.campaign: types.campaign_t
        self.ctx: types.context_t
        self.init_location: types.game_init_location_t
        self.known_areas: tp.Set[str]
        self.known_cells: tp.Set[types.cell_t]
        self.party: 'types.group_t[types.hero_t]'

        self.campaign = campaign
        self.party = party
        if init_location is not None:
            self.init_location = init_location
        self.known_areas = set()
        self.known_cells = set()

    def has_ctx(self) -> bool:
        """Check if a context is opened."""
        return hasattr(self, 'ctx')

    def new(self) -> None:
        """Start self (game is a new one)."""
        area_id = self.campaign.attrs['start-area']
        location = self.campaign.attrs['start-location']
        self.enter_area(area_id, location=location)
        self.start_exploration()

    def start(self) -> None:
        """Start self."""
        assert self.init_location is not None
        area_id, cell = self.init_location
        self.enter_area(area_id, cell=cell)
        self.start_exploration()

    def close(self) -> None:
        """Close self."""
        if self.has_ctx():
            self.ctx.close()
        Area.init_content_all()
        glob.main().close_area()

    def start_exploration(self) -> None:
        """Enter in an exploration context with self.area."""
        self.switch_context(ExplorationContext(self.area))

    def switch_context(self, ctx: types.context_t) -> None:
        """Switch self's current context.

        self's current context is closed and ctx is opened.

        """
        if self.has_ctx():
            self.ctx.close()
        self.ctx = ctx
        ctx.open()

    def not_saved(self) -> bool:
        """Check if self has not been saved."""
        return True

    def in_fight(self) -> bool:
        """Check if self's current context is a fight context."""
        return (
            self.has_ctx() and
            isinstance(self.ctx, (FightContext, PreFightContext))
        )

    def add_known_cell(self, cell: types.cell_t) -> None:
        """Add cell to the set of known cells (in the current area)."""
        self.known_cells.add(cell)

    def is_cell_known(self, cell: types.cell_t) -> bool:
        """Check if cell is known (in the current area)."""
        return cell in self.known_cells

    def add_known_area(self, area_id: str) -> None:
        """Add area_id to the set of known areas."""
        self.known_areas.add(area_id)

    def leave_area(self) -> None:
        """Leave the current area.

        The party is removed from the current area and this one is
        unloaded.

        """
        self.area.del_object(self.party.leader)
        self.write_current_area(Data.get_run_area_file(self.area.id))
        self.area.init_content()

    def enter_area(
            self,
            area_id: str,
            location: tp.Optional[types.area_location_t] = None,
            cell: tp.Optional[types.cell_t] = None
    ) -> None:
        """Enter area area_id.

        The party is put at the given location (if not None) or
        otherwise at the given cell.  Either location or cell must not
        be None.

        """
        assert location is not None or cell is not None
        self.known_cells = set()
        self.area = Area.get_(area_id)
        area_id = self.area.id
        self.area.load_content(
            load_dynamic=area_id not in self.known_areas
        )
        if area_id in self.known_areas:
            self.read_current_area(
                Data.get_run_area_file(self.area.id)
            )
        else:
            self.add_known_area(area_id)
        if location is not None:
            cell = self.area.get_location_cell(location)
        assert cell is not None
        self.area.add_object(cell, self.party.leader)
        glob.main().set_area(self.area)

    def write_current_area(self, area_file: str) -> None:
        """Write dynamic content of the current area in file area_file.

        The dynamic content consists of known cells and the list of
        dynamic objects currently in the area.

        """
        data = {
            'cells': list(self.known_cells),
            'objects': self.area.serialise_objects(
                lambda obj: obj.is_dynamic() and not isinstance(obj, Hero)
            )
        }
        pw.Util.save_json_file(area_file, data)

    def read_current_area(self, area_file: str) -> None:
        """Read dynamic content of the current area from file area_file."""
        dict_t = tp.TypedDict('dict_t', {
            'cells': tp.List[types.cell_t],
            'objects': tp.List[types.serialisation_t]
        })
        data = tp.cast(dict_t, pw.Util.load_json_file(area_file))
        cells = data['cells']
        for cell in cells:
            self.add_known_cell((cell[0], cell[1]))
        self.area.unserialise_objects(data['objects'])

    def write(self, data_dir: str) -> None:
        """Write data of self in directory data_dir.

        Files written are:

        * {data_dir}/data.json => main data for the game (with party
             data, id of the compaign, id of the current area, ...)
        * {data_dir}/areas/area-1.json => dynamic content for an area
           ...
        * {data_dir}/areas/area-n.json

        There is one area-x.json file for each known area.

        """
        data = {
            'campaign': self.campaign.id,
            'leader': self.party.leader.serialise(),
            'area': self.area.id,
            'location': self.party.leader.cell,
            'known_areas': list(self.known_areas)
        }
        pw.Util.save_json_file(os.path.join(data_dir, 'data.json'), data)
        areas_dir = os.path.join(data_dir, 'areas')
        Io.mkdir_if_missing(areas_dir)
        for area_id in self.known_areas:
            area_file = os.path.join(areas_dir, area_id + '.json')
            if area_id != self.area.id:
                Io.cp(Data.get_run_area_file(area_id), area_file)
            else:
                self.write_current_area(area_file)

    @classmethod
    def read(cls, data_dir: str) -> types.game_t:
        """Read game data from data_dir and return the corresponding Game."""
        dict_t = tp.TypedDict("dict_t", {
            'campaign': str,
            'leader': types.serialisation_t,
            'area': str,
            'location': tp.Tuple[int, int],
            'known_areas': tp.Iterable[str]
        })
        d = pw.Util.load_json_file(os.path.join(data_dir, 'data.json'))
        data = tp.cast(dict_t, d)
        campaign = Campaign.get_(data['campaign'])
        loc_cell = (data['location'][0], data['location'][1])
        leader = Hero.unserialise(data['leader'])
        result = Game(
            campaign,
            leader.group,
            init_location=(data['area'], loc_cell)
        )
        areas_dir = os.path.join(data_dir, 'areas')
        for area_id in data['known_areas']:
            result.add_known_area(area_id)
            area_file = os.path.join(areas_dir, area_id + '.json')
            Io.cp(area_file, Data.get_run_area_file(area_id))
        return result
