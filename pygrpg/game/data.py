#!/usr/bin/env python3

"""Definition of class Data."""

import typing as tp

import os
import tempfile

from pygrpg.util import Zip, Io
from pygrpg.paths import Paths
from pygrpg.typing import types
from . import Save


class Data:
    """Document this."""

    @classmethod
    def quit(cls) -> None:
        """Document this."""
        Io.rmtree(Paths['dir-run-pid'])

    @classmethod
    def get_run_area_file(cls, area_id: str) -> str:
        """Document this."""
        return os.path.join(Paths['dir-run-areas'], area_id + '.json')

    @classmethod
    def get_save_list(cls) -> tp.List[types.save_t]:
        """Document this."""
        save_dir: str = Paths['dir-saves']
        result = []
        Io.mkdir_if_missing(save_dir)
        for d in os.listdir(save_dir):
            fd = os.path.join(save_dir, d)
            mdata = os.path.join(fd, Save.MDATA_FILE)
            if os.path.isdir(fd) and os.path.isfile(mdata):
                save = Save.read(mdata)
                result.append(save)
        result = sorted(result, key=lambda save: save.date, reverse=True)
        return result

    @classmethod
    def new_save(cls, name: str, game: types.game_t) -> types.save_t:
        """Document this."""
        num = 1
        found = False
        while not found:
            fd = os.path.join(Paths['dir-saves'], str(num).zfill(4))
            if not os.path.exists(fd):
                found = True
            else:
                num += 1
        result = Save(str(num).zfill(4), name, game)
        return result

    @classmethod
    def write_save(cls, save: types.save_t) -> None:
        """Document this."""
        Io.mkdir_if_missing(save.get_dir())
        tmp_dir = tempfile.mkdtemp()
        save_dir = save.write(tmp_dir)
        Zip.zip_directory(save.get_data_file(), save_dir)
        Io.rmtree(tmp_dir)

    @classmethod
    def load_game(cls, save: types.save_t) -> tp.Optional[types.game_t]:
        """Document this."""
        data_file = save.get_data_file()
        tmp_dir = tempfile.mkdtemp()
        try:
            Zip.unzip_file(data_file, tmp_dir)
        except FileNotFoundError:
            return None
        save.read_game(tmp_dir)
        Io.rmtree(tmp_dir)
        return save.game
