#!/usr/bin/env python3

"""Definition of class Save."""

import typing as tp
import os
import datetime
import dateutil.parser
import pygwin as pw

from pygrpg.typing import types
from pygrpg.util import Io
from pygrpg.paths import Paths


class Save:
    """Document this."""

    MDATA_FILE = 'mdata.json'

    def __init__(
            self,
            sid: str,
            name: str,
            game: tp.Optional[types.game_t] = None,
            date: tp.Optional[datetime.datetime] = None
    ):
        """Initialise self."""
        self.date: datetime.datetime
        self.game: types.game_t
        self.id: str
        self.name: str

        self.id = sid
        self.name = name
        if game is not None:
            self.game = game
        self.date = datetime.datetime.now() if date is None else date

    def get_dir(self) -> str:
        """Get the path of self's directory."""
        return os.path.join(Paths['dir-saves'], str(self.id).zfill(4))

    def get_meta_data_file(self) -> str:
        """Get the json file containing self's meta data."""
        return os.path.join(self.get_dir(), Save.MDATA_FILE)

    def get_data_file(self) -> str:
        """Get the zip file containing self's data."""
        return os.path.join(self.get_dir(), 'data.zip')

    def delete(self) -> None:
        """Delete self from disk."""
        Io.rmtree(self.get_dir())

    def _write_meta_data(self) -> None:
        pw.Util.save_json_file(self.get_meta_data_file(), {
            'id': self.id,
            'name': self.name,
            'date': self.date.strftime('%Y/%m/%d %H:%M')
        })

    def rename(self, new_name: str) -> None:
        """Rename self."""
        self.name = new_name
        self._write_meta_data()

    def write(self, tmp_dir: str) -> str:
        """Write self."""
        self.date = datetime.datetime.now()
        self._write_meta_data()
        assert self.game is not None
        result = os.path.join(tmp_dir, 'data')
        Io.mkdir_if_missing(result)
        self.game.write(result)
        return result

    def read_game(self, data_dir: str) -> None:
        """Read game data of self."""
        from . import Game
        data_dir = os.path.join(data_dir, 'data')
        self.game = Game.read(data_dir)

    @classmethod
    def read(cls, mdata_file: str) -> types.save_t:
        """Return the Save object described by mdata_file."""
        td = tp.cast(
            tp.Dict[str, str], pw.Util.load_json_file(mdata_file)
        )
        result = Save(
            td['id'],
            td['name'],
            date=dateutil.parser.parse(td['date'])
        )
        return result
