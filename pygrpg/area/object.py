#!/usr/bin/env python3

"""Definition of class Object."""

import typing as tp

from pygrpg.resource import Resource
from pygrpg.item import Inventory
from pygrpg.typing import types
from pygrpg.interfaces import Instanciable
from . import AreaObject


class Object(AreaObject, Resource, Instanciable):
    """An Object is any static object that can appear on the Area."""

    HAS_TEXT = False
    HAS_ICON = True

    blocking: bool
    chest: bool
    depth: int
    door: bool
    inventory: types.inventory_t
    opaque: bool
    opened: bool
    walkable: bool

    def __init__(
            self,
            rid: str,
            depth: int,
            opaque: bool,
            walkable: bool,
            blocking: bool,
            door: bool,
            chest: bool
    ):
        """Document this."""
        AreaObject.__init__(self)
        Resource.__init__(self, rid)
        self.depth = depth
        self.opaque = opaque
        self.walkable = walkable
        self.blocking = blocking
        self.door = door
        self.chest = chest
        self.opened = False

    def has_inventory(self) -> bool:
        """Check if self has an inventory."""
        return hasattr(self, 'inventory')

    def get_depth(self) -> int:
        """Get self's depth."""
        return self.depth

    def get_image_files(self) -> tp.Iterator[str]:
        """Document this."""
        if self.opened:
            yield self.get_image_file_(media='open')
        else:
            yield self.get_image_file_()

    def is_blocking(self) -> bool:
        """Document this."""
        if self.walkable or (self.door and self.opened):
            return False
        return self.blocking

    def is_dynamic(self) -> bool:
        """Check if self is dynamic (a door or a chest)."""
        return self.door or self.chest

    def is_openable(self) -> bool:
        """Document this."""
        return self.door or self.chest

    def is_opened(self) -> bool:
        """Document this."""
        return self.opened is not None and self.opened

    def change_state(self) -> None:
        """Document this."""
        self.opened = not self.opened

    def serialise(self) -> types.serialisation_t:
        result: types.serialisation_t = {
            **super().serialise(),
            'id': self.id
        }
        if self.is_openable():
            result['opened'] = self.opened
        if self.has_inventory():
            result['inventory'] = self.inventory.serialise()
        return result

    @classmethod
    def unserialise(cls, data: types.serialisation_t) -> types.object_t:
        result: types.object_t = Object.get_(data['id']).instanciate()
        if 'opened' in data:
            result.opened = data['opened']
        if 'inventory' in data:
            result.inventory = Inventory.unserialise(data['inventory'])
        return result

    def instanciate(self) -> types.object_t:
        result: types.object_t
        if not self.is_dynamic():
            result = self
        else:
            result = Object(
                rid=self.id,
                depth=self.depth,
                opaque=self.opaque,
                walkable=self.walkable,
                blocking=self.blocking,
                door=self.door,
                chest=self.chest
            )
            if self.is_chest():
                result.inventory = Inventory()
        return result

    ###########################################################################

    def gui_editor_tooltip(
            self,
            **kwargs: tp.Any
    ) -> tp.Optional[types.pw.Node]:
        if self.is_chest():
            return self.inventory.gui_editor_tooltip()
        return None
