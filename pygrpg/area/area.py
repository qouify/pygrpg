#!/usr/bin/env python3

"""Definition of class Area."""

import os
import typing as tp
import pygwin as pw

from pygrpg.resource import Resource
from pygrpg.util import Directions, Io, Bresenham
from pygrpg.campaign import Campaign
from pygrpg.item import Inventory
from pygrpg.typing import types
from . import ItemObject, AreaObject, Object


class Area(Resource):
    """An Area is like a game board.

    Each area is attached to a campaign.

    It consists of a 2D grid where each cell is identified by its
    (i,j) coordinates.  Each cell can contains AreaObject objects.

    Some cells can have an associated location (a string).

    Some cells can have a target which is a couple (area_id, loc).
    This means that if the party moves to this cell, then it is
    "teleported" to area area_id on the cell that has location loc.

    """

    campaign: types.campaign_t
    cells: tp.Dict[types.cell_t, tp.List[types.area_object_t]]
    heroes: tp.Set[types.hero_t]
    loaded: bool
    locations: tp.Dict[types.cell_t, str]
    monsters: tp.Set[types.monster_t]
    targets: tp.Dict[types.cell_t, types.area_target_t]

    def __init__(self, rid: str, campaign: Campaign):
        Resource.__init__(self, rid)
        self.campaign = campaign
        self.loaded = False
        self.init_content()

    def init_content(self) -> None:
        """Initialise self's content (empty all its content)."""
        self.loaded = False
        self.cells = dict()
        self.locations = dict()
        self.targets = dict()
        self.heroes = set()
        self.monsters = set()

    def get_center_cell(self) -> types.cell_t:
        """Get the coordinates of the cell that is at self's center."""
        ir: tp.Tuple[int, int]
        jr: tp.Tuple[int, int]
        first = True
        for i, j in self.cells:
            if first:
                ir = i, i
                jr = j, j
                first = False
            else:
                ir = min(ir[0], i), max(ir[1], i)
                jr = min(jr[0], j), max(jr[1], j)
        if first:
            return 0, 0
        return (int((ir[0] + ir[1]) / 2), int((jr[0] + jr[1]) / 2))

    def _get_content_file(self) -> str:
        return os.path.join(self.get_content_dir(), 'data.json')

    def get_heroes(self) -> tp.Iterator[types.hero_t]:
        """Generate all the heroes in the area."""
        yield from self.heroes

    def get_monsters(self) -> tp.Iterator[types.monster_t]:
        """Generate all monsters in the area."""
        yield from self.monsters

    def add_object(self, cell: types.cell_t, obj: types.area_object_t) -> None:
        """Add object obj in self at the given cell."""
        from pygrpg.being import Hero, Monster
        self.cells.setdefault(cell, list())
        self.cells[cell] = [
            o for o in self.cells[cell]
            if o.get_depth() != obj.get_depth()
        ]
        self.cells[cell].append(obj)
        self.cells[cell].sort(key=lambda o: o.get_depth())
        obj.set_cell(cell)
        if isinstance(obj, Hero):
            self.heroes.add(obj)
        elif isinstance(obj, Monster):
            self.monsters.add(obj)

    def del_object(
            self,
            obj: types.area_object_t,
            cell: tp.Optional[types.cell_t] = None
    ) -> None:
        """Delete object obj from self."""
        from pygrpg.being import Hero, Monster
        if cell is None:
            cell = obj.get_cell()
        obj.set_cell(None)
        assert cell is not None
        self.cells[cell] = [o for o in self.cells[cell] if obj != o]
        if self.cells[cell] == []:
            del self.cells[cell]
        if isinstance(obj, Hero):
            self.heroes.remove(obj)
        elif isinstance(obj, Monster):
            self.monsters.remove(obj)

    def get_objects(
            self,
            cell: types.cell_t
    ) -> tp.Iterator[types.area_object_t]:
        """Generate all objects of self at the given cell."""
        yield from self.cells.get(cell, [])

    def clear_cell(self, cell: types.cell_t) -> None:
        """Delete all objects from self at the given cell."""
        for obj in self.get_objects(cell):
            self.del_object(obj, cell=cell)

    def has_objects(self, cell: types.cell_t) -> bool:
        """Check if some objects are present in self at the given cell."""
        return cell in self.cells

    def _get_object(
            self,
            cell: types.cell_t,
            pred: types.area_object_predicate_t
    ) -> tp.Optional[types.area_object_t]:
        return next((o for o in self.get_objects(cell) if pred(o)), None)

    def get_blocking_object(
            self,
            cell: types.cell_t
    ) -> tp.Optional[types.area_object_t]:
        """Get the blocking object at the given cell of self."""
        return self._get_object(cell, lambda o: o.is_blocking())

    def get_being(
            self,
            cell: types.cell_t
    ) -> tp.Optional[types.being_t]:
        """Get the being at the given cell of self."""
        from pygrpg.being import Being
        result = self._get_object(cell, lambda o: isinstance(o, Being))
        assert result is None or isinstance(result, Being)
        return result

    def get_item_object(
            self,
            cell: types.cell_t
    ) -> tp.Optional[types.item_object_t]:
        """Get the ItemObject at the given cell of self."""
        result = self._get_object(cell, lambda o: isinstance(o, ItemObject))
        assert result is None or isinstance(result, ItemObject)
        return result

    def get_chest_object(
            self,
            cell: types.cell_t
    ) -> tp.Optional[types.object_t]:
        """Get the chest at the given cell of self."""
        result = self._get_object(cell, lambda o: o.is_chest())
        assert result is None or isinstance(result, Object)
        return result

    def get_openable_object(
            self,
            cell: types.cell_t
    ) -> tp.Optional[types.object_t]:
        """Get the openable object at the given cell of self."""
        result = self._get_object(cell, lambda o: o.is_openable())
        assert result is None or isinstance(result, Object)
        return result

    def add_item_object(self, cell: types.cell_t) -> types.item_object_t:
        """Add an ItemObject at the given cell of self.

        Nothing is done if the cell already has an ItemObject.  The
        ItemObject a the cell is returned.

        """
        result = self.get_item_object(cell)
        if result is None:
            result = ItemObject(Inventory())
            self.add_object(cell, result)
        return result

    def put_item(self, cell: types.cell_t, item: types.item_t) -> None:
        """Add an item to the ItemObject at the given cell of self.

        An ItemObject is added at the given cell if not present.

        """
        self.add_item_object(cell).inventory.add_item(item)

    def put_inventory(
            self,
            cell: types.cell_t,
            inv: types.inventory_t
    ) -> None:
        """Add inventory inv to the ItemObject at the given cell of self.

        An ItemObject is added at the given cell if not present.

        """
        if not inv.is_empty():
            self.add_item_object(cell).inventory.add_inventory(inv)

    def move_being(
            self,
            being: types.being_t,
            direct: types.direction_t
    ) -> None:
        """Move the being to the given direction."""
        new_cell = Directions.move(being.cell, direct)
        self.del_object(being)
        self.add_object(new_cell, being)

    def del_location(self, cell: types.cell_t) -> None:
        """Delete the location from the given cell of self."""
        if cell in self.locations:
            del self.locations[cell]

    def has_location(self, location: str) -> bool:
        """Check if self has location."""
        return self.get_location_cell(location) is not None

    def get_cell_location(self, cell: types.cell_t) -> tp.Optional[str]:
        """Get the location associated to the given cell."""
        return self.locations.get(cell)

    def get_location_cell(self, location: str) -> tp.Optional[types.cell_t]:
        """Get the cell of the location."""
        return next(
            (c for c, l in self.locations.items() if l == location), None
        )

    def get_locations(self) -> types.cell_list_t:
        """Get self's locations."""
        return list(self.locations.keys())

    def get_targets(self) -> types.cell_list_t:
        """Get self's target cells."""
        return list(self.targets.keys())

    def del_cell_target(self, cell: types.cell_t) -> None:
        """Document this."""
        if cell in self.targets:
            del self.targets[cell]

    def get_cell_target(
            self,
            cell: types.cell_t
    ) -> tp.Optional[types.area_target_t]:
        """Document this."""
        return self.targets.get(cell)

    def is_visible(self, cell0: types.cell_t, cell1: types.cell_t) -> bool:
        """Check if cell1 is visible from cell0.

        The method checks that opaque object is between the two cells.

        """
        if cell0 == cell1:
            return True
        l1 = list(Bresenham.compute(cell0, cell1))[1:-1]
        l2 = list(Bresenham.compute(cell1, cell0))[1:-1]
        l2.reverse()
        for c1, c2 in zip(l1, l2):
            def check(lobj: tp.Iterable[types.area_object_t]) -> bool:
                return any(obj.is_opaque() for obj in lobj)
            if check(self.get_objects(c1)) and check(self.get_objects(c2)):
                return False
        return True

    def serialise_objects(
            self,
            check: types.area_object_predicate_t = lambda _: True
    ) -> tp.List[types.serialisation_t]:
        """Document this."""
        objects: tp.Dict[types.area_object_t, types.cell_list_t] = dict()
        for cell, objs in self.cells.items():
            for obj in (obj for obj in objs if check(obj)):
                objects.setdefault(obj, list()).append(cell)
        return [
            {'object': obj.serialise(), 'cells': cells}
            for obj, cells in objects.items()
        ]

    def unserialise_objects(
            self,
            data: tp.List[types.serialisation_t],
            check: types.area_object_predicate_t = lambda _: True
    ) -> None:
        """Document this."""
        for obj_cell in data:
            obj = AreaObject.unserialise(obj_cell['object'])
            if check(obj):
                for cell in obj_cell['cells']:
                    cell = (cell[0], cell[1])
                    self.add_object(cell, obj)

    def save_content(self) -> None:
        """Save self's content (all its objects, targets, locations, ...)."""
        content_file = self._get_content_file()
        Io.mkpath_of_file(content_file)
        content = {
            'locations': list(self.locations.items()),
            'targets': list(self.targets.items()),
            'objects': self.serialise_objects()
        }
        pw.Util.save_json_file(content_file, content)

    def load_content(self, load_dynamic: bool = True) -> tp.Any:
        """Load self's content (all its objects, targets, locations, ...).

        If load_dynamic == True, dynamic objects (beings, chests, ...)
        are loaded.

        """
        content = pw.Util.load_json_file(self._get_content_file())
        if content is not None:
            self.unserialise_objects(
                content['objects'],
                check=lambda obj: load_dynamic or not obj.is_dynamic()
            )
            for cell, location in content['locations']:
                self.locations[(cell[0], cell[1])] = location
            for cell, location in content['targets']:
                self.targets[(cell[0], cell[1])] = location
        self.loaded = True

    @classmethod
    def init_content_all(cls) -> None:
        """Unload the content of areas."""
        for area in Area.get_all():
            if area.loaded:
                area.init_content()

    def check(self) -> tp.Iterator[types.check_msg_t]:
        yield from super().check()
        self.load_content()
        for area_id, location in self.targets.values():
            tgt_area = Area.get(area_id)
            if tgt_area is None:
                yield (
                    'error',
                    f'in {self.id}: target area {area_id} does not exist'
                )
            else:
                tgt_area.load_content()
                if not tgt_area.has_location(location):
                    yield (
                        'error',
                        f'in {self.id}: target area {area_id} does ' +
                        f'not have location {location}'
                    )
