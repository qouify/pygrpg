#!/usr/bin/env python3

"""Definition of class AreaObject."""

import typing as tp

from pygrpg.interfaces import Serialisable, Describable
from pygrpg.typing import types


class AreaObject(Serialisable, Describable):
    """Document this."""

    DEPTH_ITEM = 50
    DEPTH_BEING = 100

    def get_depth(self) -> int:
        """Document this."""
        return 0

    def get_image_files(self) -> tp.Iterator[str]:
        """Document this."""
        assert False
        yield from []

    def is_blocking(self) -> bool:
        """Document this."""
        return False

    def is_chest(self) -> bool:
        """Document this."""
        return False

    def is_dynamic(self) -> bool:
        """Document this."""
        return False

    def is_opaque(self) -> bool:
        """Document this."""
        return False

    def is_openable(self) -> bool:
        """Document this."""
        return False

    def get_cell(self) -> tp.Optional[types.cell_t]:
        """Document this."""
        assert False
        return 0, 0

    def set_cell(self, cell: tp.Optional[types.cell_t]) -> None:
        """Document this."""

    def serialise(self) -> types.serialisation_t:
        return {
            'type': type(self).__name__
        }

    @classmethod
    def unserialise(cls, data: types.serialisation_t) -> types.area_object_t:
        from . import ItemObject, Object
        from pygrpg.being import Hero, Monster
        unserialisers: tp.Dict[str, tp.Type[types.area_object_t]] = {
            'Hero': Hero,
            'ItemObject': ItemObject,
            'Monster': Monster,
            'Object': Object
        }
        return unserialisers[data['type']].unserialise(data)

    ###########################################################################

    def gui_draw(self, **kwargs: tp.Any) -> types.pg.surface.Surface:
        """Document this."""
        from pygrpg.gui import Util, Constants
        result = kwargs.get('surface')
        pos = kwargs.get('pos', (0, 0))
        size = kwargs.get('size', Constants['icon-size'])
        if result is None:
            result = Util.empty_tile(size=size)
        Util.draw_images(
            self.get_image_files(),
            result,
            size=size,
            pos=pos
        )
        return result
