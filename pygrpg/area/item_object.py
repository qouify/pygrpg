#!/usr/bin/env python3

"""Definition of class ItemObject."""

import typing as tp

from pygrpg.media import Image
from pygrpg.item import Inventory
from pygrpg.typing import types
from . import AreaObject


class ItemObject(AreaObject):
    """Document this."""

    def __init__(self, inventory: types.inventory_t):
        """Document this."""
        self.inventory: types.inventory_t

        AreaObject.__init__(self)
        self.inventory = inventory

    def get_depth(self) -> int:
        return AreaObject.DEPTH_ITEM

    def get_image_files(self) -> tp.Iterator[str]:
        img = Image.get('image-treasure')
        if img is None:
            yield from self.inventory.get_image_files()
        else:
            yield img.get_image_file_()

    def is_dynamic(self) -> bool:
        return True

    def serialise(self) -> types.serialisation_t:
        return {
            **AreaObject.serialise(self),
            'inventory': self.inventory.serialise()
        }

    @classmethod
    def unserialise(cls, data: types.serialisation_t) -> types.item_object_t:
        return ItemObject(Inventory.unserialise(data['inventory']))

    ###########################################################################

    def gui_editor_tooltip(
            self,
            **kwargs: tp.Any
    ) -> tp.Optional[types.pw.Node]:
        return self.inventory.gui_editor_tooltip()
