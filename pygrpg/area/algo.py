#!/usr/bin/env python3

"""Definition of class Algo."""

import typing as tp

from pygrpg.util import Directions
from pygrpg.typing import types
from . import Area


class Algo:
    """Class Algo defines a number of useful graph based methods."""

    @staticmethod
    def bfs(
            start: types.cell_t,
            proceed: tp.Callable[
                [types.cell_t,
                 types.direction_t,
                 types.cell_t], bool] = lambda c0, move, c1: True
    ) -> tp.Iterator[tp.Tuple[types.cell_t, float]]:
        """Perform a bfs starting from cell start.

        A destination cell c1 of an arc (c0, c1) is ignored by the
        algorithm if proceed(c0, m, c1) = False (where m is the move
        from c0 to c1).  The method generates couples (c, d) with c a
        cell, and d the distance from start to c.

        """
        todo = []
        visited = set()
        if proceed(start, (0, 0), start):
            todo.append((start, 0.0))
            yield start, 0
        while todo != []:
            c0, dist0 = todo[0]
            todo = todo[1:]
            for move, distance in Directions.DISTANCE.items():
                c1 = Directions.move(c0, move)
                dist1 = dist0 + distance
                if c1 not in visited and proceed(c0, move, c1):
                    todo.append((c1, dist1))
                    visited.add(c1)
                    yield c1, dist1

    @staticmethod
    def cells_around(
            cell: types.cell_t,
            rng: tp.Tuple[float, float],
            check: types.cell_predicate_t = lambda cell: True
    ) -> tp.Iterator[types.cell_t]:
        """Document this."""
        def proceed(
                c0: types.cell_t,
                move: types.direction_t,
                c1: types.cell_t
        ) -> bool:
            if not check(c1):
                return False
            d1 = 0 if c1 == cell else distances[c0] + Directions.DISTANCE[move]
            distances[c1] = d1
            return d1 <= rng_max
        rng_min, rng_max = rng
        distances: tp.Dict[types.cell_t, float] = dict()
        for c, dist in Algo.bfs(cell, proceed=proceed):
            if dist >= rng_min:
                yield c

    @staticmethod
    def find_closest_cell_around(
            cell: types.cell_t,
            check: types.cell_predicate_t
    ) -> tp.Optional[types.cell_t]:
        """Document this."""
        for c, _ in Algo.bfs(cell):
            if check(c):
                return c
        return None

    @staticmethod
    def accessible_cells(
            area: Area,
            cell: types.cell_t,
            max_distance: float
    ) -> tp.Iterator[types.cell_t]:
        """Document this."""
        return Algo.cells_around(
            cell, (0, max_distance),
            check=lambda c: c == cell or area.get_blocking_object(c) is None
        )

    @staticmethod
    def shortest_path(
            area: Area,
            src: types.cell_t,
            dst: types.cell_t
    ) -> tp.Optional[tp.List[types.direction_t]]:
        """Document this."""
        def proceed(
                c0: types.cell_t,
                move: types.direction_t,
                c1: types.cell_t
        ) -> bool:
            if c1 == src:
                return True
            if c1 != dst and area.get_blocking_object(c1) is not None:
                return False
            paths[c1] = paths[c0] + [move]
            return True
        paths: tp.Dict[types.cell_t, types.cell_list_t] = {src: []}
        for c, _ in Algo.bfs(src, proceed=proceed):
            if c == dst:
                return paths[c]
        return None
