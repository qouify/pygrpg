#!/usr/bin/env python3

"""Definition of class Media."""

from pygrpg.resource import Resource


class Media(Resource):
    """A Media (image, sound) that can be used during the game."""

    HAS_TEXT = False
