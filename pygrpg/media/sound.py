#!/usr/bin/env python3

"""Definition of class Sound."""

from . import Media


class Sound(Media):
    """A sound that can be played during the game."""
