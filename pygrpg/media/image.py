#!/usr/bin/env python3

"""Definition of class Image."""

from . import Media


class Image(Media):
    """An Image that can be drawned during the game."""
