#!/usr/bin/env python3

"""Definition of class MediaSource."""

import typing as tp

from pygrpg.resource import Resource


class MediaSource(Resource):
    """Document this."""

    HAS_TEXT = False

    def __init__(
            self,
            rid: str,
            name: str,
            urls: tp.Sequence[str]
    ):
        """Document this."""
        Resource.__init__(self, rid)
        self.name = name
        self.urls = list(urls)
