#!/usr/bin/env python3

"""Definition of class Editor."""

from pygrpg.typing import types
from pygrpg.config import EditorConfig
from . import App


class Editor(App[types.area_editor_node_t]):
    """Application Editor is the area editor."""

    def __init__(self) -> None:
        from pygrpg.gui import MainEditor
        App.__init__(self, 'editor', MainEditor, EditorConfig)
