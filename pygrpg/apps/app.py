#!/usr/bin/env python3

"""Definition of class App."""

from logging.handlers import RotatingFileHandler
import logging
import typing as tp
import pygame as pg

from pygrpg import glob
from pygrpg.resource import loader
from pygrpg.game import Data
from pygrpg.paths import Paths
from pygrpg.gui import PygwinConfig
from pygrpg.typing import types

AT = tp.TypeVar('AT', bound=types.area_node_t)


class App(tp.Generic[AT]):
    """Document this."""

    config_cls: tp.Type[types.config_t]
    main_cls: tp.Type['types.main_t[AT]']
    mode: types.game_mode_t

    def __init__(
            self,
            mode: types.game_mode_t,
            main_cls: tp.Type['types.main_t[AT]'],
            config_cls: tp.Type[types.config_t]
    ):
        self.config_cls = config_cls
        self.main_cls = main_cls
        self.mode = mode

    def launch(self, **kwargs: tp.Any) -> None:
        """Launch self."""
        #  setup logging
        if Paths['file-log'] is not None:
            logger = logging.getLogger()
            fmt = '%(asctime)s;%(levelname)s;%(module)s:%(lineno)d;%(message)s'
            formatter = logging.Formatter(fmt)
            file_handler = RotatingFileHandler(
                Paths['file-log'], 'a', 10_000_000, 1
            )
            file_handler.setLevel(logging.INFO)
            file_handler.setFormatter(formatter)
            logger.addHandler(file_handler)

        #  setup pygame
        pg.init()
        pg.font.init()

        #  load configuration, resources and setup pygwin
        self.config_cls.load()
        language = kwargs.pop('language', None)
        if language is not None:
            self.config_cls['language'] = language
        loader.load_game_resources()
        PygwinConfig.config(self.mode)

        game_mdata = Paths.get_game_meta_data()
        title = f'{game_mdata["name"]} -- v{game_mdata["version"]}'
        glob.MAIN = tp.cast(
            'types.main_t[types.area_node_t]',
            self.main_cls(title, **kwargs)
        )
        glob.main().start()
        clock = pg.time.Clock()
        win_sys = glob.main().win_sys
        win_sys.refresh()
        pg.display.update()

        #  main loop
        while not win_sys.closed:
            for pgevt in pg.event.get():
                win_sys.process_pg_event(pgevt)
            win_sys.refresh()
            pg.display.update()
            clock.tick(50)

        Data.quit()
        pg.quit()
