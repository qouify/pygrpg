#!/usr/bin/env python3

"""Definition of class Game."""

from pygrpg.typing import types
from pygrpg.config import GameConfig
from . import App


class Game(App[types.area_game_node_t]):
    """A Game is an application in play mode."""

    def __init__(self) -> None:
        from pygrpg.gui import MainGame
        App.__init__(self, 'game', MainGame, GameConfig)
