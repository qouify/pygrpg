#!/usr/bin/env python3

"""Definition of class ShellInputText."""

import typing as tp
import pygame as pg
import pygwin as pw

from pygrpg.typing import types


class ShellInputText(pw.InputText):
    """Document this."""

    def __init__(self, **kwargs: tp.Any):
        """Document this."""
        super().__init__(**kwargs)
        self.handler = kwargs.get('handler')
        self.completer: tp.Optional[types.completer_t]
        self.completer = kwargs.get('completer')
        self.push_choices = kwargs.get('push_choices')
        self.max_history_size = kwargs.get('max_history_size', 1000)
        self.history: tp.Optional[tp.List[str]]
        if self.max_history_size > 0:
            self.history = list()
        else:
            self.history = None
        self.shift = 0

    def complete_input(self) -> None:
        """Document this."""
        if self.completer is None:
            return
        comp = self.completer.complete(self.value)
        if len(comp) == 1:
            self.append_value(comp[0])
            self.set_cursor_at_end()
        elif len(comp) > 1:
            if self.push_choices is not None:
                self.push_choices(comp)

    def activate(self) -> bool:
        value = self.value
        self.set_value('')
        if self.history is not None:
            self.history.append(value)
            if len(self.history) > self.max_history_size:
                self.history = self.history[1:]
        if self.handler is not None:
            self.handler(value)
        self.shift = 0
        self._update_manager()
        return True

    def new_key(self, key: int, uni: str) -> bool:
        def update_from_history(move: int) -> None:
            if self.history is None:
                return
            self.shift = max(0, min(len(self.history), self.shift + move))
            if self.shift == 0:
                self.set_value('')
            else:
                self.set_value(self.history[len(self.history) - self.shift])
            self.set_cursor_at_end()
        result = True
        if key == pg.K_RETURN:
            self.activate()
        elif key == pg.K_UP:
            update_from_history(1)
        elif key == pg.K_DOWN:
            update_from_history(-1)
        elif key == pg.K_TAB and self.completer is not None:
            self.complete_input()
        else:
            if key == pg.K_c and pg.key.get_pressed()[pg.K_LCTRL]:
                self.set_value('')
            else:
                result = super().new_key(key, uni)
        if result:
            self._update_manager()
        return result
