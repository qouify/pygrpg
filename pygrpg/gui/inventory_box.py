#!/usr/bin/env python3

"""Definition of class InventoryBox."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types
from . import ResourceGrid


class InventoryBox(pw.Box):
    """An Inventory box is a pygwin Box containing an inventory."""

    def __init__(self, inv: types.inventory_t, **kwargs: tp.Any):
        """Document this."""
        style = {
            **kwargs.get('style', {}),
            'vspacing': 0,
            'hspacing': 0
        }
        self.grid: ResourceGrid
        self.inv: types.inventory_t

        pw.Box.__init__(self, style=style)
        self.grid = ResourceGrid(inv.items, row_size=8, **kwargs)
        self.inv = inv
        self.update()

    def update(self) -> None:
        """Document this."""
        self.empty()
        self.grid.update(self.inv.items)
        self.pack(self.grid)
