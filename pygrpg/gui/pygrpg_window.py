#!/usr/bin/env python3

"""Definition of class PygrpgWindow."""

import typing as tp
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types


class PygrpgWindow(pw.Window):
    """PygrpgWindow is a specialisation of pygwin.Window for Pygrpg.

    In particular, we don't use in this class the title system of
    pygwin.Window objects as this one does not allow to change title
    alignment.

    """

    def __init__(self, node: types.pw.Node, **kwargs: tp.Any):
        self.has_title: bool

        frame_content = kwargs.get('frame_content', True)
        maximised = kwargs.get('maximised', True)
        title = kwargs.pop('title', None)
        style = kwargs.pop('style', {})

        if maximised:
            style['size'] = ('100%', '100%')

        style_max = {
            'expand': True,
            'size': ('100%', '100%')
        }
        if frame_content:
            node = pw.Frame(node, style=style_max)
        self.has_title = title is not None
        if not self.has_title:
            content = node
        else:
            content = pw.Box(style=style_max)
            if not isinstance(title, list):
                if isinstance(title, str):
                    title = title.title()
                title = [title]
            title = [pw.Label.node_of(x, stc='win_title') for x in title]
            if len(title) == 1:
                title = title[0]
            else:
                title = pw.Box(
                    *title,
                    style={'orientation': 'horizontal'}
                )
            content.pack(title)
            content.pack(node)
        pw.Window.__init__(
            self,
            glob.main().win_sys,
            content,
            style=style,
            **kwargs
        )

        #  give the focus to the first node
        self.move_focus_sequential(True)

    def set_title(self, title: types.pw.Node) -> None:
        """Set self's title node."""
        assert self.has_title
        tp.cast(types.pw.Box, self.children[0]).replace(0, title)
