#!/usr/bin/env python3

"""Definition of class InputWindow."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types
from . import PygrpgWindow


class InputWindow(PygrpgWindow):
    """An InputWindow contains an InputText node used to type something."""

    def __init__(
            self,
            text: tp.Optional[str],
            on_validate: tp.Callable[[str], bool],
            input_text_kwargs: tp.Dict[str, tp.Any],
            **kwargs: tp.Any
    ) -> None:
        """Document this."""
        def validate(_: types.pg.event.Event) -> bool:
            if on_validate(input_text.value):
                self.close()
                return True
            return False
        box = pw.Box(style={'halign': 'center'})
        if text is not None:
            box.pack(pw.Label(text, style={'halign': 'center'}))
        input_text = pw.InputText(**input_text_kwargs)
        box.pack(input_text)
        input_text.add_processor('on-activate', validate)
        PygrpgWindow.__init__(
            self, box, maximised=False, frame_content=False, **kwargs
        )
