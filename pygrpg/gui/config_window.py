#!/usr/bin/env python3

"""Definition of class ConfigWindow."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types
from pygrpg.resource import Text
from .config import KeysWindow, AudioWindow, AnimationsWindow, GroupWindow
from . import PygrpgWindow


class ConfigWindow(PygrpgWindow):
    """A ConfigWindow is a window allowing to change config. parameters."""

    def __init__(self) -> None:
        def fun_open_grp_win(
                title: str,
                win_cls: tp.Type[GroupWindow]
        ) -> types.link_t:
            def fun() -> bool:
                full_title = [
                    Text.val('#text-ui-win-title-configuration'), '-', title
                ]
                win_cls(title=full_title).open()
                return True
            return fun
        box = pw.Box()
        for (txt, win_cls) in [
                ('#text-ui-word-keys', KeysWindow),
                ('#text-ui-word-animations', AnimationsWindow),
                ('#text-ui-word-audio', AudioWindow)
        ]:
            txt = Text.val(txt)
            lbl = pw.Label(txt, link=fun_open_grp_win(txt, win_cls))
            box.pack(lbl)
        title = Text.val('#text-ui-win-title-configuration')
        PygrpgWindow.__init__(self, box, title=title)
