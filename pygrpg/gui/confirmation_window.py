#!/usr/bin/env python3

"""Definition of class ConfirmationWindow."""

import typing as tp
import pygwin as pw

from pygrpg.resource import Text
from . import PygrpgWindow


class ConfirmationWindow(PygrpgWindow):
    """A ConfirmationWindow contains a label and two yes and no links."""

    def __init__(
            self,
            text: str,
            on_yes: tp.Callable[[], tp.Any],
            **kwargs: tp.Any
    ):
        """Initialise self.

        text is the label that will be put in the window.  on_yes is
        called if the user validates the yes link.

        """
        def yes() -> bool:
            self.close()
            on_yes()
            return True

        def no() -> bool:
            self.close()
            return True
        yes_label = pw.Label(Text.val('#text-ui-word-yes'), link=yes)
        no_label = pw.Label(Text.val('#text-ui-word-no'), link=no)
        style = {
            'halign': 'center',
            'orientation': 'horizontal',
            'hspacing': 50
        }
        buttons = pw.Box(yes_label, no_label, style=style)
        box = pw.Box(pw.Label(text, style={'halign': 'center'}), buttons)
        kwargs = {
            'maximised': False,
            'frame_content': False,
            **kwargs
        }
        PygrpgWindow.__init__(self, box, **kwargs)
        no_label.get_focus()
