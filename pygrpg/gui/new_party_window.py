#!/usr/bin/env python3

"""Definition of class NewPartyWindow."""

import typing as tp

import random
import pygwin as pw

from pygrpg import glob
from pygrpg.being import Race, Hero, Group
from pygrpg.resource import Text
from pygrpg.typing import types
from .new_party import Util, HeroWindow
from . import ConfirmationWindow, PygrpgWindow


class NewPartyWindow(PygrpgWindow):
    """A NewPartyWindow is the main window of a party creation."""

    def __init__(self, campaign: types.campaign_t):
        def check_party() -> tp.Optional[str]:
            names = set()
            for hero in heroes.values():
                name = hero['name']
                text = None
                if name == '':
                    text = '#text-ui-err-hero-name-empty'
                elif hero['pt_attrs'] > 0:
                    text = '#text-ui-err-hero-has-attribute-points'
                elif hero['pt_skills'] > 0:
                    text = '#text-ui-err-hero-has-skill-points'
                elif name in names:
                    text = '#text-ui-err-heroes-with-same-name'
                if text is not None:
                    return Text.val(text, data=name)
                names.add(name)
            return None

        def create_party() -> 'types.group_t[types.hero_t]':
            return Group([
                Hero(
                    name=hero['name'],
                    sex=hero['sex'],
                    race=hero['race'],
                    attrs=hero['attrs'],
                    skills=hero['skills'],
                    dressings={
                        dp: d
                        for dp, d in hero['dressings'].items()
                        if d is not None
                    }
                )
                for hero in heroes.values()
            ])

        def start_game() -> None:
            msg = check_party()
            main_game = glob.main_game()
            if msg is not None:
                main_game.push_alert(msg)
            else:
                main_game.party_created(campaign, create_party())

        tbl = pw.Table()

        #  random generation of heroes
        heroes = dict()
        for i in range(campaign.attrs['heroes']):
            pt_skills = int(campaign.attrs['skill-points'])
            pt_attrs = int(campaign.attrs['attr-points'])
            max_name_len = int(campaign.attrs['max-hero-name-len'])
            hero: types.tmp_hero_t = {
                'name': '',
                'race': random.choice(list(Race.get_all())),
                'sex': random.choice(list(Hero.SEXS)),
                'skills': dict(),
                'pt_skills': pt_skills,
                'pt_attrs': pt_attrs,
                'pt_attrs_init': pt_attrs,
                'max_name_len': max_name_len
            }
            Util.randomize_attrs(hero)
            Util.randomize_dressings(hero)
            heroes[i] = hero

        icons = dict()
        for i, hero in heroes.items():
            def open_win(i: int) -> types.link_t:
                def f() -> bool:
                    HeroWindow(heroes[i]).open()
                    return True
                return f
            icons[i] = pw.Image(hero['icon'])
            link = pw.Label(Text.val('#text-ui-word-update'), link=open_win(i))
            tbl.new_row({0: icons[i], 1: link})
        lnk_btn = pw.Label(
            Text.val('#text-ui-word-start'),
            link=start_game
        )
        tbl.new_row({0: lnk_btn}, colspan={0: 2})
        title = Text.val('#text-ui-win-title-new-party')
        PygrpgWindow.__init__(self, tbl, title=title)

    def close(self) -> None:
        def close_confirmed() -> None:
            PygrpgWindow.close(self)
        ConfirmationWindow(
            Text.val('#text-ui-msg-cancel-team-creation'),
            close_confirmed
        ).open()
