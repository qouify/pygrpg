#!/usr/bin/env python3

"""Definition of class GroupWindow."""

import typing as tp

from pygrpg.typing import types
from pygrpg.config import GameConfig as Cfg
from pygrpg.gui import PygrpgWindow


class GroupWindow(PygrpgWindow):
    """GroupWindow is the base class of sub-windows configuration."""

    def __init__(self, **kwargs: tp.Any):
        PygrpgWindow.__init__(self, self._node(), **kwargs)

    def close(self) -> None:
        Cfg.save()
        PygrpgWindow.close(self)

    def _node(self) -> types.pw.Node:
        assert False
