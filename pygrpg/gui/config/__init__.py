#!/usr/bin/env python3

"""Document this."""

from .group_window import GroupWindow
from .animations_window import AnimationsWindow
from .audio_window import AudioWindow
from .keys_window import KeysWindow

__all__ = [
    'AnimationsWindow',
    'AudioWindow',
    'GroupWindow',
    'KeysWindow'
]
