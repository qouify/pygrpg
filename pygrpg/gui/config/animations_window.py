#!/usr/bin/env python3

"""Definition of class AnimationsWindow."""

import typing as tp
import pygame as pg
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.resource import Text
from pygrpg.config import GameConfig as Cfg
from . import GroupWindow


class AnimationsWindow(GroupWindow):
    """The AnimationsWindow allows to change animations config parameters."""

    def open(self, pos: tp.Optional[types.pos_t] = None) -> None:
        super().open()
        win_sys = glob.main().win_sys
        win_sys.bind_key(pg.K_LEFT, None)
        win_sys.bind_key(pg.K_RIGHT, None)

    def close(self) -> None:
        super().close()
        win_sys = glob.main().win_sys
        win_sys.bind_key(pg.K_LEFT, 'move-focus-west')
        win_sys.bind_key(pg.K_RIGHT, 'move-focus-east')

    def _node(self) -> types.pw.Node:
        result = pw.Table()

        #  speed
        def change_speed(_: types.pg.event.Event) -> bool:
            Cfg['speed'] = range_speed.value
            return True
        range_speed = pw.Range(
            tp.cast(tp.List[int], Cfg.AVAILABLE['speed'])[0],
            tp.cast(tp.List[int], Cfg.AVAILABLE['speed'])[-1],
            value=Cfg['speed'],
            stc='config_speed_range'
        )
        range_speed.add_processor('on-change', change_speed)
        result.new_row({
            0: pw.Label(Text.val('#text-ui-word-speed')),
            1: range_speed
        })

        #  move camera
        def change_move_camera(_: types.pg.event.Event) -> bool:
            Cfg['move-camera-animations'] = checkbox_move_camera.value
            return True
        checkbox_move_camera = pw.Checkbox(value=Cfg['move-camera-animations'])
        checkbox_move_camera.add_processor('on-change', change_move_camera)
        result.new_row({
            0: pw.Label(Text.val('#text-ui-conf-')),
            1: checkbox_move_camera
        })
        return result
