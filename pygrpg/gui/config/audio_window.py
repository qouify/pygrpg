#!/usr/bin/env python3

"""Definition of class AudioWindow."""

import pygwin as pw

from pygrpg.typing import types
from pygrpg.resource import Text
from pygrpg.config import GameConfig as Cfg
from . import GroupWindow


class AudioWindow(GroupWindow):
    """An AudioWindow allows to change audio configuration."""

    def _node(self) -> types.pw.Node:
        def snd_range(param: str) -> types.pw.Range:
            def change_event(_: types.pg.event.Event) -> bool:
                val = result.value
                Cfg[param] = val
                return True
            result = pw.Range(
                0, 100, value=Cfg[param], stc='config_audio_range'
            )
            result.add_processor('on-change', change_event)
            return result
        result = pw.Table()
        result.new_row({
            0: pw.Label(Text.val('#text-ui-conf-sounds-level')),
            1: snd_range('sounds-level')
        })
        result.new_row({
            0: pw.Label(Text.val('#text-ui-conf-music-level')),
            1: snd_range('music-level')
        })
        return result
