#!/usr/bin/env python3

"""Definition of clas KeysWindow."""

import string
import typing as tp
import pygame as pg
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.resource import Text
from pygrpg.config import GameConfig as Cfg
from pygrpg.gui import PygrpgWindow
from . import GroupWindow


class KeysWindow(GroupWindow):
    """A KeysWindow allows to change key configuration."""

    def __init__(self, **kwargs: tp.Any):
        self.param_text: tp.Dict[str, types.pw.Label]

        self.param_text = dict()
        GroupWindow.__init__(self, **kwargs)

    def _node(self) -> types.pw.Node:
        def processor(param: str) -> types.link_t:
            return lambda: self._param_link(param)

        def back_to_default() -> None:
            Cfg.reset_keys_to_default()
            self._update_param_keys()
        keys = pw.Table()
        params = sorted(Cfg.ALL_KEYS, key=Cfg.key_param_to_text)
        for param in params:
            text = Cfg.key_param_to_text(param)
            self.param_text[param] = pw.Label('')
            keys.new_row({
                0: pw.Label(text, link=processor(param)),
                1: self.param_text[param]
            })
        lnk = pw.Label(
            Text.val('#text-ui-keys-back-to-default'),
            link=back_to_default,
            style={'halign': 'center'}
        )
        result = pw.Box(lnk, keys, style={'size': ('100%', None)})
        self._update_param_keys()
        return result

    def _param_link(self, param: str) -> bool:
        def on_key(evt: types.pg.event.Event) -> bool:
            result = False
            if evt.type == pg.KEYDOWN:
                result = True
                if evt.key == pg.K_ESCAPE:
                    win.close()
                elif not Cfg.is_key_allowed(evt.key):
                    msg = Text.val('#text-ui-err-unauthorized-key')
                    glob.main().push_alert(msg)
                else:
                    win.close()
                    Cfg[param] = evt.key
                    Cfg.fix_keys(param)
                    self._update_param_keys()
            return result
        box = pw.Box()
        box.pack(pw.Label(Text.val('#text-ui-msg-press-key-for-action')))
        box.pack(pw.Label(Cfg.key_param_to_text(param), stc='em'))
        win = PygrpgWindow(box, maximised=False, frame_content=False)
        win.add_processor('on-key', on_key)
        win.open()
        return True

    def _update_param_keys(self) -> None:
        for param in Cfg.ALL_KEYS:
            key: int = Cfg[param]
            if key is not None and pg.K_a <= key <= pg.K_z:
                key_text = string.ascii_lowercase[key - pg.K_a]
            else:
                key_text = Text.val({
                    pg.K_ASTERISK: '#text-ui-key-asterisk',
                    pg.K_DOWN: '#text-ui-key-down',
                    pg.K_LEFT: '#text-ui-key-left',
                    pg.K_PAGEUP: '#text-ui-key-page-up',
                    pg.K_PAGEDOWN: '#text-ui-key-page-down',
                    pg.K_RIGHT: '#text-ui-key-right',
                    pg.K_SPACE: '#text-ui-key-space',
                    pg.K_UP: '#text-ui-key-up',
                    pg.K_TAB: '#text-ui-key-tab',
                    None: '#text-ui-key-none'
                }[key])
            self.param_text[param].text = key_text
