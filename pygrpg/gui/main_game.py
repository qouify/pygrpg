#!/usr/bin/env python3

"""Definition of class MainGame."""

import typing as tp
import pygame as pg

from pygrpg import glob
from pygrpg.resource import Text
from pygrpg.game import Data, Game
from pygrpg.typing import types
from . import AreaGameNode, Main, MenuWindow, NewGameWindow, CreditsWindow
from . import NewPartyWindow, SavesWindow, ConfigWindow, PartyWindow


class MainGame(Main[types.area_game_node_t]):
    """MainGame is the Main in game mode."""

    def __init__(self, title: str, **kwargs: tp.Any):
        Main.__init__(self, title, AreaGameNode, False, **kwargs)

        self.win_sys.bind_key(
            pg.K_ESCAPE, 'user-defined',
            fun=self.on_escape_key
        )
        self.win_sys.bind_key(pg.K_DOWN, 'move-focus-south')
        self.win_sys.bind_key(pg.K_UP, 'move-focus-north')
        self.win_sys.bind_key(pg.K_LEFT, 'move-focus-west')
        self.win_sys.bind_key(pg.K_RIGHT, 'move-focus-east')
        self.win_sys.bind_key(pg.K_RETURN, 'activate')

    def start(self) -> None:
        """Start pygrpg.

        The menu window is opened.

        """
        self.open_menu_window()

    def on_escape_key(self) -> bool:
        #  the top window is closed, if any.  If no window is opened
        #  => open a menu window.
        top = self.top_window()
        if top is None:
            self.open_menu_window()
        else:
            top.close()
        return True

    def open_new_game_window(self) -> None:
        """Start a new game.

        Confirmation is asked to the user if a game is currently on.

        """
        def new_game_confirmed() -> None:
            glob.close_game()
            self.area_panel.clear_msgs()
            NewGameWindow().open()
        self.do_after_confirmation(
            not glob.in_unsaved_game(),
            Text.val('#text-ui-msg-exit-game-without-saving'),
            new_game_confirmed
        )

    def open_menu_window(self) -> None:
        """Open the menu window."""
        MenuWindow().open()

    def open_saves_window(self) -> None:
        """Open the saves window."""
        SavesWindow().open()

    def open_configuration_window(self) -> None:
        """Open the configuration window."""
        ConfigWindow().open()

    def open_credits_window(self) -> None:
        """Open the credits window."""
        CreditsWindow().open()

    def new_game_chosen(
            self,
            campaign: types.campaign_t
    ) -> None:
        """Document this."""
        glob.close_game()
        NewPartyWindow(campaign).open()

    def party_created(
            self,
            campaign: types.campaign_t,
            party: 'types.group_t[types.hero_t]'
    ) -> None:
        """Document this."""
        self.close_all_windows()
        glob.GAME = Game(campaign, party)
        glob.GAME.new()

    def load_game(self, save: types.save_t) -> None:
        """Document this."""
        game = Data.load_game(save)
        if game is None:
            self.push_alert(Text.val('#text-ui-err-savegame-not-found'))
        else:
            self.area_panel.clear_msgs()
            self.close_all_windows()
            glob.close_game()
            glob.GAME = game
            glob.GAME.start()

    def end_game(self) -> None:
        """Document this."""
        self.close_all_windows()
        glob.close_game()
        self.area_panel.clear_msgs()
        self.open_menu_window()

    def open_hero_window(self, hero: types.hero_t) -> None:
        """Document this."""
        area = self.get_area_()
        PartyWindow(hero.group, area, hero.cell).open()
