#!/usr/bin/env python3

"""Definition of class Util."""

import typing as tp
import collections
import pygame as pg
import pygwin as pw

from pygrpg.typing import types
from . import Constants


class Util:
    """Class Util contains some useful methods used everywhere in the gui."""

    @staticmethod
    def draw_images(
            images: tp.Iterable[str],
            surface: types.pg.surface.Surface,
            size: tp.Optional[types.size_t] = None,
            pos: types.pos_t = (0, 0)
    ) -> None:
        """Draw images (given as file paths) on the given surface."""
        if size is None:
            size = Constants['icon-size']
        for img in images:
            media = pw.Media.get_image(img, scale=size)
            if media is not None:
                surface.blit(media, pos)

    @staticmethod
    def get_predefined_image(
            img_id: str,
            size: tp.Optional[types.size_t] = None
    ) -> tp.Optional[types.pg.surface.Surface]:
        """Get a surface of the Image resource identified by img_id.

        Return None, if the resource does not exist.

        """
        from pygrpg.media import Image
        img = Image.get(img_id)
        if img is not None:
            return pw.Media.get_image(img.get_image_file_(), scale=size)
        return None

    @staticmethod
    def empty_tile(
            size: tp.Optional[types.size_t] = None
    ) -> types.pg.surface.Surface:
        """Return an empty surface of the given size.

        If size is None, Constants['icon-size'] is used instead.

        """
        if size is None:
            size = Constants['icon-size']
        result = pg.Surface(size).convert_alpha()
        result.fill((0, 0, 0, 0))
        return result

    @staticmethod
    def get_range(rng: tp.Tuple[float, float]) -> str:
        """Return a string for the range rng."""
        min_rng, max_rng = rng
        if min_rng == max_rng:
            return str(min_rng)
        return f'{min_rng} - {max_rng}'

    @staticmethod
    def colored_text(color: types.color_t, text: str) -> str:
        """Return the text enclosed in a color tag to be used in pygwin."""
        r = color[0]
        g = color[1]
        b = color[2]
        return f'<color rgb="{r},{g},{b}">{text}</color>'

    @staticmethod
    def resource_description_grid(
            resources: tp.Sequence[types.resource_t]
    ) -> tp.Optional[types.pw.Grid]:
        """Return a Grid containing icons of resources.

        Resources with the same id are merged.  If resources are
        merged a label with the number of identical resources is
        drawned on the icon.

        """
        from pygrpg.resource import Resource
        if resources == []:
            return None
        result = pw.Grid(style={'grid-row-size': 4})
        ctr = collections.Counter(res.id for res in resources)
        for res_id, count in ctr.items():
            surf = Resource.get_(res_id).gui_icon()
            result.pack(pw.Image(surf))
            if count > 1:
                font = pw.Media.get_font_(pw.style.DEFAULT['font'])
                lbl = font.render(str(count), True, (255, 255, 0))
                surf.blit(lbl, (5, 5))
        return result
