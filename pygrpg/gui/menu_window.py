#!/usr/bin/env python3

"""Definition of class MenuWindow."""

import pygwin as pw

from pygrpg import glob
from pygrpg.resource import Text
from . import PygrpgWindow


class MenuWindow(PygrpgWindow):
    """A MenuWindow is the game's main menu."""

    def __init__(self) -> None:
        items = [
            ('new-game', glob.main_game().open_new_game_window),
            ('saves', glob.main_game().open_saves_window),
            ('configuration', glob.main_game().open_configuration_window),
            ('credits', glob.main_game().open_credits_window),
            ('quit', glob.main_game().quit_game)
        ]
        box = pw.Box(*[
            pw.Label(Text.val(f'#text-ui-menu-{txt}'), link=fun)
            for txt, fun in items
        ])
        PygrpgWindow.__init__(
            self, box, title=Text.val('#text-ui-win-title-menu')
        )
