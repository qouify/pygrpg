#!/usr/bin/env python3

"""Definition of class AreaEditorNode."""

import typing as tp
import pygame as pg
import pygwin as pw

from pygrpg.typing import types
from pygrpg.util import Coord
from . import AreaNode, Constants, Util


class AreaEditorNode(AreaNode):
    """An AreaEditorNode is an AreaNode in editor mode."""

    selected_first: tp.Optional[types.cell_t]

    def __init__(self, **kwargs: tp.Any):
        super().__init__(**kwargs)
        self.selected_first = None

    def can_grab_focus(self) -> bool:
        return True

    def get_cell_tooltip(
            self,
            cell: types.cell_t
    ) -> tp.Optional[types.pw.Box]:
        nodes: tp.List[types.pw.Node] = list()
        area = self.area
        if area is not None:
            location = area.get_cell_location(cell)
            if location is not None:
                nodes.append(pw.Label(f'location: {location}'))
            target = area.get_cell_target(cell)
            if target is not None:
                tarea, tlocation = target
                nodes.append(pw.Label(f'target: ({tarea}, {tlocation})'))
            for obj in area.get_objects(cell):
                tooltip = obj.gui_editor_tooltip(boxify=False)
                if tooltip is not None:
                    nodes.append(tooltip)
        if nodes != list():
            return pw.Box(*nodes, stc='tooltip')
        return None

    def _draw_info_marker(self, cell: types.cell_t) -> None:
        cs = self.cell_size
        img = Util.get_predefined_image(
            '#image-editor_cell_info',
            size=(cs, cs)
        )
        if img is not None:
            self.surface.blit(img, self.cell_to_pos(cell))

    def delete_selected(self) -> None:
        """Delete all objects present on selected cells."""
        for cell in self.selected:
            self.area.clear_cell(cell)

    def _redraw(self) -> None:
        super()._redraw()
        for cell in self.area.get_locations():
            self._draw_info_marker(cell)
        for cell in self.area.get_targets():
            self._draw_info_marker(cell)

    def on_key(self, pgevt: types.pg.event.Event) -> bool:
        if not self.has_focus():
            return False
        key = pgevt.key
        if key not in AreaNode.MOVE_KEYS:
            return super().on_key(pgevt)
        pressed = pg.key.get_pressed()
        if not pressed[pg.K_LCTRL] and not pressed[pg.K_LSHIFT]:
            self.clear_selected()
            self.selected_first = None
        self.move_center(AreaNode.MOVE_KEYS[key])
        selected_cells = set(self.selected)
        if self.selected_first is None:
            self.selected_first = self.center_cell
        if pressed[pg.K_LSHIFT]:
            for cell in Coord.points_inside(
                    self.center_cell, self.selected_first
            ):
                selected_cells.add(cell)
        else:
            selected_cells.add(self.center_cell)
        self.clear_selected()
        for cell in selected_cells:
            self.select_cell(cell, Constants['color-editor-selected-cell'])
        return True
