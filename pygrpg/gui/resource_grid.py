#!/usr/bin/env python3

"""Definition of class ResourceGrid."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types


class ResourceGrid(pw.Grid):
    """A ResourceGrid is a grid containing resource nodes."""

    def __init__(
            self,
            elements: tp.Sequence[types.resource_t],
            *args: types.pw.Node,
            **kwargs: tp.Any
    ):
        """Document this."""
        self.link: tp.Callable[[types.resource_t, types.pw.Node], bool]

        style = kwargs.pop('style', {})
        pw.Grid.__init__(
            self, *args,
            **{'style': {**style, 'vspacing': 0, 'hspacing': 0}, **kwargs}
        )
        if 'element_link' in kwargs:
            self.link = kwargs['element_link']
        self.update(elements)

    def update(self, elements: tp.Sequence[types.resource_t]) -> None:
        """Document this."""
        self.empty()
        for element in sorted(elements):
            node = element.gui_mknode()
            if hasattr(self, 'link'):
                node.set_link(self.link(element, node))
            self.pack(node)
