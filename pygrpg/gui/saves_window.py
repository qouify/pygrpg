#!/usr/bin/env python3

"""Definition of class SavesWindow."""

import typing as tp
import pygwin as pw

from pygrpg import glob
from pygrpg.game import Data
from pygrpg.resource import Text
from pygrpg.typing import types
from . import InputWindow, PygrpgWindow


class SavesWindow(PygrpgWindow):
    """A SavesWindow is a window containing save games data."""

    def __init__(self) -> None:
        def new_save() -> bool:
            def validate(save_name: str) -> bool:
                self._new(save_name)
                return True
            msg = None
            if glob.game().in_fight():
                msg = Text.val('#text-ui-err-cannot-save-while-in-fight')
            if msg is not None:
                glob.main().push_alert(msg)
                return True
            InputWindow(
                Text.val('#text-ui-msg-prompt-savegame'),
                validate,
                {'stc': 'input_text_save_name'}
            ).open()
            return True
        self.box_save_list = pw.Box()
        if glob.GAME is None:
            box = self.box_save_list
        else:
            new_save_link = pw.Label(
                Text.val('#text-ui-new-savegame'),
                link=new_save,
                style={'halign': 'center'}
            )
            box = pw.Box(new_save_link, self.box_save_list)
        self._update_list()
        title = Text.val('#text-ui-win-title-saves')
        PygrpgWindow.__init__(self, box, title=title)

    def _update_list(self) -> None:
        def delete(save: types.save_t) -> tp.Callable[[], None]:
            return lambda: self._delete(save)

        def update(save: types.save_t) -> tp.Callable[[], None]:
            return lambda: self._update(save)

        def load(save: types.save_t) -> tp.Callable[[], None]:
            return lambda: self._load(save)

        def rename(save: types.save_t) -> tp.Callable[[], None]:
            return lambda: self._rename(save)
        b = self.box_save_list
        b.empty()
        for save in Data.get_save_list():
            t = pw.Table()
            date = save.date.strftime('%Y/%m/%d %H:%M')
            row: tp.Dict[int, types.pw.Node] = dict()
            row[0] = pw.Box(
                pw.Label(save.name, style={'halign': 'center'}),
                pw.Label(date, style={'halign': 'center'})
            )
            if glob.GAME is not None:
                row[1] = pw.Label(
                    Text.val('#text-ui-word-save'), link=update(save)
                )
            for txt, fun in [
                    ('#text-ui-word-load', load),
                    ('#text-ui-word-delete', delete),
                    ('#text-ui-word-rename', rename)
            ]:
                row[len(row)] = pw.Label(Text.val(txt), link=fun(save))
            t.new_row(row)
            b.pack(pw.Box(t, stc='bordered_box'))

    def _new(self, save_name: str) -> None:
        msg = None
        if save_name == '':
            msg = Text.val('#text-ui-err-savegame-name-empty')
        elif any(save.name == save_name for save in Data.get_save_list()):
            msg = Text.val('#text-ui-err-savegame-already-exists')
        if msg is not None:
            glob.main().push_alert(msg)
            return
        save = Data.new_save(save_name, glob.game())
        Data.write_save(save)
        self._update_list()

    def _delete(self, save: types.save_t) -> None:
        msg = Text.val(
            '#text-ui-msg-confirm-savegame-deletion', data=save.name
        )
        glob.main().do_after_confirmation(
            False, msg, lambda: self._delete_confirmed(save)
        )

    def _delete_confirmed(self, save: types.save_t) -> None:
        save.delete()
        self._update_list()

    def _load(self, save: types.save_t) -> None:
        glob.main().do_after_confirmation(
            not glob.in_unsaved_game(),
            Text.val('#text-ui-msg-exit-game-without-saving'),
            lambda: self._load_confirmed(save)
        )

    def _load_confirmed(self, save: types.save_t) -> None:
        glob.main_game().load_game(save)
        self._update_list()

    def _update(self, save: types.save_t) -> None:
        msg = None
        if glob.game().in_fight():
            msg = Text.val('#text-ui-err-cannot-save-while-in-fight')
        if msg is not None:
            glob.main().push_alert(msg)
        else:
            save.game = glob.game()
            msg = Text.val(
                '#text-ui-msg-confirm-savegame-update', data=save.name
            )
            glob.main().do_after_confirmation(
                False, msg, lambda: self._update_confirmed(save)
            )

    def _update_confirmed(self, save: types.save_t) -> None:
        Data.write_save(save)
        self._update_list()

    def _rename(self, save: types.save_t) -> None:
        InputWindow(
            Text.val('#text-ui-msg-prompt-savegame'),
            lambda new_name: self._rename_confirmed(save, new_name),
            {'value': save.name, 'stc': 'input_text_save_name'}
        ).open()

    def _rename_confirmed(self, save: types.save_t, new_name: str) -> bool:
        if new_name == save.name:
            return True
        msg = None
        if new_name == '':
            msg = Text.val('#text-ui-err-savegame-name-empty')
        elif any(save.name == new_name for save in Data.get_save_list()):
            msg = Text.val('#text-ui-err-savegame-already-exists')
        if msg is not None:
            glob.main().push_alert(msg)
            return False

        save.rename(new_name)
        self._update_list()
        return True
