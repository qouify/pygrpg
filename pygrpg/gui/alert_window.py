#!/usr/bin/env python3

"""Definition of class AlertWindow."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types
from pygrpg.resource import Text
from . import PygrpgWindow


class AlertWindow(PygrpgWindow):
    """An AlertWindow is a popup that contains a single node."""

    def __init__(
            self,
            node: types.pw.Node,
            **kwargs: tp.Any
    ):
        """Initialise an AlertWindow.

        node is the node contained in self.

        """
        lnk_close = pw.Label(
            Text.val('#text-ui-word-close'),
            link=self.close,
            style={'halign': 'center'}
        )
        box = pw.Box(node, lnk_close, style={'halign': 'center'})
        kwargs = {'maximised': False, 'frame_content': False, **kwargs}
        PygrpgWindow.__init__(self, box, **kwargs)
