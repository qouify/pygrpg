#!/usr/bin/env python3

"""Definition of class PygrpgAnimation."""

import pygwin as pw

from pygrpg import glob


class PygrpgAnimation(pw.Animation):
    """Document this."""

    def start(self, start_now: bool = False) -> None:
        glob.main().win_sys.frozen = True
        super().start(start_now=start_now)

    def stop(self) -> None:
        glob.main().win_sys.frozen = False
        super().stop()
