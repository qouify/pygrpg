#!/usr/bin/env python3

"""Definition of class HeroWindow."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types
from pygrpg.resource import Text
from pygrpg.gui import PygrpgWindow
from .attrs_window import AttrsWindow
from .status_window import StatusWindow
from .skills_window import SkillsWindow
from .appearance_window import AppearanceWindow
from .group_window import GroupWindow


class HeroWindow(PygrpgWindow):
    """A HeroWindow contains links to open all hero sub-windows."""

    def __init__(self, hero: types.tmp_hero_t):
        def fun_open_hero_win(
                title: str,
                cls: tp.Type[GroupWindow]
        ) -> types.link_t:
            def fun() -> bool:
                full_title = pw.Box(
                    pw.Image(hero['icon']),
                    pw.Label(title, stc='win_title'),
                    style={'orientation': 'horizontal'}
                )
                cls(hero, title=full_title).open()
                return True
            return fun
        box = pw.Box()
        cls: tp.Type[GroupWindow]
        for lbl, cls in [
                ('#text-ui-word-status', StatusWindow),
                ('#text-ui-word-attributes', AttrsWindow),
                ('#text-ui-word-appearance', AppearanceWindow),
                ('#text-ui-word-skills', SkillsWindow)
        ]:
            txt = Text.val(lbl)
            box.pack(pw.Label(txt, link=fun_open_hero_win(txt, cls)))

        title = pw.Image(hero['icon'])
        PygrpgWindow.__init__(self, box, title=title)
