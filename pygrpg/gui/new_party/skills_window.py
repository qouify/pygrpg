#!/usr/bin/env python3

"""Definition of class SkillsWindow."""

import pygwin as pw

from pygrpg.resource import Text
from pygrpg.skill import Skill, SkillCategory
from pygrpg.typing import types
from .group_window import GroupWindow


class SkillsWindow(GroupWindow):
    """A SkillsWindow allows to select initial skills of an hero."""

    def _node(self) -> types.pw.Node:
        def skill_change(
                skill: types.skill_t
        ) -> types.pg_handler_t:
            def fun(_: types.pg.event.Event) -> bool:
                before = hero['skills'].get(skill, 0)
                hero['skills'][skill] = selects[skill].value
                now = hero['skills'][skill]
                if before < now:
                    hero['pt_skills'] -= 1
                else:
                    hero['pt_skills'] += 1
                update_enabledness()
                return True
            return fun

        def update_enabledness() -> None:
            for skill in Skill.get_all():
                if hero['pt_skills'] == 0:
                    selects[skill].disable_move(pw.Select.NEXT)
                else:
                    selects[skill].enable_move(pw.Select.NEXT)
            points.text = Text.val(
                '#text-ui-remaining-points',
                data=str(hero['pt_skills'])
            )

        hero = self.hero
        result = pw.Table()
        selects = dict()
        points = pw.Label('')
        for cat in SkillCategory.get_all():
            first = True
            for skill in Skill.get_all():
                if skill.category != cat:
                    continue
                if first:
                    first = False
                    lbl = pw.Label(
                        cat.get_text_val(),
                        style={'halign': 'center'}
                    )
                    result.new_row({0: lbl}, colspan={0: 2})
                name = skill.get_text_val()
                select = pw.IntSelect(0, 0, value=hero['skills'].get(skill, 0))
                result.new_row({0: pw.Label(name), 1: select})
                select.add_processor('on-change', skill_change(skill))
                selects[skill] = select
        update_enabledness()
        result.set_cell((0, 2), points)
        return result
