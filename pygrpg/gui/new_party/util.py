#!/usr/bin/env python3

"""Definition of class Util."""

import typing as tp
import random
import numpy
import pygame as pg

from pygrpg.being import BeingAttr, Dressing, DressingSlot
from pygrpg.gui import Constants, Util as GuiUtil
from pygrpg.typing import types


class Util:
    """Class Util defines some helper methods for the party creation."""

    @classmethod
    def randomize_attrs(cls, hero: types.tmp_hero_t) -> None:
        """Randomize all hero attributes."""
        def random_attr(attr: types.being_attr_t) -> int:
            default = race.get_default_attr(attr)
            mina = race.get_min_attr(attr)
            maxa = race.get_max_attr(attr)
            while True:
                result = int(numpy.random.poisson(default))
                if mina <= result <= maxa:
                    return result
        race = hero['race']
        hero['attrs'] = {
            attr: random_attr(attr)
            for attr in BeingAttr.get_all()
        }
        hero['attrs_base'] = {**hero['attrs']}
        hero['pt_attrs'] = hero['pt_attrs_init']

    @classmethod
    def randomize_dressings(cls, hero: types.tmp_hero_t) -> None:
        """Randomize all hero dressings."""
        hero['dressings'] = dict()
        for slot in DressingSlot.get_all():
            avail: tp.List[tp.Optional[types.dressing_t]]
            avail = list(Dressing.available(slot, hero['sex']))
            if avail != list():
                avail += [None]
                hero['dressings'][slot] = random.choice(avail)
        Util.draw_icon(hero)

    @classmethod
    def draw_icon(cls, hero: types.tmp_hero_t) -> types.pg.surface.Surface:
        """Redraw the hero icon and return the resulting surface."""
        result: types.pg.surface.Surface
        if 'icon' in hero:
            result = hero['icon']
        else:
            result = pg.Surface(Constants['icon-size']).convert_alpha()
            hero['icon'] = result
        sex_img = hero['race'].get_image_sex(hero['sex'])
        result.fill((0, 0, 0, 0))
        images = [sex_img]
        for _, d in hero['dressings'].items():
            if d is not None:
                images.append(d.get_image_file_())
        GuiUtil.draw_images(images, surface=result)
        return result
