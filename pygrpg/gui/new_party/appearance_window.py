#!/usr/bin/env python3

"""Definition of class AppearanceWindow."""

import typing as tp
import pygame as pg
import pygwin as pw

from pygrpg.typing import types
from pygrpg.being import Dressing, DressingSlot
from pygrpg.resource import Text
from pygrpg.gui import Constants
from pygrpg.util import IntCouple
from .group_window import GroupWindow
from . import Util


class AppearanceWindow(GroupWindow):
    """An AppearanceWindow allows to select an hero's dressings."""

    NO_DRESSING = 0

    def _node(self) -> types.pw.Node:
        def large_icon_size() -> types.size_t:
            return IntCouple.prod(
                Constants['xx-icon-size'],
                Constants['icon-size']
            )

        def draw_icon() -> None:
            Util.draw_icon(hero)
            large_icon.surface = pg.transform.scale(
                hero['icon'], large_icon_size()
            )

        def update_selects() -> None:
            #  we copy hero['dressings'] in dressings.  otherwise it
            #  does'nt work (why ???)
            dressings = dict(hero['dressings'].items())
            for w, d in dressings.items():
                if d is None:
                    dressing[w].set_value(AppearanceWindow.NO_DRESSING)
                else:
                    dressing[w].set_value(d)
            draw_icon()

        def random_appearance() -> bool:
            Util.randomize_dressings(hero)
            update_selects()
            return True

        def change_dressing(_: types.pg.event.Event) -> bool:
            for w in hero['dressings']:
                if dressing[w].value == AppearanceWindow.NO_DRESSING:
                    hero['dressings'][w] = None
                else:
                    hero['dressings'][w] = dressing[w].value
            draw_icon()
            return True

        hero = self.hero
        center = {'halign': 'center'}
        dressing = dict()
        large_icon = pw.Image(
            pg.surface.Surface(large_icon_size()),
            style=center
        )
        box = pw.Box(
            pw.Label(
                Text.val('#text-ui-word-random'),
                link=random_appearance,
                style=center
            )
        )
        for slot in DressingSlot.get_all():
            txt = slot.get_text_val()
            dressings: tp.Dict[
                tp.Union[int, types.dressing_t], str] = {
                dressing: txt
                for dressing in Dressing.available(slot, sex=hero['sex'])
            }
            if dressings != {}:
                dressings[AppearanceWindow.NO_DRESSING] = txt
                dressing[slot] = pw.ItemSelect(dressings, style=center)
                dressing[slot].add_processor('on-change', change_dressing)
                box.pack(dressing[slot])
        update_selects()
        return pw.Box(box, large_icon, style={'orientation': 'horizontal'})
