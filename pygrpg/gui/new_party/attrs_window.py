#!/usr/bin/env python3

"""Definition of class AttrsWindow."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types
from pygrpg.resource import Text
from .group_window import GroupWindow
from . import Util


class AttrsWindow(GroupWindow):
    """An AttrsWindow lists all attributes of an hero."""

    def __init__(self, hero: types.tmp_hero_t, **kwargs: tp.Any):
        """Document this."""
        super().__init__(hero, **kwargs)
        self._reset()

    def _node(self) -> types.pw.Node:
        return pw.Table()

    def _reset(self) -> None:
        hero = self.hero
        tbl = tp.cast(types.pw.Table, self.node)

        points: types.pw.Label
        selects: tp.Dict[types.being_attr_t, types.pw.IntSelect]

        def reset_points() -> None:
            val = Text.val(
                '#text-ui-remaining-points', data=str(hero['pt_attrs'])
            )
            points.text = val

        def update_attrs() -> None:
            before = sum(hero['attrs'].values())
            for attr in hero['attrs']:
                hero['attrs'][attr] = selects[attr].value
            now = sum(hero['attrs'].values())
            hero['pt_attrs'] += before - now

        def update_enabledness() -> None:
            for attr in hero['attrs']:
                if hero['pt_attrs'] == 0:
                    selects[attr].disable_move(pw.Select.NEXT)
                else:
                    selects[attr].enable_move(pw.Select.NEXT)

        def randomize() -> bool:
            Util.randomize_attrs(hero)
            self._reset()
            return True

        def select_change(_: types.pg.event.Event) -> bool:
            update_attrs()
            reset_points()
            update_enabledness()
            return True

        tbl.empty()
        randomize_link = pw.Label(
            Text.val('#text-ui-word-random'),
            link=randomize,
            style={'halign': 'center'}
        )
        tbl.new_row({1: randomize_link})
        selects = {
            attr: pw.IntSelect(
                hero['attrs_base'][attr],
                hero['race'].get_max_attr(attr),
                value=value
            )
            for attr, value in hero['attrs'].items()
        }
        for attr, select in selects.items():
            lbl = pw.Label(attr.get_text_val())
            tbl.new_row({0: lbl, 1: select})
            select.add_processor('on-change', select_change)
        points = pw.Label('')
        tbl.set_cell((0, 2), points)
        update_enabledness()
        reset_points()
        self.give_focus(randomize_link)
