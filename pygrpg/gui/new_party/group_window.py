#!/usr/bin/env python3

"""Definition of class GroupWindow."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types
from pygrpg.gui import PygrpgWindow


class GroupWindow(PygrpgWindow):
    """GroupWindow is the base class of hero sub-windows."""

    def __init__(self, hero: types.tmp_hero_t, **kwargs: tp.Any):
        self.hero: types.tmp_hero_t
        self.node: types.pw.Node

        self.hero = hero
        self.node = self._node()
        PygrpgWindow.__init__(self, self.node, **kwargs)

    def _node(self) -> types.pw.Node:
        return pw.Empty()
