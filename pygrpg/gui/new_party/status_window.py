#!/usr/bin/env python3

"""Definition of class StatusWindow."""

import pygwin as pw

from pygrpg.typing import types
from pygrpg.being import Race, Hero
from pygrpg.resource import Text
from pygrpg.gui import InputWindow
from .group_window import GroupWindow
from . import Util


class StatusWindow(GroupWindow):
    """A StatusWindow allows to select an hero's name, race, ..."""

    def _node(self) -> types.pw.Node:
        def name_text() -> str:
            if hero['name'] == '':
                return Text.val('#text-ui-type-name')
            return hero['name']

        def update_race(_: types.pg.event.Event) -> bool:
            hero['race'] = race.value
            Util.randomize_attrs(hero)
            Util.randomize_dressings(hero)
            return True

        def update_sex(_: types.pg.event.Event) -> bool:
            hero['sex'] = sex.value
            Util.randomize_attrs(hero)
            Util.randomize_dressings(hero)
            return True

        def update_name(new_name: str) -> bool:
            hero['name'] = new_name
            name.text = name_text()
            return True

        def name_link() -> bool:
            input_text_kwargs = {
                'max_size': hero['max_name_len'],
                'value': hero['name'],
                'stc': ['input_text_hero_name']
            }
            InputWindow(
                Text.val('#text-ui-msg-type-name'),
                update_name,
                input_text_kwargs
            ).open()
            return True

        hero = self.hero

        #  name label
        name = pw.Label(name_text(), link=name_link)

        #  sex selector
        sexs = {
            sex: Text.val(f'#text-ui-word-{sex}')
            for sex in Hero.SEXS
        }
        sex = pw.ItemSelect(sexs, value=hero['sex'])
        sex.add_processor('on-change', update_sex)

        #  race selector
        races = {
            race: race.get_text_val()
            for race in Race.get_all()
        }
        race = pw.ItemSelect(races, value=hero['race'])
        race.add_processor('on-change', update_race)

        result = pw.Table(
            cells={
                (0, 0): pw.Label(Text.val('#text-ui-word-name')),
                (0, 1): name,
                (1, 0): pw.Label(Text.val('#text-ui-word-sex')),
                (1, 1): sex,
                (2, 0): pw.Label(Text.val('#text-ui-word-race')),
                (2, 1): race
            }
        )
        return result
