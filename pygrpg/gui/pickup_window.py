#!/usr/bin/env python3

"""Definition of class PickupWindow."""

import typing as tp
import pygwin as pw

from pygrpg.resource import Text
from pygrpg.typing import types
from . import InventoryBox, PygrpgWindow


class PickupWindow(PygrpgWindow):
    """A PyckupWindow allows to pick items lying on the floor or in chest."""

    def __init__(
            self,
            src: types.pickup_src_t,
            hero: types.being_t,
            inventory: types.inventory_t
    ):
        move_t = tp.Callable[[], None]
        hero_inventory = hero.get_inventory()
        hero_view: InventoryBox

        def take(item: types.item_t, node: types.pw.Node) -> move_t:
            return lambda: move_item(item, node, inventory, hero_inventory)

        def discard(item: types.item_t, node: types.pw.Node) -> move_t:
            return lambda: move_item(item, node, hero_inventory, inventory)

        def take_all() -> None:
            hero_inventory.add_inventory(inventory)
            inventory.empty()
            inv_view.update()
            hero_view.update()

        def move_item(
                item: types.item_t,
                item_node: types.pw.Node,
                src: types.inventory_t,
                dst: types.inventory_t
        ) -> None:
            if src == inventory:
                grid = inv_view.grid
                other_grid = hero_view.grid
            else:
                grid = hero_view.grid
                other_grid = inv_view.grid
            idx = grid.children.index(item_node)
            src.del_item(item)
            dst.add_item(item)
            inv_view.update()
            hero_view.update()
            if grid.children == []:
                other_grid.children[0].get_focus()
            else:
                idx = min(idx, len(grid.children) - 1)
                grid.children[idx].get_focus()

        self.inventory = inventory

        title = {
            'chest': '#text-ui-win-title-items-chest',
            'ground': '#text-ui-win-title-items-ground'
        }[src]

        #  on top: frame with inventory
        inv_view = InventoryBox(
            inventory,
            element_link=take
        )
        inv_frame = pw.Frame(
            inv_view,
            style={'size': ('100%', 200)}
        )

        #  on bottom: frame with the hero inventory
        hero_view = InventoryBox(
            hero_inventory,
            element_link=discard
        )
        hero_frame = pw.Frame(
            hero_view,
            style={'expand': True, 'size': ('100%', '100%')}
        )

        take_all_label = pw.Label(Text.val('#text-ui-take-all'), link=take_all)
        large = {'font-size': 'large'}
        box = pw.Box(
            pw.Box(
                pw.Label(Text.val(title).capitalize(), style=large),
                take_all_label,
                style={'orientation': 'horizontal'}
            ),
            inv_frame,
            pw.Label(
                Text.val('#text-ui-word-inventory').capitalize(), style=large
            ),
            hero_frame,
            style={'expand': True, 'size': ('100%', '100%')}
        )

        PygrpgWindow.__init__(self, box, frame_content=False)
        take_all_label.get_focus()
