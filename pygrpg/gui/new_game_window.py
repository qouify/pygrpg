#!/usr/bin/env python3

"""Definition of class NewGameWindow."""

import typing as tp
import pygwin as pw

from pygrpg import glob
from pygrpg.resource import Text
from pygrpg.campaign import Campaign
from pygrpg.typing import types
from . import PygrpgWindow


class NewGameWindow(PygrpgWindow):
    """A NewGameWindow allows to start a new game by selecting a gampaign."""

    def __init__(self) -> None:
        """Document this."""
        def campaign_link(
                campaign: types.campaign_t
        ) -> tp.Callable[[], None]:
            return lambda: glob.main_game().new_game_chosen(campaign)
        box = pw.Box()
        for campaign in Campaign.get_all():
            title = campaign.get_text_val()
            link = pw.Label(title, link=campaign_link(campaign))
            box.pack(link)
        PygrpgWindow.__init__(
            self, box, title=Text.val('#text-ui-win-title-new-game')
        )
