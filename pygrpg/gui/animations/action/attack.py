#!/usr/bin/env python3

"""Definition of class AttackAnimation."""

from pygrpg.typing import types
from . import MoveAnimation


class AttackAnimation(MoveAnimation):
    """An AttackAnimation is launched when a being attacks."""

    def __init__(
            self,
            attack: types.attack_t
    ):
        super().__init__(attack.being, attack.get_target().cell, back=True)
