#!/usr/bin/env python3

"""Definition of class MoveBeingAnimation."""

import typing as tp
import pygame as pg

from pygrpg import glob
from pygrpg.gui import PygrpgAnimation
from pygrpg.typing import types
from pygrpg.util import Bresenham
from pygrpg.timing import Timing


class MoveAnimation(PygrpgAnimation):
    """Document this."""

    MOVE_BEING_PT = 2

    def __init__(
            self,
            being: types.being_t,
            dst: types.cell_t,
            back: bool = False
    ):
        """Document this."""
        area_node = glob.area_node()
        area_node.center_on_object(being)
        cell = being.cell
        cs = area_node.cell_size
        pos_src = area_node.cell_to_pos(cell)
        pos_dst = area_node.cell_to_pos(dst)
        positions = list(Bresenham.compute(pos_src, pos_dst))
        positions = Timing.sample(positions, self.MOVE_BEING_PT)
        if back and pos_src != pos_dst:
            positions = positions[:-1] + list(reversed(positions))

        #  create the surface of the object
        img = pg.Surface((cs, cs)).convert_alpha()
        img.fill((0, 0, 0, 0))
        being.gui_draw(surface=img)

        def move(num_pos: int) -> tp.Optional[int]:
            area_node.set_sprite(img, positions[num_pos])
            if num_pos == len(positions) - 1:
                area_node.del_sprite(img)
                area_node.show_object(being)
                return None
            return num_pos + 1

        area_node.hide_object(being)
        super().__init__(0, move, glob.main().area_panel)
