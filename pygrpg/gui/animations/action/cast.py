#!/usr/bin/env python3

"""Definition of class CastAnimation."""

import math
import typing as tp
import pygame as pg
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.util import Bresenham, IntCouple, Directions
from pygrpg.timing import Timing


class CastAnimation(pw.animations.AnimationSequence):
    """A CastAnimation is launched when a being casts a spell."""

    MOVE_PT = 4
    SLEEP_TIME = 400

    def __init__(self, cast: types.cast_t):
        being = cast.being
        spell = cast.spell
        area_panel = glob.main().area_panel
        area_node = glob.area_node()
        src = being.cell
        dst = cast.cell

        anims: tp.List[pw.Animation] = list()

        #  first animation that throws the animation image to the
        #  destination cell
        anim_file = spell.get_image_file(media='animation')
        area_node.center_on_cell(src)
        if anim_file is not None:
            anim_img = pw.Media.get_image_(anim_file)
            anim_img = anim_img.copy().convert_alpha()
            x, y = IntCouple.diff(dst, src)
            angle = math.degrees(math.atan2(-y, x))
            img = pg.transform.rotate(anim_img, angle).convert_alpha()
            pos_src = area_node.cell_to_pos(src)
            pos_dst = area_node.cell_to_pos(dst)
            positions = list(Bresenham.compute(pos_src, pos_dst))
            positions = Timing.sample(positions, self.MOVE_PT)

            def throw_anim(prog: int) -> tp.Optional[int]:
                num_pos = prog
                area_node.set_sprite(img, positions[num_pos])
                if num_pos == len(positions) - 1:
                    area_node.del_sprite(img)
                    return None
                return num_pos + 1

            anims.append(pw.Animation(0, throw_anim, area_panel))

        #  second animation of the effect on the area
        area_file = spell.get_image_file(media='area')
        if area_file is not None:
            anim_img = pw.Media.get_image_(area_file)

            #  create the dictionary mapping distance -> cell list
            dist: tp.Dict[float, tp.List[types.cell_t]] = dict()
            for cell in cast.area:
                d = Directions.distance(cast.cell, cell)
                dist.setdefault(d, []).append(cell)
            cells = [cells for _, cells in sorted(dist.items())]

            prog_t = tp.Tuple[int, tp.Optional[int]]

            def draw_img(prog: prog_t) -> tp.Optional[prog_t]:
                d, start = prog
                if start is None:
                    if d == 0:
                        area_node.center_on_cell(cast.cell)
                    for cell in cells[d]:
                        sprite = anim_img.copy()
                        pos = area_node.cell_to_pos(cell)
                        area_node.set_sprite(sprite, pos)
                    return d, pg.time.get_ticks()
                now = pg.time.get_ticks()
                if now - start < Timing.get_time(self.SLEEP_TIME):
                    return prog
                d += 1
                if d < len(cells):
                    return d, None
                area_node.clear_sprites()
                return None
            anims.append(
                pw.Animation((0, None), draw_img, area_panel)
            )

        super().__init__(anims, glob.main().area_panel)
