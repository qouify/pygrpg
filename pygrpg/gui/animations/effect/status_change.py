#!/usr/bin/env python3

"""Definition of class StatusChangeAnimation."""

import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.timing import Timing


class StatusChangeAnimation(pw.animations.SleepAnimation):
    """Document this."""

    TIME = 1_000

    def __init__(
            self,
            effect: types.status_change_t,
            being: types.being_t
    ):
        area_node = glob.area_node()
        area_node.center_on_object(being)
        cell = being.cell
        pos = area_node.cell_to_pos(cell)
        ctype = effect.ctype
        assert ctype is not None
        icon = ctype.gui_icon()
        area_node.set_sprite(icon, pos)
        descr = effect.gui_realisation_description({'being': being})
        if descr is not None:
            glob.main().push_alert(descr)

        #  remove the status change image when the animation stops
        def callback() -> None:
            area_node.del_sprite(icon)

        super().__init__(glob.main().area_panel, Timing.get_time(self.TIME))
        self.callback = callback
