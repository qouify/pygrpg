#!/usr/bin/env python3

"""This package contains all animation classes for effects."""

from .status_change import StatusChangeAnimation

EFFECT_TIME = 1_000

__all__ = [
    'EFFECT_TIME',
    'StatusChangeAnimation'
]
