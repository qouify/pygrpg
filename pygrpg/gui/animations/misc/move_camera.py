#!/usr/bin/env python3

"""Definition of class MoveCameraAnimation."""

import typing as tp
import pygwin as pw

from pygrpg import glob
from pygrpg.gui import PygrpgAnimation
from pygrpg.typing import types
from pygrpg.util import Bresenham
from pygrpg.timing import Timing
from pygrpg.config import GameConfig as Cfg


class MoveCameraAnimation(pw.animations.AnimationSequence):
    """A MoveCameraAnimation moves the center cell of the AreaNode."""

    MOVE_CAMERA_CELL_PERIOD = 10
    SLEEP_TIME = 1000

    def __init__(
            self,
            dst: types.cell_t,
            back: bool = True,
            sleep: bool = True
    ):
        """Initialize self.

        The animation moves the area node's center cell to the current
        one to dst.  If back is True, the center cell is changed back
        to the initial position.  If back and sleep are both True,
        then the animation sleeps for some time before going back to
        the initial position.

        """
        a: pw.Animation
        anims: tp.List[pw.Animation]
        area_panel = glob.main().area_panel
        area_node = glob.area_node()
        src = area_node.center_cell

        #  the move-camera-animations configuration parameter is off
        #  => create a dummy animation if not (back and sleep),
        #  otherwise sleep for some time on the destination cell then
        #  move back on the source cell
        if not Cfg['move-camera-animations']:
            if not (back and sleep):
                anims = []
            else:
                def have_slept() -> None:
                    area_node.center_on_cell(src)
                area_node.center_on_cell(dst)
                sleep_time = Timing.get_time(self.SLEEP_TIME)
                a = pw.animations.SleepAnimation(area_panel, sleep_time)
                a.callback = have_slept
                anims = [a]
            super().__init__(anims, area_panel)
            return

        #  create the list of cells we have to go through from src to
        #  dst
        cells = list(Bresenham.compute(src, dst))
        period = Timing.get_time(self.MOVE_CAMERA_CELL_PERIOD)

        progress_t = tp.Tuple[int, tp.List[types.cell_t]]

        def move(progress: progress_t) -> tp.Optional[progress_t]:
            num_cell, cells = progress
            if num_cell == len(cells):
                return None
            area_node.center_on_cell(cells[num_cell])
            return num_cell + 1, cells

        a = PygrpgAnimation((0, cells), move, area_panel, period=period)
        anims = [a]
        if back and src != dst:
            if sleep:
                sleep_time = Timing.get_time(self.SLEEP_TIME)
                a = pw.animations.SleepAnimation(area_panel, sleep_time)
                anims.append(a)
            cells = list(reversed(cells))
            a = PygrpgAnimation((0, cells), move, area_panel, period=period)
            anims.append(a)

        super().__init__(anims, area_panel)
