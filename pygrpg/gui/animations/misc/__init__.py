#!/usr/bin/env python3

"""This package contains various animation classes."""

from .move_camera import MoveCameraAnimation

__all__ = [
    'MoveCameraAnimation'
]
