#!/usr/bin/env python3

"""Definition of class PygwinConfig."""

import os
import pygwin as pw

from pygrpg.typing import types
from pygrpg.paths import Paths
from . import Constants


class PygwinConfig:
    """Class PygwinConfig contains method to configure pygwin package."""

    @classmethod
    def config(cls, mode: types.game_mode_t) -> None:
        """Configure pygwin package for the given mode (game or editor)."""
        #  load gui configuration file
        if os.path.isfile(Paths[f'file-{mode}-gui']):
            Constants.load(Paths[f'file-{mode}-gui'])

        #  load game configuration file
        if os.path.isdir(Paths['dir-config-media']):
            pw.Media.add_media_path(Paths['dir-config-media'])

        #  load fonts
        if os.path.isfile(Paths['file-fonts']):
            pw.Media.load_fonts(Paths['file-fonts'])

        #  load styles
        for style_dir in [Paths['dir-styles'], Paths[f'dir-{mode}-styles']]:
            if os.path.isdir(style_dir):
                for f in os.listdir(style_dir):
                    file_path = os.path.join(style_dir, f)
                    if (
                            os.path.isfile(file_path)
                            and os.path.splitext(file_path)[1] == '.json'
                    ):
                        pw.StyleClass.load(file_path)
