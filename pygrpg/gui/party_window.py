#!/usr/bin/env python3

"""Definition of class PartyWindow."""

import pygwin as pw

from pygrpg.typing import types
from .party import HeroWindow, Util
from . import PygrpgWindow


class PartyWindow(PygrpgWindow):
    """A PartyWindow contains hero icons and links to open HeroWindow."""

    def __init__(
            self,
            party: 'types.group_t[types.hero_t]',
            area: types.area_t,
            cell: types.cell_t
    ):
        selected = None

        def update() -> None:
            box.empty()
            for hero in party.members:
                icon = pw.Image(hero.gui_draw(), link=fun_open_hero_win(hero))
                icon.add_processor('on-focus', select(hero))
                tbl_gauges = pw.Table()
                Util.insert_status_gauges(hero, tbl_gauges)
                box_hero = pw.Box(
                    icon, tbl_gauges,
                    stc='bordered_box',
                    style={'orientation': 'horizontal'}
                )
                box.pack(box_hero)
                if hero == selected:
                    icon.get_focus()

        def select(hero: types.hero_t) -> types.pg_handler_t:
            def fun(_: types.pg.event.Event) -> bool:
                nonlocal selected
                selected = hero
                return True
            return fun

        def fun_open_hero_win(hero: types.hero_t) -> types.link_t:
            def fun() -> bool:
                def close(_: types.pg.event.Event) -> bool:
                    #  when hero window is closed, update the party
                    #  window as hero icons, hps or mps may have
                    #  changed
                    update()
                    return True
                win = HeroWindow(hero, area, cell)
                win.add_processor('on-close', close)
                win.open()
                return True
            return fun

        box = pw.Box()
        update()
        PygrpgWindow.__init__(self, box)
