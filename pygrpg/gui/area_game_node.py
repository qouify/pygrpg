#!/usr/bin/env python3

"""Definition of class AreaGameNode."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types
from . import AreaNode, Util


class AreaGameNode(AreaNode):
    """An AreaGameNode is an AreaNode in game mode."""

    draw_cursor: bool

    def __init__(self, **kwargs: tp.Any):
        super().__init__(**kwargs)
        self.draw_cursor = True

    def enable_cursor(self) -> None:
        """Cursor highlighting the center cell is now drawned."""
        self.draw_cursor = True

    def disable_cursor(self) -> None:
        """Cursor highlighting the center cell is not drawned anymore."""
        self.draw_cursor = False

    def get_cell_tooltip(
            self,
            cell: types.cell_t
    ) -> tp.Optional[types.pw.Node]:
        """Get the tooltip node to be displayed for the cell."""
        if not self.cell_is_visible(cell) or not self.cell_is_known(cell):
            return None
        nodes = list()
        for obj in self.area.get_objects(cell):
            tooltip = obj.gui_game_tooltip(boxify=False)
            if tooltip is not None:
                nodes.append(tooltip)
        if nodes != list():
            return pw.Box(*nodes, stc='tooltip')
        return None

    def _redraw(self) -> None:
        AreaNode._redraw(self)
        if self.draw_cursor:
            cs = self.cell_size
            size = (cs, cs)
            img = Util.get_predefined_image('#image-area_cursor', size=size)
            if img is not None:
                self.surface.blit(img, self.get_center_cell_pos())
