#!/usr/bin/env python3

"""Definition of class EquipSlotWindow."""

import typing as tp
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.resource import Text
from pygrpg.gui import PygrpgWindow, ResourceGrid


class EquipSlotWindow(PygrpgWindow):
    """An EquipSlotWindow allows to change the item equipped at some slot."""

    def __init__(
            self,
            hero: types.hero_t,
            slot: types.equipment_slot_t,
            **kwargs: tp.Any
    ):
        def equip(item: types.item_t, _: types.pw.Node) -> types.link_t:
            def fun() -> bool:
                if not hero.can_equip(item):
                    alert = Text.val('#text-ui-err-cannot-equip-item')
                    glob.main().push_alert(alert)
                else:
                    hero.equip(slot, item)
                    self.close()
                return True
            return fun
        items = hero.get_inventory().items
        equippable = [
            it for it in items
            if it.has_equip() and slot in it.equip.allowed_slots
        ]
        node: types.pw.Node
        if equippable == []:
            node = pw.Label(Text.val('#text-ui-msg-no-equippable-item'))
        else:
            node = ResourceGrid(equippable, element_link=equip)
        PygrpgWindow.__init__(self, node, **kwargs)
