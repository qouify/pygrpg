#!/usr/bin/env python3

"""Definition of class StatusWindow."""

import pygwin as pw

from pygrpg.typing import types
from pygrpg.resource import Text
from .group_window import GroupWindow
from .util import Util


class StatusWindow(GroupWindow):
    """A StatusWindow contains all status informations of an hero."""

    def _node(self) -> types.pw.Node:
        result = pw.Table()
        result.new_row({
            0: pw.Label(self.hero.get_name())
        })
        result.new_row({
            0: pw.Label(Text.val('#text-ui-word-sex')),
            1: pw.Label(Text.val(self.hero.get_sex_text()))
        })
        result.new_row({
            0: pw.Label(Text.val('#text-ui-word-race')),
            1: pw.Label(Text.val(self.hero.race.get_text()))
        })
        Util.insert_status_gauges(self.hero, result)
        return result
