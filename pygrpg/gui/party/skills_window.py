#!/usr/bin/env python3

"""Definition of class SkillsWindow."""

import pygwin as pw

from pygrpg.typing import types
from pygrpg.resource import Text
from .group_window import GroupWindow


class SkillsWindow(GroupWindow):
    """A SkillsWindow lists all skills of a hero with its XPs in these."""

    def _node(self) -> types.pw.Node:
        skills = self.hero.skills
        result = pw.Table()
        center = {'halign': 'center'}
        cat = None
        txt_xp_short = Text.val('#text-ui-short-xp').upper()
        for skill in sorted(skills):
            if skill.category != cat:
                cat = skill.category
                result.new_row(
                    {0: pw.Label(cat.get_text_val(), style=center)},
                    colspan={0: 3}
                )
            txt_skill = skill.get_text_val()
            txt_attr_short = skill.attr.get_text_short_val()
            txt_short_level = Text.val('#text-ui-short-level')
            name = pw.Label(f'{txt_skill} ({txt_attr_short})')
            level = pw.Label(f'{txt_short_level} {skills[skill]}')
            xp = pw.Gauge(
                0,
                self.hero.get_xp_for_next_level(skill),
                self.hero.get_xp(skill),
                label_format=lambda _, val, xp_max:
                f'{val} {txt_xp_short} / {max}',
                stc='xp_gauge'
            )
            result.new_row({0: name, 1: level, 2: xp})
        return result
