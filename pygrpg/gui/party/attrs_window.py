#!/usr/bin/env python3

"""Definition of class AttrsWindow."""

import pygwin as pw

from pygrpg.typing import types
from pygrpg.being import BeingAttr
from .group_window import GroupWindow


class AttrsWindow(GroupWindow):
    """An AttrsWindow list all attributes of an hero."""

    def _node(self) -> types.pw.Node:
        result = pw.Table()
        for attr in sorted(BeingAttr.get_all()):
            result.new_row({
                0: attr.gui_label(),
                1: pw.Label(str(self.hero.get_attr(attr)))
            })
        return result
