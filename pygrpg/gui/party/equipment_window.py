#!/usr/bin/env python3

"""Definition of class EquipmentWindow."""

import typing as tp
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.resource import Text
from pygrpg.item import EquipmentSlot
from pygrpg.gui import Util
from .group_window import GroupWindow
from .equip_slot_window import EquipSlotWindow
from .util import Util as PartyUtil


class EquipmentWindow(GroupWindow):
    """An EquipmentWindow provides a view on an hero equipment."""

    def _node(self) -> types.pw.Node:

        updated_slot: tp.Optional[types.equipment_slot_t] = None
        hero = self.hero

        def update_equipment(update_title: bool = True) -> None:
            result.empty()
            for slot in EquipmentSlot.get_all():
                update_equipment_row(slot)
            if updated_slot is not None:
                slots[updated_slot].get_focus()

            #  the hero icon may have changed => update the window title
            if update_title:
                self.update_title()

        def update_equipment_row(slot: types.equipment_slot_t) -> None:
            def equip() -> bool:
                def closed(_: types.pg.event.Event) -> bool:
                    update_equipment()
                    return True
                if glob.game().in_fight():
                    return False
                slot_text = slot.get_text_val()
                title_nodes = PartyUtil.get_title_nodes(
                    hero, Text.val('#text-ui-word-equipment'), '-', slot_text
                )
                title = pw.Box(
                    *title_nodes,
                    style={'orientation': 'horizontal'}
                )
                win = EquipSlotWindow(hero, slot, title=title)
                win.add_processor('on-close', closed)
                win.open()
                nonlocal updated_slot
                updated_slot = slot
                return True

            def unequip() -> bool:
                hero.unequip(slot)
                nonlocal updated_slot
                updated_slot = slot
                update_equipment()
                return True

            def use() -> bool:
                equip = hero.equipment
                PartyUtil.use_item(hero, equip[slot], 'equipment', slot)
                nonlocal updated_slot
                updated_slot = slot
                update_equipment()
                return True
            idx = list(EquipmentSlot.get_all()).index(slot)
            result.set_cell((idx, 0), pw.Label(slot.get_text_val()))
            item = hero.equipment.get(slot)
            if item is not None:
                slots[slot] = item.gui_mknode()
            else:
                slots[slot] = pw.Image(Util.empty_tile())
            slots[slot].set_link(equip)
            if not glob.game().in_fight():
                if item is not None:
                    lunequip = pw.Label(
                        Text.val('#text-ui-word-unequip'),
                        link=unequip
                    )
                    result.set_cell((idx, 3), lunequip)
                    if item.has_use():
                        luse = pw.Label(
                            Text.val('#text-ui-word-use'),
                            link=use
                        )
                        result.set_cell((idx, 4), luse)
            result.set_cell((idx, 1), slots[slot])

            #  hero is redrawn so update the area
            glob.area_node().set_updated()

        result = pw.Table()
        slots: tp.Dict[types.equipment_slot_t, types.pw.Node] = dict()
        update_equipment(update_title=False)
        return result
