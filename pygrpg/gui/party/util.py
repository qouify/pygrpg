#!/usr/bin/env python3

"""Definition of class Util."""

import typing as tp
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.action import Use
from pygrpg.being import Status


class Util:
    """This class defines some helper methods for this package."""

    @classmethod
    def insert_status_gauges(
            cls,
            hero: types.hero_t,
            table: types.pw.Table
    ) -> None:
        """Put in table Gauge objects containing hero's current statuses."""
        for status in Status.get_all():
            gauge = pw.Gauge(
                0, hero.get_base_status(status), hero.get_status(status),
                stc=status.get_id_suffix() + '_gauge'
            )
            table.new_row({0: pw.Label(status.get_text_val()), 1: gauge})

    @classmethod
    def use_item(
            cls,
            hero: types.hero_t,
            item: types.item_t,
            src: types.use_src_t,
            slot: tp.Optional[types.equipment_slot_t]
    ) -> None:
        """Called when an item is used via the inventory or equipement win."""
        if not item.has_use():
            glob.main().push_alert('')
        else:
            act = Use(being=hero, item=item, src=src, slot=slot)
            act.compute()
            act.do()

    @classmethod
    def get_title_nodes(
            cls,
            hero: types.hero_t,
            *txts: str
    ) -> tp.List[types.pw.Node]:
        """Get the list of pygwin Node that will form the title of a window.

        The resulting title is composed of the hero icon followed by
        labels containings strings of txts.

        """
        vcentered = {'valign': 'center'}
        result: tp.List[types.pw.Node] = [
            pw.Label(txt, stc='win_title', style={**vcentered})
            for txt in txts
        ]
        result.insert(0, pw.Image(hero.gui_draw()))
        return result
