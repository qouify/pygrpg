#!/usr/bin/env python3

"""Definition of class SelectCastTargetWindow."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types
from pygrpg.resource import Text
from pygrpg.gui import PygrpgWindow


class SelectCastTargetWindow(PygrpgWindow):
    """A SelectCastTargetWindow allows the player to choose a spell's target.

    This window is opened in exploration context only (during a fight,
    the target is selected directly via the AreaNode).

    """

    def __init__(
            self,
            spell: types.spell_t,
            party: 'types.group_t[types.hero_t]',
            fun: tp.Callable[[types.hero_t], tp.Any],
            **kwargs: tp.Any
    ):
        """Initialise a SelectCastTargetWindow.

        spell is the spell cast.  party is the party of the caster
        containing potential targets.  fun is a function called when
        the target hero has been selected by the player.

        """
        def on_select_hero(hero: types.hero_t) -> types.link_t:
            def go() -> bool:
                fun(hero)
                self.close()
                return True
            return go
        text = Text.val('#text-ui-msg-select-target-for-spell')
        box = pw.Box(pw.Label(text))
        for hero in party.members:
            icon = hero.gui_draw(draw_bars=True)
            img = pw.Image(icon, link=on_select_hero(hero))
            box.pack(img)
        PygrpgWindow.__init__(self, box, title=spell.get_text_val())
