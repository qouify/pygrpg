#!/usr/bin/env python3

"""Definition of class GroupWindow."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types
from pygrpg.gui import PygrpgWindow


class GroupWindow(PygrpgWindow):
    """GroupWindow is an abstract class implemented by all hero windows."""

    def __init__(
            self,
            hero: types.hero_t,
            area: types.area_t,
            cell: types.cell_t,
            title_texts: tp.Optional[tp.Sequence[str]] = None
    ):
        self.area: types.area_t
        self.cell: types.cell_t
        self.hero: types.hero_t
        self.title_texts: tp.List[str]

        self.hero = hero
        self.title_texts = (
            list(title_texts) if title_texts is not None else list()
        )
        self.area = area
        self.cell = cell
        PygrpgWindow.__init__(self, self._node(), title=self.get_title_node())

    def _node(self) -> types.pw.Node:
        assert False

    def get_title_node(self) -> types.pw.Node:
        """Get self's title node."""
        vcentered = {'valign': 'center'}
        nodes: tp.List[types.pw.Node] = [
            pw.Label(txt, stc='win_title', style={**vcentered})
            for txt in self.title_texts
        ]
        nodes.insert(0, pw.Image(self.hero.gui_draw()))
        return pw.Box(*nodes, style={'orientation': 'horizontal'})

    def update_title(self) -> None:
        """Update self's title."""
        self.set_title(self.get_title_node())
