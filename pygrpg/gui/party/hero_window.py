#!/usr/bin/env python3

"""Definition of class HeroWindow."""

import typing as tp
import pygwin as pw

from pygrpg.resource import Text
from pygrpg.typing import types
from .attrs_window import AttrsWindow
from .equipment_window import EquipmentWindow
from .inventory_window import InventoryWindow
from .status_window import StatusWindow
from .skills_window import SkillsWindow
from .spells_window import SpellsWindow
from .group_window import GroupWindow


class HeroWindow(GroupWindow):
    """A HeroWindow contains links to open all hero sub-windows."""

    def _node(self) -> types.pw.Node:
        def fun_open_hero_win(
                title: str,
                win_cls: tp.Type[GroupWindow]
        ) -> types.link_t:
            def fun() -> bool:
                #  when closing a sub window we have to update the
                #  title since the hero icon may have changed
                def close(_: types.pg.event.Event) -> bool:
                    self.update_title()
                    return True
                win = win_cls(
                    self.hero, self.area, self.cell, title_texts=[title]
                )
                win.add_processor('on-close', close)
                win.open()
                return True
            return fun
        result = pw.Box()
        win_cls: tp.Type[GroupWindow]
        for lbl, win_cls in [
                ('#text-ui-word-status', StatusWindow),
                ('#text-ui-word-attributes', AttrsWindow),
                ('#text-ui-word-effects', StatusWindow),
                ('#text-ui-word-skills', SkillsWindow),
                ('#text-ui-word-spells', SpellsWindow),
                ('#text-ui-word-equipment', EquipmentWindow),
                ('#text-ui-word-inventory', InventoryWindow)
        ]:
            txt = Text.val(lbl)
            result.pack(pw.Label(txt, link=fun_open_hero_win(txt, win_cls)))
        return result
