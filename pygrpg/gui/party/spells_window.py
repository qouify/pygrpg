#!/usr/bin/env python3

"""Definition of class SpellsWindow."""

import typing as tp
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.action import Cast
from pygrpg.resource import Text
from pygrpg.gui import ResourceGrid, AlertWindow
from .group_window import GroupWindow
from .select_cast_target_window import SelectCastTargetWindow


class SpellsWindow(GroupWindow):
    """A SpellsWindow contain all spells of an hero.

    There is a link for each spell icon that allows the player to
    select a spell to cast.

    """

    def _node(self) -> types.pw.Node:
        def cast(
                spell: types.spell_t,
                spell_node: types.pw.Node
        ) -> types.link_t:
            def do() -> bool:
                txt: tp.Optional[str] = None
                if spell.when == 'fight' or spell.target_type == 'enemy':
                    txt = '#text-ui-err-cannot-cast-spell-now'
                elif not spell.is_castable_by(hero):
                    txt = '#text-ui-err-cannot-cast-spell-now'
                if txt is not None:
                    glob.main().push_alert(Text.val(txt, data=hero.name))
                    return True

                def validate(targets: tp.List[types.hero_t]) -> None:
                    act = Cast(being=hero, spell=spell, targets=targets)
                    act.compute()
                    descr = act.gui_description()
                    nodes: tp.List[types.pw.Node] = list()
                    if descr is not None:
                        nodes.append(pw.Label(descr))
                    for being, effect in act.effects:
                        assign = {'being': being}
                        descr = effect.gui_realisation_description(assign)
                        if descr is not None:
                            nodes.append(pw.Label(descr))

                    def on_close(_: types.pg.event.Event) -> bool:
                        act.do()
                        return True
                    if nodes == list():
                        act.do()
                    else:
                        win = AlertWindow(pw.Box(*nodes))
                        win.add_processor('on-close', on_close)
                        win.open()
                if spell.card == 'one':
                    def select(hero: types.hero_t) -> None:
                        validate([hero])
                    SelectCastTargetWindow(spell, hero.group, select).open()
                else:
                    validate(list(hero.get_fellows()))
                return True
            return do
        hero = self.hero
        result = pw.Box()
        skills = {spell.skill for spell in hero.spells}
        for skill in sorted(list(skills)):
            result.pack(skill.gui_label())
            spells = sorted([s for s in hero.spells if s.skill == skill])
            result.pack(ResourceGrid(spells, element_link=cast))
        return result
