#!/usr/bin/env python3

"""Definition of class InventoryWindow."""

import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.resource import Text
from pygrpg.gui import InventoryBox, PygrpgWindow
from .group_window import GroupWindow
from .util import Util


class InventoryWindow(GroupWindow):
    """An InventoryWindow provides a view of an hero inventory.

    The Inventory is the same for all heroes as it's linked to the
    party.

    """

    def _node(self) -> types.pw.Node:
        def item_link(
                item: types.item_t,
                item_node: types.pw.Node
        ) -> types.link_t:
            def open_window() -> bool:
                def discard() -> bool:
                    items = self.hero.get_inventory().items
                    del items[items.index(item)]
                    self.area.put_item(self.cell, item)
                    inv.grid.remove_node(item_node)
                    win.close()

                    #  item is put on the floor so update the area
                    glob.area_node().set_updated()
                    return True

                def use() -> bool:
                    Util.use_item(self.hero, item, 'inventory', None)
                    inv.grid.remove_node(item_node)
                    win.close()
                    return True

                box = pw.Box(
                    pw.Label(Text.val('#text-ui-word-discard'), link=discard)
                )
                if item.has_use():
                    box.pack(pw.Label(Text.val('#text-ui-word-use'), link=use))
                win = PygrpgWindow(
                    box, stc='tooltip', maximised=False, frame_content=False
                )
                win.open()
                return True
            return open_window
        if glob.game().in_fight():
            txt = Text.val('#text-ui-msg-no-inventory-while-in-fight')
            return pw.Label(txt)
        inv = InventoryBox(self.hero.get_inventory(), element_link=item_link)
        result = inv
        return result
