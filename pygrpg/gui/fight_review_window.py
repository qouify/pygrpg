#!/usr/bin/env python3

"""Definition of class FightReviewWindow."""

import typing as tp
import pygwin as pw

from pygrpg.resource import Text
from pygrpg.typing import types
from . import PygrpgWindow


class FightReviewWindow(PygrpgWindow):
    """A FightReviewWindow is a window that pops once a fight finished.

    The FightReviewWindow details xp/levels won, ...

    """

    def __init__(
            self,
            won: bool,
            xps: types.fight_xps_t,
            lvls: types.fight_levels_t
    ):
        """Initialise self.

        won states if the fight has been won.  xps (lvls) is a
        Dict[hero, Dict[Skill, int]] indicating xps (levels) won by
        hero in each skill.

        """
        if won:
            title = '#text-ui-win-title-fight-victory'
        else:
            title = '#text-ui-win-title-fight-defeat'

        maximise = {'expand': True, 'size': ('100%', '100%')}

        #  experience
        menu_items: tp.Dict[pw.Node, pw.Node] = dict()
        txt_short_level = Text.val('#text-ui-short-level')
        txt_short_xp = Text.val('#text-ui-short-xp')
        for hero, xp in xps.items():
            xp_node: pw.Node
            if xp == dict():
                xp_node = pw.Label(Text.val('#text-ui-no-xp-gained'))
            else:
                xp_node = pw.Table()
                for skill, pt in xp.items():
                    lbl_skill = skill.gui_label()
                    lbl_xp = pw.Label(f'+ {pt} {txt_short_xp}')
                    lvl: pw.Node
                    if skill in lvls[hero]:
                        txt = f'+ {lvls[hero][skill]} {txt_short_level}'
                        lvl = pw.Label(txt)
                    else:
                        lvl = pw.Empty()
                    xp_node.new_row({0: lbl_skill, 1: lbl_xp, 2: lvl})
            xp_node = pw.Frame(xp_node, style=maximise)

            stat_node = pw.Label('not yet implemented')

            menu_items[pw.Image(hero.gui_draw())] = pw.Menu({
                pw.Label(Text.val('#text-ui-word-experience')): xp_node,
                pw.Label(Text.val('#text-ui-word-statistics')): stat_node
            }, style=maximise)

        menu = pw.Menu(menu_items, style=maximise)
        PygrpgWindow.__init__(self, menu, title=Text.val(title))
