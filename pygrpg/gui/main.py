#!/usr/bin/env python3

"""Definition of class Main."""

import typing as tp
import pygame as pg
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.resource import Text
from . import ConfirmationWindow, AlertWindow, AreaPanel

AT = tp.TypeVar('AT', bound=types.area_node_t)


class Main(tp.Generic[AT]):
    """Document this."""

    area_panel: 'types.area_panel_t[AT]'
    screen: pg.surface.Surface
    title: str
    win_sys: pw.WindowSystem

    def __init__(
            self,
            title: str,
            area_node_cls: tp.Type[AT],
            with_input: bool,
            **kwargs: tp.Any
    ):
        """Document this."""
        fullscreen = kwargs['fullscreen']
        resolution = kwargs['resolution']

        self.title = title

        #  setup pygame display
        pg.display.init()
        pg.display.set_caption(self.title)
        pg.key.set_repeat(200, 100)
        flags = 0 if not fullscreen else pg.FULLSCREEN
        self.screen = pg.display.set_mode(resolution, flags)

        #  initialise the pygwin window system
        self.win_sys = pw.WindowSystem(self.screen, transparent=False)
        self.win_sys.disable_mouse()

        self.area_panel = AreaPanel(
            self.win_sys, area_node_cls, with_input, self,
            style={
                'size': ('100%', '100%'),
                'padding': 0,
                'border': None
            }
        )
        self.win_sys.open_panel(self.area_panel, (0, 0))

    def get_input(self) -> types.shell_input_text_t:
        """Get self's input text."""
        return self.area_panel.input

    def get_area_node(self) -> AT:
        """Document this."""
        return self.area_panel.area_node

    def get_area(self) -> tp.Optional[types.area_t]:
        """Document this."""
        area_node = self.get_area_node()
        return None if not area_node.has_area() else area_node.area

    def get_area_(self) -> types.area_t:
        """Document this."""
        return self.get_area_node().area

    def get_size(self) -> types.size_t:
        """Document this."""
        return self.screen.get_size()

    def start(self) -> None:
        """Document this."""

    def window_opened(self) -> bool:
        """Check if there is some window is opened."""
        return self.top_window() is not None

    def top_window(self) -> tp.Optional[types.pw.Window]:
        """Get self's top window."""
        return self.win_sys.top_window()

    def close_all_windows(self) -> None:
        """Close all windows of self."""
        self.win_sys.close_all_windows()
        self.get_area_node().set_updated()

    def do_after_confirmation(
            self,
            check: bool,
            msg: str,
            handler: tp.Callable[[], tp.Any]
    ) -> None:
        """Call handler if check is False or after User's confirmation.

        A ConfirmationWindow containing message msg is opened (if
        check is True).

        """
        if check:
            handler()
        else:
            ConfirmationWindow(msg, handler).open()

    def push_alert(self, msg: str) -> None:
        """Push an alert message msg.

        The message is put in an AlertWindow is some window is already
        opened.  Otherwise it is put in the self's AreaPanel text
        board.

        """
        if self.top_window() is not None:
            AlertWindow(pw.Label(msg)).open()
        else:
            self.area_panel.push_msg(msg)

    def quit_game(self) -> None:
        """Quit game after opening a confirmation window."""
        def confirmed() -> None:
            self.win_sys.closed = True
        self.do_after_confirmation(
            not glob.in_unsaved_game(),
            Text.val('#text-ui-msg-exit-game-without-saving'),
            confirmed
        )

    def set_area(self, area: types.area_t) -> None:
        """Set the area of self's area node."""
        self.get_area_node().set_area(area)

    def close_area(self) -> None:
        """Close self's current area."""
        self.get_area_node().set_area(None)

    def on_escape_key(self) -> bool:
        """Handle the escape key."""
        return False
