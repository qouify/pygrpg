#!/usr/bin/env python3

"""Definition of class Constants."""

import pygwin as pw


class Constants(metaclass=pw.SubscriptableType):
    """Class Constants defines a number of constants used in the GUI."""

    ALL = {
        'area-panel-cell-border': 2,
        'area-panel-default-zoom-level': 1,
        'area-panel-zoom-levels': [32, 48, 64],
        'color-editor-selected-cell': (0, 0, 255, 20),
        'color-pre-fight-cell-selection': (0, 0, 255, 40),
        'color-shadow-cell': (0, 0, 0, 150),
        'color-targetable-cell': (0, 0, 255, 50),
        'color-target-cell': (0, 255, 0, 80),
        'color-target-error-cell': (255, 0, 0, 80),
        'color-text-err-msg': (255, 0, 0),
        'color-text-info-msg': (50, 200, 200),
        'elements-per-row': 8,
        'icon-size': (48, 48),
        'x-icon-size': 2,
        'xx-icon-size': 4
    }
