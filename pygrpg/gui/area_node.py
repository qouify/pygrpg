#!/usr/bin/env python3

"""Definition of class AreaNode."""

import math
import functools
import typing as tp
import pygame as pg
import pygwin as pw

from pygrpg import glob
from pygrpg.util import IntCouple, Directions
from pygrpg.typing import types
from pygrpg.being import Being
from . import Constants


class AreaNode(pw.Node):
    """Document this."""

    NO_CELL_DESCRIPTION = None

    MOVE_KEYS = {
        pg.K_UP: Directions.NORTH,
        pg.K_DOWN: Directions.SOUTH,
        pg.K_RIGHT: Directions.EAST,
        pg.K_LEFT: Directions.WEST
    }

    ZOOM_KEYS: tp.Dict[int, tp.Literal[-1, 1]] = {
        pg.K_PAGEUP: 1,
        pg.K_PAGEDOWN: -1
    }

    area: types.area_t
    cell_size: int
    center_cell: types.cell_t
    current_zoom_level: int
    description: types.pw.Node
    hidden: tp.Set[types.area_object_t]
    selected: tp.Dict[types.cell_t, tp.List[types.color_t]]
    sprites: tp.Dict[types.pg.surface.Surface, types.pos_t]
    surface: types.pg.surface.Surface
    updated: bool
    zoom_levels: tp.List[int]

    def __init__(self, **kwargs: tp.Any):
        """Document this."""
        self.visibility: types.cell_predicate_t
        self.known: types.cell_predicate_t

        pw.Node.__init__(self, **kwargs)
        self.updated = True
        if 'area' in kwargs:
            self.area = kwargs['area']
        self.center_cell = 0, 0
        self.zoom_levels = Constants['area-panel-zoom-levels']
        self.current_zoom_level = Constants['area-panel-default-zoom-level']
        self.cell_size = int(self.zoom_levels[self.current_zoom_level])
        self.selected = dict()
        self.sprites = dict()
        self.hidden = set()

    def has_area(self) -> bool:
        """Check if self has an area."""
        return hasattr(self, 'area')

    def has_description(self) -> bool:
        """Check if self has a node description."""
        return hasattr(self, 'description')

    def has_surface(self) -> bool:
        """Check if self has a surface."""
        return hasattr(self, 'surface')

    def set_updated(self) -> None:
        """Document this."""
        self.updated = True
        self._update_manager()

    def set_area(self, area: tp.Optional[types.area_t]) -> None:
        """Set self's area."""
        if area is None:
            if self.has_area():
                del self.area
        else:
            self.area = area
        self.set_updated()

    def get_center_pos(self) -> types.pos_t:
        """Get the center position of self."""
        w, h = self.surface.get_size()
        return int(w / 2), int(h / 2)

    def get_center_cell_pos(self) -> types.pos_t:
        """Get the position of the cell that is at the center of self."""
        x, y = self.get_center_pos()
        return (x - int(self.cell_size / 2), y - int(self.cell_size / 2))

    def get_origin_cell(self) -> types.cell_t:
        """Get the cell that is at the top left corner of self."""
        cs = self.cell_size
        cx, cy = self.get_center_cell_pos()
        return IntCouple.diff(
            self.center_cell, (math.ceil(cx / cs), math.ceil(cy / cs))
        )

    def get_cell_tooltip(
            self,
            cell: types.cell_t
    ) -> tp.Optional[types.pw.Node]:
        """Return a tooltip describing the cell content."""
        return None

    def zoom(self, move: tp.Literal[-1, 1]) -> None:
        """Change self's zoom level."""
        self.current_zoom_level += move
        if (
                self.current_zoom_level < 0 or
                self.current_zoom_level >= len(self.zoom_levels)
        ):
            self.current_zoom_level -= move
        else:
            self.cell_size = self.zoom_levels[self.current_zoom_level]
            self.set_updated()

    def center_on_cell(self, cell: types.cell_t) -> bool:
        """Center self on the cell."""
        if cell != self.center_cell:
            self.center_cell = cell
            if self.has_description():
                self.get_window().del_floating_node(self.description)
                del self.description
            descr = self.get_cell_tooltip(cell)
            if descr is not None:
                self.get_window().add_floating_node(descr)
                self.description = descr
            self.set_updated()
            return True
        return False

    def center_on_object(self, obj: types.area_object_t) -> bool:
        """Center self on object obj."""
        cell = obj.get_cell()
        if cell is not None:
            return self.center_on_cell(cell)
        return False

    def cell_to_pos(self, cell: types.cell_t) -> types.pos_t:
        """Get the position of the cell."""
        w, h = self.size_
        w = int(w / 2)
        h = int(h / 2)
        ci, cj = self.center_cell
        cpx, cpy = self.get_center_cell_pos()
        i, j = IntCouple.diff(cell, (ci, cj))
        return i * self.cell_size + cpx, j * self.cell_size + cpy

    def hide_object(self, obj: types.area_object_t) -> None:
        """Hide obj so that it's not drawn anymore."""
        if obj not in self.hidden:
            self.hidden.add(obj)
            self.set_updated()

    def show_object(self, obj: types.area_object_t) -> None:
        """Show obj so that it becomes drawned again."""
        if obj in self.hidden:
            self.hidden.remove(obj)
            self.set_updated()

    def move_center(self, move: types.direction_t) -> None:
        """Document this."""
        self.center_on_cell(IntCouple.sum(self.center_cell, move))

    def set_sprite(
            self,
            sprite: types.pg.surface.Surface,
            pos: types.pos_t
    ) -> None:
        """Document this."""
        self.sprites[sprite] = pos
        self.set_updated()

    def del_sprite(self, sprite: types.pg.surface.Surface) -> None:
        """Document this."""
        del self.sprites[sprite]
        self.set_updated()

    def clear_sprites(self) -> None:
        """Delete all sprites from self."""
        self.sprites = dict()
        self.set_updated()

    def select_cell(self, cell: types.cell_t, color: types.color_t) -> None:
        """Document this."""
        self.selected.setdefault(cell, [])
        if color not in self.selected[cell]:
            self.selected[cell].append(color)
        self.set_updated()

    def unselect_cell(self, cell: types.cell_t) -> None:
        """Document this."""
        if cell in self.selected:
            self.selected[cell].pop()
            if self.selected[cell] == []:
                del self.selected[cell]
            self.set_updated()

    def clear_selected(self) -> bool:
        """Document this."""
        if self.selected != dict():
            self.selected = dict()
            self.set_updated()
            return True
        return False

    def cell_is_visible(self, cell: types.cell_t) -> bool:
        """Document this."""
        return not hasattr(self, 'visibility') or self.visibility(cell)

    def cell_is_known(self, cell: types.cell_t) -> bool:
        """Document this."""
        return not hasattr(self, 'known') or self.known(cell)

    def has_selected_cells(self) -> bool:
        """Check if some cell is currently selected."""
        return self.selected != dict()

    def has_single_cell_selected(self) -> bool:
        """Check if a single cell is currently selected."""
        return len(self.selected) == 1

    def _redraw(self) -> None:
        if not self.has_area():
            return
        if not self.has_surface() or self.surface.get_size() != self.size:
            self.surface = pg.Surface(self.size_)
        self.surface.fill((0, 0, 0))
        self._draw_visible_cells()
        self._draw_selected()
        self.updated = False

        #  draw the icon of the current action is any
        if glob.ctx() is not None:
            ctx = glob.area_ctx()
            if ctx.has_action():
                ctx.action.gui_icon(surface=self.surface)

    def _draw(
            self,
            surface: types.pg.surface.Surface,
            pos: types.pos_t
    ) -> None:
        if self.has_area():
            if self.updated:
                self._redraw()
            surface.blit(self.surface, pos)
            for sprite, sprite_pos in self.sprites.items():
                surface.blit(sprite, sprite_pos)

    def _draw_object(
            self,
            obj: types.area_object_t,
            pos: types.pos_t,
            surface: tp.Optional[types.pg.surface.Surface] = None
    ) -> None:
        if surface is None:
            surface = self.surface
        size = (self.cell_size, self.cell_size)
        obj.gui_draw(
            surface=surface,
            size=size,
            pos=pos,
            draw_bars=glob.GAME is not None and glob.game().in_fight()
        )

    def _draw_visible_cells(self) -> None:
        def visible_cells() -> tp.Iterator[types.cell_t]:
            cs = self.cell_size
            ci, cj = self.get_origin_cell()
            x0, y0 = self.cell_to_pos((ci, cj))
            w, h = self.surface.get_size()
            row = 0
            y = y0
            while y < h:
                x = x0
                col = 0
                while x < w:
                    yield IntCouple.sum((ci, cj), (col, row))
                    col += 1
                    x += cs
                row += 1
                y += cs
        for cell in visible_cells():
            pos = self.cell_to_pos(cell)
            visible = self.cell_is_visible(cell)
            known = self.cell_is_known(cell)
            if visible or known:
                obj = self.area.get_objects(cell)
                has_obj = False
                for o in obj:
                    has_obj = True
                    if (
                            (visible or not isinstance(o, Being))
                            and o not in self.hidden
                    ):
                        self._draw_object(o, pos)
                if has_obj and not visible and known:
                    self._highlight_cell(
                        cell, Constants['color-shadow-cell'], borders=False
                    )

    def _draw_selected(self) -> None:
        for cell, selected in self.selected.items():
            for color in selected:
                self._highlight_cell(cell, color)

    @classmethod
    @functools.lru_cache(maxsize=100)
    def _new_cell_surface(
            cls,
            cell_size: int,
            acolor: types.color_t
    ) -> types.pg.surface.Surface:
        result = pg.Surface((cell_size, cell_size)).convert_alpha()
        result.fill(acolor)
        return result

    def _highlight_cell(
            self,
            cell: types.cell_t,
            acolor: types.color_t,
            borders: bool = True
    ) -> None:
        def cell_has_color(cell: types.cell_t, color: types.color_t) -> bool:
            if cell in self.selected:
                return any(col == color for col in self.selected[cell])
            return False
        cs = self.cell_size
        s = self._new_cell_surface(cs, acolor)
        pos = self.cell_to_pos(cell)
        self.surface.blit(s, pos)

        #  draw a border between the cell and adjacent cells that
        #  don't have the same color
        if borders:
            for d in Directions.ADJACENT:
                adj = Directions.move(cell, d)
                if not cell_has_color(adj, acolor):
                    if d == Directions.NORTH:
                        rect = pg.Rect(pos, (cs, 2))
                    elif d == Directions.SOUTH:
                        rect = pg.Rect(pw.Pos.sum(pos, (0, cs - 2)), (cs, 2))
                    elif d == Directions.WEST:
                        rect = pg.Rect(pos, (2, cs))
                    elif d == Directions.EAST:
                        rect = pg.Rect(pw.Pos.sum(pos, (cs - 2, 0)), (2, cs))
                    pw.Draw.rectangle(self.surface, acolor, rect)

    def on_key(self, pgevt: types.pg.event.Event) -> bool:
        """Document this."""
        key = pgevt.key
        if key in AreaNode.MOVE_KEYS:
            self.move_center(AreaNode.MOVE_KEYS[key])
        elif key in AreaNode.ZOOM_KEYS:
            self.zoom(AreaNode.ZOOM_KEYS[key])
        else:
            return False
        return True
