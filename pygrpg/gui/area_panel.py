#!/usr/bin/env python3

"""Definition of class AreaPanel."""

import typing as tp
import pygame as pg
import pygwin as pw

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.config import GameConfig as Cfg
from . import ShellInputText

AT = tp.TypeVar('AT', bound=types.area_node_t)


class AreaPanel(pw.Panel, tp.Generic[AT]):
    """An AreaPanel corresponds to the gameboard."""

    def __init__(
            self,
            win_sys: types.pw.WindowSystem,
            area_node_cls: tp.Type[AT],
            with_input: bool,
            main: 'types.main_t[AT]',
            **kwargs: tp.Any
    ):
        """Document this."""
        self.area_node: AT
        self.input: types.shell_input_text_t
        self.main: 'types.main_t[AT]'
        self.selected_callback: tp.Optional[
            types.area_panel_resource_selection_callback_t
        ]
        self.selected_desc: tp.Optional[types.pw.Node]
        self.selected_icon: tp.Optional[types.pw.Node]
        self.selected_index: int
        self.selection: tp.List[types.resource_t]

        def on_key(pgevt: types.pg.event.Event) -> bool:

            result = False
            has_ctx = glob.ctx() is not None

            #  the user is selecting something
            if self.selection != []:
                if pgevt.key == Cfg['key-move-west']:
                    self._selection_move(-1)
                    result = True
                elif pgevt.key == Cfg['key-move-east']:
                    self._selection_move(1)
                    result = True
                elif pgevt.key == pg.K_RETURN:
                    selected = self.get_selected()
                    assert (
                        selected is not None and
                        self.selected_callback is not None
                    )
                    self.selected_callback(selected)
                    result = True

            #  check if the current context grabs the event or if the
            #  area node grabs the event
            result = (
                result
                or has_ctx and glob.area_ctx().on_key(pgevt)
                or self.area_node.on_key(pgevt)
            )

            #  escape key: if we're in editor mode (i.e., the panel
            #  has an input text), then ESCAPE key alternatively gives
            #  the focus to the input text and to the area node
            if not result and pgevt.key == pg.K_ESCAPE and self.has_input():
                if self.input.has_focus():
                    self.area_node.get_focus()
                else:
                    self.input.get_focus()
                result = True

            #  update the current context
            result = has_ctx and glob.area_ctx().update() or result

            return result

        self.main = main
        style = {'expand': True, 'size': ('100%', '100%')}
        self.area_node = area_node_cls(style=style)
        self.selection = list()
        self.selected_index = 0
        self.selected_icon = None
        self.selected_desc = None
        self.selected_callback = None
        box = pw.Box(
            self.area_node,
            style={'vspacing': 0, 'expand': True, 'size': ('100%', '100%')}
        )

        pw.Panel.__init__(self, win_sys, box, **kwargs)

        #  input text
        if with_input:
            self.input = ShellInputText(
                prompt='# ',
                stc='shell_input'
            )
            self.add_floating_node(self.input)
            self.input.get_focus()

        #  info board
        self.board = pw.TextBoard(stc='info_board')
        self.add_floating_node(self.board)
        self.add_processor('on-key', on_key)

    def push_msg(self, msg: str) -> None:
        """Document this."""
        self.board.push_text(msg)

    def clear_msgs(self) -> None:
        """Document this."""
        self.board.empty()

    def has_input(self) -> bool:
        """Check if the panel has an input text."""
        return self.input is not None

    def set_selection_list(
            self,
            selection: tp.Iterable[types.resource_t],
            callback: types.area_panel_resource_selection_callback_t
    ) -> None:
        """Document this."""
        self.selection = list(selection)
        self.selected_index = 0
        self._update_selected_icon()
        self.selected_callback = callback

    def reset_selection_list(self) -> None:
        """Document this."""
        self.selection = []
        self.selected_index = 0
        self._update_selected_icon()

    def get_selected(self) -> tp.Optional[types.resource_t]:
        """Document this."""
        if self.selection == []:
            return None
        return self.selection[self.selected_index]

    def _update_selected_icon(self) -> None:
        for node in [self.selected_icon, self.selected_desc]:
            if node is not None:
                self.del_floating_node(node)
        if self.selection != []:
            cur = self.get_selected()
            assert cur is not None
            img = pw.Image(
                cur.gui_icon(),
                stc=['item_slot', 'selection']
            )
            self.add_floating_node(img)
            self.selected_icon = img
            desc = cur.gui_game_tooltip()
            if desc is not None:
                self.add_floating_node(desc)
            self.selected_desc = desc
        self.area_node.set_updated()

    def _selection_move(self, move: int) -> None:
        self.selected_index += move
        if self.selected_index == len(self.selection):
            self.selected_index = 0
        elif self.selected_index == - 1:
            self.selected_index = len(self.selection) - 1
        self._update_selected_icon()
