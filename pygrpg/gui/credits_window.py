#!/usr/bin/env python3

"""Definition of class CreditsWindow."""

import webbrowser
import pygwin as pw

from pygrpg.resource import Text
from pygrpg.media import MediaSource
from pygrpg.typing import types
from . import PygrpgWindow


class CreditsWindow(PygrpgWindow):
    """A CreditsWindow is a window containing credits to all media sources."""

    def __init__(self) -> None:
        def open_url(url: str) -> types.link_t:
            def fun() -> bool:
                webbrowser.open(url)
                return True
            return fun
        box = pw.Box(
            pw.Label(Text.val('#text-ui-msg-credits')),
            style={'vspacing': 30}
        )
        for src in MediaSource.get_all():
            box_src = pw.Box(pw.Label(src.name), style={'vspacing': 10})
            for url in src.urls:
                box_src.pack(pw.Label(url, link=open_url(url)))
            box.pack(box_src)
        title = Text.val('#text-ui-win-title-credits')
        PygrpgWindow.__init__(self, box, title=title)
