#!/usr/bin/env python3

"""Definition of class MainEditor."""

import typing as tp

from pygrpg.area import Area, Object
from pygrpg.campaign import Campaign
from pygrpg.typing import types
from pygrpg.util import Completer
from pygrpg.item import Item
from pygrpg.being import Monster, Group
from . import Main, AreaEditorNode, Util, Constants


class MainEditor(Main[types.area_editor_node_t]):
    """Document this."""

    def __init__(self, title: str, **kwargs: tp.Any):
        """Document this."""
        cmd_fun = tp.Callable[[str, tp.Sequence[str]], None]
        self.campaign: types.campaign_t
        self.center_cells: tp.Dict[types.area_t, types.cell_t]
        self.cmd: str
        self.completer: types.completer_t
        self.modified: tp.Set[types.area_t]
        self.prev_cmd: str
        self.works_if_single_cell_selected: tp.Set[cmd_fun]
        self.works_if_cells_selected: tp.Set[cmd_fun]
        self.works_if_no_area_opened: tp.Set[cmd_fun]

        Main.__init__(self, title, AreaEditorNode, True, **kwargs)

        custom_completer = Completer(self._custom_completer)
        self.completer = custom_completer
        self.modified = set()
        self.center_cells = dict()

        self.works_if_single_cell_selected = {
            self._cmd_add_item,
            self._cmd_add_monster,
            self._cmd_del_item,
            self._cmd_del_monster,
            self._cmd_goto_cell_target,
            self._cmd_set_cell_location,
            self._cmd_set_cell_target,
            self._cmd_unset_cell_location,
            self._cmd_unset_cell_target
        }
        self.works_if_cells_selected = {
            self._cmd_add_object,
            self._cmd_clear_cell
        }
        self.works_if_no_area_opened = {
            self._cmd_close_campaign,
            self._cmd_goto_campaign_start,
            self._cmd_open_area,
            self._cmd_open_campaign,
            self._cmd_quit
        }
        self.works_if_no_campaign_opened = {
            self._cmd_open_campaign,
            self._cmd_quit
        }

        for cmd, meth in {
                'add item %item':
                self._cmd_add_item,
                'add item %item %int':
                self._cmd_add_item,
                'add monster %monster':
                self._cmd_add_monster,
                'add monster %monster %int':
                self._cmd_add_monster,
                'del item %delitem':
                self._cmd_del_item,
                'del item %delitem %int':
                self._cmd_del_item,
                'del monster %delmonster':
                self._cmd_del_monster,
                'del monster %delmonster %int':
                self._cmd_del_monster,
                'add object %object':
                self._cmd_add_object,
                'clear cell':
                self._cmd_clear_cell,
                'goto cell target':
                self._cmd_goto_cell_target,
                'set cell location %string':
                self._cmd_set_cell_location,
                'set cell target %string %string':
                self._cmd_set_cell_target,
                'unset cell location':
                self._cmd_unset_cell_location,
                'unset cell target':
                self._cmd_unset_cell_target,
                'close campaign':
                self._cmd_close_campaign,
                'goto campaign start':
                self._cmd_goto_campaign_start,
                'open campaign %campaign':
                self._cmd_open_campaign,
                'close area':
                self._cmd_close_area,
                'open area %area':
                self._cmd_open_area,
                'save area':
                self._cmd_save_area,
                'quit':
                self._cmd_quit
        }.items():
            self.completer.add_path(cmd, meth)

        self.area_panel.input.completer = self.completer
        self.area_panel.input.handler = self._input_handler
        self.area_panel.input.push_choices = self._push_choices

    def _reset_campaign(self) -> None:
        if hasattr(self, 'campaign'):
            del self.campaign

    def on_escape_key(self) -> bool:
        #  unselect selected cells of the area panel
        area_node = self.get_area_node()
        if area_node is not None:
            area_node.clear_selected()
            return True
        return False

    def _input_handler(self, input_string: str) -> None:
        completion_result = self.completer.get_path_data(input_string)
        if completion_result is None:
            if input_string != '':
                self._push_msg('Unrecognised command!')
        else:
            handler, data, incomplete = completion_result
            area_node = self.get_area_node()
            if incomplete:
                self._push_msg('Incomplete command!')
            elif handler is not None:
                self.cmd = input_string
                msg = None
                if (
                        handler not in self.works_if_no_campaign_opened
                        and not self._campaign_opened()
                ):
                    msg = 'No campaign opened!'
                if (
                        msg is None
                        and handler not in self.works_if_no_area_opened
                        and not self._some_area_opened()
                ):
                    msg = 'No area opened!'
                if (
                        msg is None
                        and handler in self.works_if_cells_selected
                        and not area_node.has_selected_cells()
                ):
                    msg = 'No cell selected!'
                if (
                        msg is None
                        and handler in self.works_if_single_cell_selected
                        and not area_node.has_single_cell_selected()
                ):
                    msg = 'A single cell must be selected!'
                if msg is None:
                    handler(input_string, data)
                else:
                    self._push_msg(msg)
                self.prev_cmd = input_string
        if self.get_area_node() is not None:
            self.get_area_node().set_updated()

    def _push_choices(self, choices: tp.List[str]) -> None:
        if choices == []:
            return
        choices[0].split('-')
        new_choices = set()
        for choice in choices:
            words = choice.split('-')
            if words[-1] == '' and len(words) > 1:
                new_choices.add(words[-2])
            else:
                new_choices.add(words[-1])
        self._push_msg(', '.join(sorted(list(new_choices))), level='info')

    def _get_confirmation(self) -> bool:
        return self.prev_cmd == self.cmd

    def set_area(self, area: types.area_t) -> None:
        area_node = self.get_area_node()
        current_area = self.get_area()
        if current_area is not None:
            self.center_cells[current_area] = area_node.center_cell
        super().set_area(area)
        cell = self.center_cells.get(area)
        if cell is None:
            cell = area.get_center_cell()
            self.center_cells[area] = cell
        area_node.center_on_cell(cell)

    def _set_current_area_modified(self, modified: bool) -> None:
        area = self.get_area_()
        if modified:
            self.modified.add(area)
        elif area in self.modified:
            self.modified.remove(area)

    def _unload_area(self, area: types.area_t) -> None:
        area.init_content()
        if area in self.modified:
            self.modified.remove(area)

    def _some_area_modified(self) -> bool:
        return len(self.modified) > 0

    def _some_area_opened(self) -> bool:
        return self.get_area_() is not None

    def _is_area_modified(self, area: types.area_t) -> bool:
        return area in self.modified

    def _campaign_opened(self) -> bool:
        return hasattr(self, 'campaign')

    def _push_msg(
            self,
            msg: str,
            level: tp.Literal['err', 'info'] = 'err'
    ) -> None:
        self.push_alert(
            Util.colored_text(Constants[f'color-text-{level}-msg'], msg)
        )

    def _selected_cell(self) -> types.cell_t:
        result = self.get_area_node().selected_first
        assert result is not None
        return result

    def _switch_or_load_area(self, area_id: str) -> types.area_t:
        area = Area.get_(area_id)
        if not area.loaded:
            area.load_content()
        self.set_area(area)
        return area

    def _check_area_exists(self, area_id: str) -> bool:
        area = Area.get(area_id)
        if area is None or area.campaign != self.campaign:
            self._push_msg(f'Area {area_id} does not exist!')
            return False
        return True

    def _check_area_has_location(
            self,
            area: types.area_t,
            location: str
    ) -> tp.Optional[types.cell_t]:
        result = area.get_location_cell(location)
        if result is None:
            msg = f'Area {area.id} does not have a location {location}!'
            self._push_msg(msg)
        return result

    def _can_close_areas(self) -> bool:
        result = not self._some_area_modified() or self._get_confirmation()
        if not result:
            self._push_msg(
                'Some opened areas have not been saved. Repeat to confirm.',
                level='info'
            )
        return result

    def _move_to_location(self, area_id: str, location: str) -> None:
        area = self.get_area_()
        if not self._check_area_exists(area_id):
            return
        if area is None or area_id != area.id:
            area = self._switch_or_load_area(area_id)
        cell = self._check_area_has_location(area, location)
        if cell is None:
            return
        self.set_area(area)
        area_node = self.get_area_node()
        area_node.clear_selected()
        area_node.center_on_cell(cell)

    def _init_areas(self) -> None:
        self.modified = set()

    T = tp.TypeVar('T', bound=types.resource_t)

    def _pick_resource(
            self,
            cls: tp.Type[T],
            rid: str,
            descr: str
    ) -> tp.Optional[T]:
        result = cls.pick_random(rid)
        if result is None:
            self._push_msg(f'No such {descr}: {rid}!')
        return result

    def _update_prompt(self) -> None:
        prompt = ''
        if self._campaign_opened():
            assert self.campaign is not None
            prompt += '[' + self.campaign.get_id_suffix()
            area = self.get_area()
            if area is not None:
                prompt += '/' + area.get_id_suffix()
            prompt += '] '
        prompt += '# '
        self.get_input().set_prompt(prompt)

    def _get_number(
            self,
            data: tp.Sequence[str], idx: int
    ) -> tp.Optional[int]:
        if len(data) <= idx:
            return 1
        try:
            return int(data[idx])
        except ValueError:
            self._push_msg(f'Number expected (got {data[idx]})!')
            return None

    def _custom_completer(self, pattern: str, word: str) -> tp.List[str]:
        def merge(
                cls: tp.Type[types.resource_t],
                resources: tp.Iterable[types.resource_t],
                include: tp.Callable[[types.resource_t], bool],
                word: str
        ) -> tp.List[str]:
            result = set()
            for res in (res for res in resources if include(res)):
                #  remove the first word that identify the resource type
                suffix = res.get_id_suffix()
                if suffix.startswith(word):
                    try:
                        idx = suffix.index('-', len(word))
                        match = suffix[:idx + 1]
                    except ValueError:
                        match = suffix
                    result.add(match)
            return sorted(list(result))

        resources: tp.Iterable[types.resource_t]

        def include(res: types.resource_t) -> bool:
            if cls != Area:
                return True
            assert isinstance(res, Area)
            return res.campaign == self.campaign

        def completer_delmonster() -> tp.List[types.monster_t]:
            area = self.get_area_()
            cell = self._selected_cell()
            being = tp.cast(types.monster_t, area.get_being(cell))
            if being is None:
                return []
            return being.group.members

        def completer_delitem() -> tp.List[types.item_t]:
            area = self.get_area_()
            cell = self._selected_cell()
            chest = area.get_chest_object(cell)
            being = area.get_being(cell)
            item_obj = area.get_item_object(cell)
            inventory: tp.Optional[types.inventory_t] = None
            if chest is not None:
                inventory = chest.inventory
            elif being is not None:
                inventory = being.get_inventory()
            elif item_obj is not None:
                inventory = item_obj.inventory
            if inventory is None:
                return []
            return list(inventory.items)
        getter_t = tp.Callable[[], tp.Iterable[types.resource_t]]
        d: tp.Dict[
            str,
            tp.Tuple[
                tp.Type[types.resource_t],
                tp.Optional[getter_t]
            ]
        ] = {
            '%campaign': (Campaign, None),
            '%area': (Area, None),
            '%monster': (Monster, None),
            '%object': (Object, None),
            '%item': (Item, None),
            '%delmonster': (Monster, completer_delmonster),
            '%delitem': (Item, completer_delitem)
        }
        cls, get_func = d[pattern]
        if cls == Area and not self._campaign_opened():
            return []
        if get_func is None:
            resources = cls.get_all()
        else:
            resources = get_func()

        result = merge(cls, resources, include, word)

        #  if the resulting list contains a single incomplete
        #  resource-id (with '-' at the end) we duplicate it so that
        #  the completer does not terminate the word with a ' '
        if len(result) == 1 and result[0][-1] == '-':
            result.append(result[0])
        return result

    ###########################################################################

    def _cmd_add_item(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        no = self._get_number(data, 1)
        if no is None:
            return
        area = self.get_area_()
        cell = self._selected_cell()
        chest = area.get_chest_object(cell)
        being = area.get_being(cell)
        item = self._pick_resource(Item, data[0], 'item')
        if item is None:
            return
        for _ in range(no):
            inst = item.instanciate()
            if chest is not None:
                chest.inventory.add_item(inst)
            elif being is not None:
                being.get_inventory().add_item(inst)
            else:
                area.put_item(cell, inst)
        self._set_current_area_modified(True)

    def _cmd_add_monster(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        no = self._get_number(data, 1)
        if no is None:
            return
        cell = self._selected_cell()
        area = self.get_area_()
        for _ in range(no):
            monster = self._pick_resource(Monster, data[0], 'monster')
            if monster is None:
                return
            being = area.get_being(cell)

            #  if there no being present on the cell we create a new
            #  one and put it and the cell.  if items where on the
            #  cell we remove them and put these in the new group
            #  inventory
            new_monster = monster.instanciate()
            if being is not None:
                being.group.add_member(new_monster)
            else:
                grp = Group([new_monster])
                area.add_object(cell, new_monster)
                item_object = area.get_item_object(cell)
                if item_object is not None:
                    grp.inventory.add_inventory(item_object.inventory)
                    area.del_object(item_object, cell=cell)
        self._set_current_area_modified(True)

    def _cmd_del_item(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        area = self.get_area_()
        cell = self._selected_cell()
        grp = area.get_being(cell)
        if grp is not None:
            pass
        else:
            pass  # items = area.get_item_object(cell)

        self._set_current_area_modified(True)

    def _cmd_del_monster(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        area = self.get_area_()
        cell = self._selected_cell()
        being = area.get_being(cell)
        no = self._get_number(data, 1)
        if being is None or no is None:
            return
        grp = tp.cast('types.group_t[types.monster_t]', being.group)
        members = grp.members
        for _ in range(no):
            monster = self._pick_resource(Monster, data[0], 'monster')
            if monster is None:
                return
            idx = next(
                (i for i, m in enumerate(members) if m.id == monster.id), None
            )
            if idx is None:
                break
            del members[idx]

        #  no monster left in the group => put the group inventory
        #  on the floor and remove the group from the area
        if members == []:
            area.put_inventory(cell, grp.inventory)
            area.del_object(being)

        self._set_current_area_modified(True)

    def _cmd_add_object(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        area = self.get_area_()
        for cell in self.get_area_node().selected:
            obj = self._pick_resource(Object, data[0], 'object')
            if obj is None:
                return
            area.add_object(cell, obj.instanciate())
        self._set_current_area_modified(True)

    def _cmd_clear_cell(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        self.get_area_node().delete_selected()
        self._set_current_area_modified(True)

    def _cmd_goto_cell_target(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        cell = self._selected_cell()
        area = self.get_area_()
        target = area.get_cell_target(cell)
        if target is None:
            self._push_msg('Cell does not have an associated target!')
            return
        area_id, location = target
        self._move_to_location(area_id, location)

    def _cmd_set_cell_location(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        cell = self._selected_cell()
        location = data[0]
        area = self.get_area_()
        if area.has_location(location):
            self._push_msg(f'Location {location} already exists!')
            return
        area.locations[cell] = location
        self._set_current_area_modified(True)

    def _cmd_set_cell_target(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        cell = self._selected_cell()
        area_id = data[0]
        location = data[1]
        if not self._check_area_exists(area_id):
            return
        area = Area.get_(area_id)
        area.load_content()
        if not self._check_area_has_location(area, location):
            return
        self._set_current_area_modified(True)
        area = self.get_area_()
        area.targets[cell] = (area_id, location)

    def _cmd_unset_cell_location(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        cell = self._selected_cell()
        area = self.get_area_()
        location = area.get_cell_location(cell)
        if location is None:
            self._push_msg('Cell is not a location!')
        else:
            area.del_location(cell)
            self._set_current_area_modified(True)

    def _cmd_unset_cell_target(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        cell = self._selected_cell()
        area = self.get_area_()
        location = area.get_cell_target(cell)
        if location is None:
            self._push_msg('Cell does not have a target!')
        else:
            area.del_cell_target(cell)
            self._set_current_area_modified(True)

    def _cmd_close_campaign(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        if self._campaign_opened():
            self._push_msg('No campaign opened!')
        elif self._can_close_areas():
            self.close_area()
            self._reset_campaign()
            self._init_areas()
            self._update_prompt()

    def _cmd_goto_campaign_start(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        area = self.campaign.attrs['start-area']
        location = self.campaign.attrs['start-location']
        if area is not None and location is not None:
            self._move_to_location(area, location)

    def _cmd_open_campaign(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        if self._campaign_opened():
            self._push_msg('Close opened campaign first!')
        else:
            cid = data[0]
            c = Campaign.get(cid)
            if c is None:
                self._push_msg(f'Campaign {cid} does not exist!')
            else:
                self.campaign = c
                self._init_areas()
                self._update_prompt()

    def _cmd_close_area(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        area = self.get_area_()
        if self._is_area_modified(area) and not self._get_confirmation():
            self._push_msg('Area not saved. Repeat to confirm.', level='info')
            return
        self._unload_area(area)
        new_area = next((area for area in Area.get_all() if area.loaded), None)
        if new_area is not None:
            self.set_area(new_area)
        else:
            self.close_area()
        self._update_prompt()

    def _cmd_open_area(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        area_id = data[0]
        if not self._check_area_exists(area_id):
            return
        self._switch_or_load_area(area_id)
        self.get_area_node().clear_selected()
        self._update_prompt()

    def _cmd_save_area(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        area = self.get_area_()
        area.save_content()
        self._set_current_area_modified(False)

    def _cmd_quit(
            self,
            cmd: str,
            data: tp.Sequence[str]
    ) -> None:
        if self._can_close_areas():
            self.quit_game()
