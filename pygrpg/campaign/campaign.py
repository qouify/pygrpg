#!/usr/bin/env python3

"""Definition of class Campaign."""

import typing as tp

from pygrpg.typing import types
from pygrpg.resource import Resource
from pygrpg.rule import Rule


class Campaign(Resource):
    """Document this."""

    def __init__(
            self,
            rid: str,
            attrs: types.campaign_attr_t
    ):
        """Initialise self.

        attrs is the attributes dictionary of the campaign.

        """
        self.attrs: types.campaign_attr_t

        Resource.__init__(self, rid)
        self.attrs = attrs

    def get_xp_for_level(self, lvl: int) -> int:
        """Get the number of xps required to reach level lvl (from lvl - 1)."""
        return int(Rule.get_('xp-per-level').eval({'level': lvl}))

    def check(self) -> tp.Iterator[types.check_msg_t]:
        def err(msg: str) -> tp.Iterator[types.check_msg_t]:
            yield 'error', f'in {self.id}: {msg}'
        from pygrpg.area import Area
        yield from super().check()
        area_id = self.attrs['start-area']
        location = self.attrs['start-location']
        if area_id is None:
            yield from err('start-area not set')
        if location is None:
            yield from err('start-location not set')
        if area_id is not None and location is not None:
            area = Area.get(area_id)
            if area is None:
                yield from err(f'start area {area_id} does not exist')
            else:
                area.load_content()
                if not area.has_location(location):
                    yield from err(
                        f'start area {area_id} ' +
                        f'does not have location {location}'
                    )
