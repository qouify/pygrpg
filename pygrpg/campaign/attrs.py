#!/usr/bin/env python3

"""Definition of class Attrs."""

import typing as tp
import pygwin as pw

from pygrpg.typing import types


DEFAULT = {
    'attr-points': 0,
    'heroes': 1,
    'max-hero-name-len': 20,
    'skill-points': 0
}

Attrs: tp.Callable[[types.campaign_attr_t], types.campaign_attr_t]
Attrs = pw.runtime_checked_dict.mk_runtime_checked_dict(
    tp.cast(tp.Type[types.campaign_attr_t], types.campaign_attr_t),
    default=DEFAULT
)
