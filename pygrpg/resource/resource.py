#!/usr/bin/env python3

"""Definition of class Resource."""

import os
import logging
import typing as tp
import re
import random
import itertools

from pygrpg.typing import types
from pygrpg.paths import Paths
from pygrpg.interfaces import Describable

RT = tp.TypeVar('RT', bound='Resource')


class Resource(Describable):
    """Document this."""

    ALL: tp.Dict[str, types.resource_t] = dict()
    SEP = '-'

    HAS_TEXT = True
    HAS_TEXT_SHORT = False
    HAS_ICON = False

    def __init__(
            self,
            rid: str,
            order: tp.Optional[int] = None
    ):
        self.id: str
        self.order: int

        self.id = rid
        if order is not None:
            self.order = order

    def __str__(self) -> str:
        return self.get_text_val()

    def __lt__(self, other: tp.Any) -> bool:
        if not isinstance(other, Resource):
            return False
        if self.has_order():
            if other.has_order():
                return self.order < other.order
            return True
        if other.has_order():
            return False
        return self.id < other.id

    def has_order(self) -> bool:
        """Check if the resource has an order."""
        return hasattr(self, 'order')

    @classmethod
    def join_id(cls, *ids: str) -> str:
        """Return the id composed of several components."""
        return cls.SEP.join(ids)

    def split_id(self) -> tp.List[str]:
        """Return the components of the self's id."""
        return self.id.split(self.SEP)

    def get_id_suffix(self) -> str:
        """Return the id suffix of self (i.e., remove the first word)."""
        return self.join_id(*self.split_id()[1:])

    def get_text(self) -> str:
        """Get the id of the text resource describing self."""
        return self.join_id('text', self.id)

    def get_text_val(self, title: bool = False, capital: bool = True) -> str:
        """Get the text value of self.get_text()."""
        from .text import Text
        return Text.val(self.get_text(), title=title, capital=capital)

    def get_text_short(self) -> str:
        """Get the id of the short text resource describing self."""
        return self.join_id('text', self.id, 'short')

    def get_text_short_val(
            self, title: bool = False, capital: bool = True
    ) -> str:
        """Get the text value of self.get_text_short()."""
        from .text import Text
        return Text.val(self.get_text_short(), title=title, capital=capital)

    def is_child(self, other: types.resource_t) -> bool:
        """Check if self is a child of resource other.

        self is a child of other if other.id prefixes its id.

        """
        return self.id.startswith(other.id)

    def has_text(self) -> bool:
        """Check if self has an associated text resource describing it."""
        return self.HAS_TEXT

    def has_text_short(self) -> bool:
        """Check if self has an associated short text resource."""
        return self.HAS_TEXT_SHORT

    def has_icon(self) -> bool:
        """Check if self has an associated icon file."""
        return self.HAS_ICON

    def get_content_dir(self) -> str:
        """Get the path of the directory in which self's data are stored."""
        return os.path.join(Paths['dir-content'], *self.split_id())

    def _get_media_file(self, **kwargs: tp.Any) -> tp.Optional[str]:
        media = kwargs['media']
        dirs = self.split_id()
        base_dir: str = Paths['dir-resources-media']
        exts = kwargs.get('extensions', [])
        while len(dirs) > 0:
            for ext in exts:
                result = os.path.join(base_dir, *dirs, media + ext)
                if os.path.isfile(result):
                    return result
            dirs.pop()
        return None

    def get_image_file(self, **kwargs: tp.Any) -> tp.Optional[str]:
        """Get the absolute path of an image file attached to self."""
        kwargs.setdefault('media', 'icon')
        return self._get_media_file(**{**kwargs, 'extensions': ['.png']})

    def get_image_file_(self, **kwargs: tp.Any) -> str:
        """Return get_image_file + check the result is not None."""
        result = self.get_image_file(**kwargs)
        if result is None:
            media = kwargs.get('media', 'icon')
            msg = f'image {media} of resource {self.id} not found'
            logging.error(msg)
            raise ValueError(msg)
        return result

    def get_media(
            self
    ) -> tp.Iterator[tp.Tuple[types.media_type_t, str]]:
        """Generate the list of media that are required for self."""
        if self.has_icon():
            yield 'image', 'icon'

    def get_exprs(self) -> types.resource_exprs_getter_t:
        """Generate all expressions that appear in the resource."""
        yield from []

    def check(self) -> tp.Iterator[types.check_msg_t]:
        """Check resource self."""
        #  check required text resources exist for self
        texts = [
            (self.get_text(), self.has_text()),
            (self.get_text_short(), self.has_text_short())
        ]
        for text, has in texts:
            if has and not Resource.exists(text):
                yield 'warning', f'missing text resource {text}'

        #  check required media exist for self
        for media_type, media in self.get_media():
            if media_type == 'image':
                if self.get_image_file(media=media) is None:
                    yield 'error', f'missing {media_type} \'{media}\''

        #  check expressions of the resource
        for e, v in self.get_exprs():
            yield from e.check(v)

    @classmethod
    def check_all(cls) -> None:
        """Check all resources.

        This does various checks (e.g., all required resources
        exist, all resources that must have an associated text have
        one).

        """
        from . import required

        colors = {
            'ok': '\033[92m',
            'error': '\033[91m',
            'warning': '\033[93m'
        }
        problems = {
            'error': 0,
            'warning': 0
        }

        msg_t = tp.Tuple[
            str, types.check_level_t, str
        ]

        #  all required resources exist
        def check_required() -> tp.Iterator[msg_t]:
            return (
                (rid, 'error', 'resource is missing')
                for rid in required.ALL
                if not Resource.exists(rid)
            )

        #  check all resources
        def check_resources() -> tp.Iterator[msg_t]:
            return (
                (r.id, lvl, msg)
                for r in Resource.get_all()
                for lvl, msg in r.check()
            )

        #  print messages
        for rid, lvl, msg in itertools.chain(
                check_required(),
                check_resources()
        ):
            problems[lvl] += 1
            color = colors.get(lvl, colors['error'])
            lvl_str = lvl.upper()
            prefix = f'[{lvl_str}:{rid}]'
            print(f'{color}{prefix} {msg}\033[0m')

        #  print summary
        if all(prb == 0 for prb in problems.values()):
            cok = colors['ok']
            print(f'{cok}Check passed!\033[0m')
        else:
            cerr = colors['error']
            cwarn = colors['warning']
            errs = problems['error']
            warns = problems['warning']
            print(f'{cwarn}{warns} warning(s)\033[0m and ', end='')
            print(f'{cerr}{errs} error(s)\033[0m found!')

    @classmethod
    def add(cls, res: types.resource_t) -> None:
        """Add res to the set of all resources."""
        Resource.ALL[res.id] = res

    @classmethod
    def exists(cls, rid: str) -> bool:
        """Check whether a loaded resource has id rid."""
        return cls.get(rid) is not None

    @classmethod
    def get(
            cls: tp.Type[RT],
            r: tp.Union[str, RT]
    ) -> tp.Optional[RT]:
        """Return the resource that has id r or r if it is a Resource.

        Return None if no resource has this id.

        """
        if isinstance(r, cls):
            return r
        if isinstance(r, str):
            if r[0] == '#':
                r = r[1:]
            result = Resource.ALL.get(r)
            prefix = cls.__name__.lower()
            if result is None and not r.startswith(prefix):
                result = Resource.ALL.get(cls.join_id(prefix, r))
            assert result is None or isinstance(result, cls)
            return result
        logging.error('resource \'%s\' not found', str(r))
        raise ValueError(f'invalid resource: {r}')

    @classmethod
    def get_(cls: tp.Type[RT], r: tp.Union[str, RT]) -> RT:
        """Return Resource.get + also check that the resource exists.

        Raise ValueError if the resource does not exist.

        """
        result = cls.get(r)
        if result is None:
            logging.error('resource \'%s\' not found', str(r))
            raise ValueError(f'invalid resource: {r}')
        return result

    @classmethod
    def get_all(cls: tp.Type[RT]) -> tp.Iterator[RT]:
        """Get all the resources of the class."""
        cls_name = cls.__name__.lower()
        for rid, res in cls.ALL.items():
            if rid.startswith(cls_name + cls.SEP):
                assert isinstance(res, cls)
                yield res
        for sub in cls.__subclasses__():
            yield from sub.get_all()

    @classmethod
    def pick_random(cls: tp.Type[RT], rid: str) -> tp.Optional[RT]:
        """Pick a random resource of this type with an id starting with rid.

        None is returned if no such resource is found.

        """
        reg_exp_id = re.compile(rid)
        reg_exp_full = re.compile(cls.join_id(cls.__name__.lower(), rid))
        try:
            choices = [
                res for res in cls.get_all()
                if reg_exp_id.match(res.get_id_suffix())
                or reg_exp_full.match(res.get_id_suffix())
            ]
            return random.choice(choices)
        except IndexError:
            return None

    ###########################################################################

    def gui_icon(self, **kwargs: tp.Any) -> types.pg.surface.Surface:
        """Draw self's image on a surface.

        image is drawn either on kwarg surface if not None or either
        on a new surface.  Kwarg size is the size of the drawn image.
        Return the surface.

        """
        import pygwin as pw
        import pygame as pg
        from pygrpg.gui import Util, Constants
        surface: tp.Optional[pg.surface.Surface] = kwargs.get('surface')
        size = kwargs.get('size')
        if size is None:
            size = Constants['icon-size']
        if surface is not None:
            result = surface
        else:
            result = Util.empty_tile(size=size)
        img = pw.Media.get_image(self.get_image_file_(), scale=size)
        if img is not None:
            result.blit(img, (0, 0))
        return result

    def gui_label(self, **kwargs: tp.Any) -> types.pw.Label:
        """Return a pygame surface containing resource's text description."""
        import pygwin as pw
        return pw.Label(
            self.get_text_val(),
            stc=self.gui_label_style_class(),
            **kwargs
        )

    @classmethod
    def gui_label_style_class(cls) -> tp.Optional[str]:
        """Return the pygwin style class name used for self's name label."""
        return None

    def gui_mknode(self, **kwargs: tp.Any) -> types.pw.Image:
        """Return pygwin Image containing self's icon.

        A tooltip is attached to the image if self has a description.

        """
        import pygame as pg
        import pygwin as pw
        from pygrpg.gui import Constants
        size = kwargs.get('size', Constants['icon-size'])
        result = pw.Image(pg.Surface(size).convert_alpha(), **kwargs)
        surface = result.surface
        surface.fill((0, 0, 0, 0))
        self.gui_icon(surface=surface)
        desc = self.gui_game_tooltip()
        if desc is not None:
            result.set_tooltip(desc)
        return result
