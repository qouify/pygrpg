#!/usr/bin/env python3

"""Definition of class Text."""

import typing as tp

from . import Resource


class Text(Resource):
    """Document this."""

    HAS_TEXT = False

    def __init__(
            self,
            rid: str,
            text: str
    ):
        """Document this."""
        Resource.__init__(self, rid)
        self.text = text

    @classmethod
    def val(
            cls,
            text_id: str,
            data: tp.Optional[tp.Union[str, tp.Sequence[str]]] = None,
            title: bool = False,
            capital: bool = True
    ) -> str:
        """Document this."""
        text = Text.get(text_id)
        if text is None:
            return text_id
        result = text.text
        if data is not None:
            try:
                result = result % data
            except TypeError:
                pass
        if title:
            result = result.title()
        elif capital:
            result = result.capitalize()
        return result
