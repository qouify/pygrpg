#!/usr/bin/env python3

"""Definition of class Loader."""

__all__ = [
    'load_game_resources'
]

import typing as tp
import os
import json
import csv
import logging

from pygrpg.being import Being, BeingAttr, Monster, Race
from pygrpg.being import DressingSlot, Dressing, Status
from pygrpg.effect import StatusChangeType
from pygrpg.item import ItemAttr, Item, EquipmentSlot, ItemAttack
from pygrpg.item import ItemDefense, ItemEquip, ItemUse
from pygrpg.media import MediaSource, Image
from pygrpg.rule import Rule
from pygrpg.skill import Skill, SkillCategory
from pygrpg.spell import Spell, SpellAttr
from pygrpg.area import Area, Object
from pygrpg.campaign import Campaign, Attrs as CampaignAttrs
from pygrpg.effect import Effect
from pygrpg.resource import Text
from pygrpg.expr import Parser as ExprParser, Range
from pygrpg.paths import Paths
from pygrpg.config import GameConfig as Cfg
from pygrpg.typing import types


def get_loaders() -> tp.Dict[
        tp.Type[types.resource_t],
        tp.Callable[[types.serialisation_t], types.resource_t]
]:
    """Get the functions required to load resources."""
    def load_requirements(
            data: types.serialisation_t
    ) -> tp.List[types.expr_t]:
        return [
            ExprParser.parse(e)
            for e in data.get('requirements', list())
        ]

    def load_effects(
            effects: tp.Optional[tp.List[types.serialisation_t]]
    ) -> tp.List[types.effect_t]:
        if effects is None:
            return list()
        return [Effect.unserialise(e) for e in effects]

    def load_range(
            rng: tp.List[str]
    ) -> types.range_t:
        return Range(
            ExprParser.parse(rng[0]), ExprParser.parse(rng[1])
        )

    def load_skill_ref(
            skill_id: tp.Optional[str]
    ) -> tp.Optional[types.skill_t]:
        return None if skill_id is None else Skill.get_(skill_id)

    def load_media_source(
            data: types.serialisation_t
    ) -> types.media_source_t:
        return MediaSource(
            rid=str(data['id']),
            name=str(data['name']),
            urls=[str(url) for url in data.get('urls', list())]
        )

    def load_image(data: types.serialisation_t) -> types.image_t:
        data.get('src')
        return Image(
            rid=str(data['id'])
        )

    def load_rule(data: types.serialisation_t) -> types.rule_t:
        return Rule(
            rid=str(data['id']),
            args=data.get('args', list()),
            expr=ExprParser.parse(data['expr'])
        )

    def load_status(
            data: types.serialisation_t
    ) -> types.status_t:
        return Status(
            rid=str(data['id'])
        )

    def load_equipment_slot(
            data: types.serialisation_t
    ) -> types.equipment_slot_t:
        return EquipmentSlot(
            rid=str(data['id']),
            weapon_slot=data.get('weapon_slot', False)
        )

    def load_being_attr(
            data: types.serialisation_t
    ) -> types.being_attr_t:
        return BeingAttr(
            rid=str(data['id'])
        )

    def load_dressing_slot(
            data: types.serialisation_t
    ) -> types.dressing_slot_t:
        return DressingSlot(
            rid=str(data['id'])
        )

    def load_dressing(
            data: types.serialisation_t
    ) -> types.dressing_t:
        return Dressing(
            rid=str(data['id']),
            slot=DressingSlot.get_(data['slot']),
            sexs=set(data.get('sexs', list()))
        )

    def load_skill_category(
            data: types.serialisation_t
    ) -> types.skill_category_t:
        return SkillCategory(
            rid=str(data['id'])
        )

    def load_skill(
            data: types.serialisation_t
    ) -> types.skill_t:
        return Skill(
            rid=str(data['id']),
            attr=BeingAttr.get_(data['attr']),
            category=SkillCategory.get_(data['category'])
        )

    def load_race(
            data: types.serialisation_t
    ) -> types.race_t:
        def get_attrs(attrs: tp.Dict[str, int]) -> types.race_attrs_t:
            return {BeingAttr.get_(attr): val for attr, val in attrs.items()}
        return Race(
            rid=str(data['id']),
            attrs_min=get_attrs(data['min']),
            attrs_max=get_attrs(data['max']),
            attrs_default=get_attrs(data['default'])
        )

    def load_status_change_type(
            data: types.serialisation_t
    ) -> types.status_change_type_t:
        return StatusChangeType(
            rid=str(data['id'])
        )

    def load_spell_attr(
            data: types.serialisation_t
    ) -> types.spell_attr_t:
        return SpellAttr(
            rid=str(data['id'])
        )

    def load_spell(
            data: types.serialisation_t
    ) -> types.spell_t:
        caster_effects = load_effects(data.get('caster_effects'))
        target_effects = load_effects(data.get('effects'))
        rng = load_range(data.get('range', ['1', '1.5']))
        area = ExprParser.parse(data.get('area', '1'))
        delay = ExprParser.parse(data.get('delay', '0'))
        attrs = {
            SpellAttr.get_(a): int(v)
            for a, v in data.items()
            if SpellAttr.exists(a)
        }
        return Spell(
            rid=str(data['id']),
            attrs=attrs,
            skill=Skill.get_(data['skill']),
            caster_effects=caster_effects,
            target_effects=target_effects,
            rng=rng,
            area=area,
            where=data.get('where', 'area'),
            when=data.get('when', 'any'),
            target_type=data.get('target_type', 'any'),
            card=data.get('card', 'one'),
            delay=delay,
            requirements=load_requirements(data)
        )

    def load_item_attr(
            data: types.serialisation_t
    ) -> types.item_attr_t:
        return ItemAttr(
            rid=str(data['id'])
        )

    def load_item(
            data: types.serialisation_t
    ) -> types.item_t:
        def load_item_attack(
                data: types.serialisation_t
        ) -> types.item_attack_t:
            rng = load_range(data.get('range', ['1', '1.5']))
            delay_expr = data.get('delay')
            delay = (
                None if delay_expr is None
                else ExprParser.parse(delay_expr)
            )
            return ItemAttack(
                effects=load_effects(data.get('effects')),
                requirements=load_requirements(data),
                rng=rng,
                delay=delay
            )

        def load_item_defense(
                data: types.serialisation_t
        ) -> types.item_defense_t:
            return ItemDefense(
                effects=load_effects(data.get('effects')),
                requirements=load_requirements(data)
            )

        def load_item_use(data: types.serialisation_t) -> types.item_use_t:
            return ItemUse(
                effects=load_effects(data.get('effects')),
                requirements=load_requirements(data)
            )

        def load_item_equip(data: types.serialisation_t) -> types.item_equip_t:
            allowed_slots = [
                EquipmentSlot.get_(s)
                for s in data.get('allowed_slots', list())
            ]
            return ItemEquip(
                effects=load_effects(data.get('effects')),
                requirements=load_requirements(data),
                allowed_slots=allowed_slots
            )
        skill = load_skill_ref(data.get('skill'))
        char_loaders = {
            'attack': load_item_attack,
            'defense': load_item_defense,
            'equip': load_item_equip,
            'use': load_item_use
        }
        char = {
            char: None if char not in data else loader(data[char])
            for char, loader in char_loaders.items()
        }
        attack = tp.cast(tp.Optional[types.item_attack_t], char['attack'])
        defense = tp.cast(tp.Optional[types.item_defense_t], char['defense'])
        equip = tp.cast(tp.Optional[types.item_equip_t], char['equip'])
        use = tp.cast(tp.Optional[types.item_use_t], char['use'])
        attrs = {
            ItemAttr.get_(a): int(v)
            for a, v in data.items()
            if ItemAttr.exists(a)
        }
        return Item(
            rid=str(data['id']),
            max_count=data.get('max_count', 1),
            attrs=attrs,
            skill=skill,
            attack=attack,
            defense=defense,
            equip=equip,
            use=use
        )

    def load_monster(
            data: types.serialisation_t
    ) -> types.monster_t:
        being_data = Being.unserialise_being_data(data)
        attrs, equip, skills, spells, effects = being_data
        return Monster(
            rid=str(data['id']),
            attrs=attrs,
            equipment=equip,
            skills=skills,
            spells=spells,
            effects=effects
        )

    def load_object(
            data: types.serialisation_t
    ) -> types.object_t:
        return Object(
            rid=str(data['id']),
            depth=int(data.get('depth', 0)),
            opaque=bool(data.get('opaque', False)),
            walkable=bool(data.get('walkable', False)),
            blocking=bool(data.get('blocking', False)),
            door=bool(data.get('door', False)),
            chest=bool(data.get('chest', False))
        )

    def load_campaign(
            data: types.serialisation_t
    ) -> types.campaign_t:
        return Campaign(
            rid=str(data['id']),
            attrs=CampaignAttrs(data.get('attrs', dict()))
        )

    def load_area(
            data: types.serialisation_t
    ) -> types.area_t:
        return Area(
            rid=str(data['id']),
            campaign=Campaign.get_(data['campaign'])
        )

    def load_text(
            data: types.serialisation_t
    ) -> types.text_t:
        return Text(
            rid=str(data['id']),
            text=str(data['text'])
        )
    return {
        MediaSource: load_media_source,
        Image: load_image,
        Rule: load_rule,
        Status: load_status,
        EquipmentSlot: load_equipment_slot,
        BeingAttr: load_being_attr,
        DressingSlot: load_dressing_slot,
        Dressing: load_dressing,
        SkillCategory: load_skill_category,
        Skill: load_skill,
        Race: load_race,
        StatusChangeType: load_status_change_type,
        SpellAttr: load_spell_attr,
        Spell: load_spell,
        ItemAttr: load_item_attr,
        Item: load_item,
        Monster: load_monster,
        Object: load_object,
        Campaign: load_campaign,
        Area: load_area,
        Text: load_text
    }


FILES = {
    'TEMPLATE': 'template.json',
    'RESOURCE': 'resources',
    'STYLE': 'style.json',
    'GUI': 'gui.json'
}
LOADERS = get_loaders()
RES_ORDER: tp.Dict[tp.Type[types.resource_t], int] = dict()


def load_data(
        data: types.serialisation_t,
        rcls: tp.Type[types.resource_t],
        prefix: tp.List[str],
        **kwargs: tp.Any
) -> int:
    """Load a single resource file."""
    result = 0
    base = kwargs.pop('base')
    joined_prefix = '-'.join([s.lower() for s in prefix])
    for res_id, res_data in data.items():
        res_data = {
            **base,
            **res_data,
            'id': f'{joined_prefix}-{res_id}'
        }
        if rcls not in RES_ORDER:
            RES_ORDER[rcls] = 1
        res_data['order'] = RES_ORDER[rcls]
        res = LOADERS[rcls](res_data)
        rcls.add(res)
        RES_ORDER[rcls] += 1
        result += 1
    return result


def load_dir(
        rdir: str,
        rcls: tp.Type[types.resource_t],
        prefix: tp.List[str],
        **kwargs: tp.Any
) -> None:
    """Recursively load a resource directory."""
    #  process the template file of the directory.  this one contains
    #  data that are shared by all resources in the directory
    base = kwargs.pop('base', dict())
    full = os.path.join(rdir, FILES['TEMPLATE'])
    if os.path.isfile(full):
        logging.info('loading template from %s', full)
        with open(full, 'r', encoding="utf8") as fd:
            data = json.loads(fd.read())
        base = {**base, **data}

    #  process json files and subdirectories of this directory
    for item in os.listdir(rdir):
        full = os.path.join(rdir, item)
        if os.path.isdir(full):
            prefix.append(item)
            load_dir(full, rcls, prefix, base=base, **kwargs)
            prefix.pop()
        else:
            if item != FILES['TEMPLATE']:
                ext = os.path.splitext(full)[1]
                if ext in ['.json', '.csv']:
                    with open(full, 'r', encoding="utf8") as fd:
                        if ext == '.json':
                            data = json.loads(fd.read())
                        else:
                            data = dict()
                            for row in csv.DictReader(fd, delimiter=';'):
                                if 'id' in row:
                                    rid = row.pop('id')
                                    data[rid] = row
                    msg = f'loading resources from {full}'
                    logging.info(msg)
                    no = load_data(data, rcls, prefix, base=base, **kwargs)
                    msg = f'loaded {no} resources from {full}'
                    logging.info(msg)


def load_game_resources() -> None:
    """Load all game resources."""
    lang = str(Cfg['language'])
    path_t = tp.Tuple[tp.Type[types.resource_t], str]
    rdirs: tp.List[path_t] = [
        *[(rcls,
           os.path.join(Paths['dir-resources'], rcls.__name__.lower()))
          for rcls in LOADERS if rcls != Text],
        (Text, os.path.join(Paths['dir-resources'], 'text', lang))
    ]
    for rcls, rdir in rdirs:
        if os.path.isdir(rdir):
            load_dir(rdir, rcls, [rcls.__name__.lower()])
