#!/usr/bin/env python3

"""Document this."""

from .resource import Resource
from .text import Text

__all__ = [
    'Resource',
    'Text'
]
