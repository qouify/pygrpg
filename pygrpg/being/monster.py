#!/usr/bin/env python3

"""Definition of class Monster."""

import typing as tp

from pygrpg.interfaces import Instanciable
from pygrpg.resource import Resource
from pygrpg.typing import types
from pygrpg.item import Equipment
from . import Being


class Monster(Resource, Being, Instanciable):
    """Document this."""

    HAS_ICON = True

    def __init__(
            self,
            rid: str,
            attrs: types.being_attrs_t,
            equipment: types.equipment_t,
            skills: types.being_skills_t,
            spells: types.being_spells_t,
            effects: types.being_effects_t
    ):
        """Document this."""
        Resource.__init__(self, rid)
        Being.__init__(
            self,
            attrs=attrs,
            skills=skills,
            equipment=equipment,
            spells=spells,
            effects=effects
        )

    def get_name(self) -> str:
        return self.get_text_val()

    @classmethod
    def unserialise(cls, data: types.serialisation_t) -> types.monster_t:
        result = Monster.get_(data['id']).instanciate()
        result.unserialise_fellows(data)
        return result

    def serialise(self) -> types.serialisation_t:
        return {
            **super().serialise(),
            'id': self.id
        }

    def instanciate(self) -> types.monster_t:
        equipment = Equipment({
            slot: item.instanciate()
            for slot, item in self.equipment.items()
        })
        result = Monster(
            rid=self.id,
            attrs=self.attrs.copy(),
            equipment=equipment,
            skills=self.skills.copy(),
            spells=self.spells.copy(),
            effects=list(self.effects)
        )
        result.init_status()
        return result

    ###########################################################################

    def _gui_game_tooltip(
            self,
            **kwargs: tp.Any
    ) -> tp.Optional[types.pw.Node]:
        from pygrpg.gui import Util
        fellows = list(self.get_fellows())
        return Util.resource_description_grid(fellows)

    def gui_editor_tooltip(
            self,
            **kwargs: tp.Any
    ) -> tp.Optional[types.pw.Node]:
        import pygwin as pw
        result = pw.Box()
        nodes = [self.gui_game_tooltip()]
        inv = self.get_inventory()
        if not inv.is_empty():
            nodes.append(inv.gui_editor_tooltip())
        result.pack(*[n for n in nodes if n is not None])
        return result
