#!/usr/bin/env python3

"""Definition of class Race."""

from pygrpg.resource import Resource
from pygrpg.typing import types


class Race(Resource):
    """Document this."""

    def __init__(
            self,
            rid: str,
            attrs_min: types.race_attrs_t,
            attrs_max: types.race_attrs_t,
            attrs_default: types.race_attrs_t
    ):
        """Document this."""
        self.min: types.race_attrs_t
        self.max: types.race_attrs_t
        self.default: types.race_attrs_t

        Resource.__init__(self, rid)
        self.min = attrs_min
        self.max = attrs_max
        self.default = attrs_default

    def get_image_sex(self, sex: types.hero_sex_t) -> str:
        """Document this."""
        result = self.get_image_file(media=sex)
        assert result is not None
        return result

    def get_min_attr(self, attr: types.being_attr_t) -> int:
        """Document this."""
        return self.min[attr]

    def get_max_attr(self, attr: types.being_attr_t) -> int:
        """Document this."""
        return self.max[attr]

    def get_default_attr(self, attr: types.being_attr_t) -> int:
        """Document this."""
        return self.default[attr]
