#!/usr/bin/env python3

"""Definition of class Being."""

import typing as tp
import abc

from pygrpg.item import Equipment, Item, Inventory
from pygrpg.rule import Rule
from pygrpg.resource import Resource
from pygrpg.attr import HasAttrs
from pygrpg.spell import Spell
from pygrpg.skill import Skill
from pygrpg.being import BeingAttr
from pygrpg.effect import Effect
from pygrpg.typing import types
from pygrpg.util import Directions
from pygrpg.area import AreaObject
from pygrpg.interfaces import Serialisable
from . import Status

BT = tp.TypeVar('BT', bound='Being')


class Being(
        AreaObject,
        Serialisable,
        HasAttrs[BeingAttr, int],
        abc.ABC
):
    """Document this."""

    def __init__(
            self: BT,
            attrs: types.being_attrs_t,
            skills: types.being_skills_t,
            equipment: tp.Optional[types.equipment_t] = None,
            spells: tp.Optional[types.being_spells_t] = None,
            effects: tp.Optional[types.being_effects_t] = None,
            direction: types.direction_t = Directions.SOUTH
    ):
        """Document this."""
        self.busy: bool
        self.cell: types.cell_t
        self.direction = direction
        self.effects: types.being_effects_t
        self.equipment: types.equipment_t
        self._group: 'types.group_t[BT]'
        self.skills: types.being_skills_t
        self.spells: types.being_spells_t
        self.statuses: tp.Dict[types.status_t, int]

        AreaObject.__init__(self)
        HasAttrs.__init__(self, attrs)
        self.equipment = equipment if equipment is not None else Equipment()
        self.skills = skills
        self.spells = spells if spells is not None else set()
        self.effects = effects if effects is not None else list()
        self.busy = False
        self.direction = direction

    @property
    def group(self: BT) -> 'types.group_t[BT]':
        """Get self's group."""
        return self._group

    @group.setter
    def group(self: BT, group: 'types.group_t[BT]') -> None:
        """Set self's group."""
        self._group = group

    @property
    def visibility(self) -> int:
        """Get self's visibility."""
        return 20

    def get_cell(self) -> tp.Optional[types.cell_t]:
        return self.cell

    def get_depth(self) -> int:
        return AreaObject.DEPTH_BEING

    def is_blocking(self) -> bool:
        return True

    def is_dynamic(self) -> bool:
        return True

    def set_cell(self, cell: tp.Optional[types.cell_t]) -> None:
        """Put self on cell."""
        if cell is None:
            if hasattr(self, 'cell'):
                del self.cell
        else:
            self.cell = cell

    def set_direction(self, direction: types.direction_t) -> None:
        """Change self's direction."""
        self.direction = direction

    def set_busy(self, busy: bool) -> None:
        """Set self's busy flag."""
        self.busy = busy

    def get_fellows(self: BT) -> tp.Iterator[BT]:
        """Get self's fellows (beings of the same group)."""
        yield from self.group.members

    def get_inventory(self) -> types.inventory_t:
        """Get the inventory of self's group."""
        return self.group.inventory

    def init_status(self) -> None:
        """Initialize all statuses of self."""
        self.statuses = {
            status: self.get_base_status(status)
            for status in Status.get_all()
        }

    def get_status(self, status: types.status_t) -> int:
        """Get the value of the status for self."""
        return self.statuses[status]

    def get_base_status(self, status: types.status_t) -> int:
        """Document this."""
        return 100

    def get_weapon(self) -> types.item_t:
        """Document this."""
        return next(
            (it for slot, it in self.equipment.items() if slot.weapon_slot),
            Item.get_('#item-weapon-unarmed')
        )

    def _unequip(self, item: types.item_t) -> None:
        self.equipment.remove(item)
        for effect in item.equip.effects:
            effect.unrealise(being=self, src=item)

    def equip(
            self,
            slot: types.equipment_slot_t,
            item: types.item_t
    ) -> tp.List[types.item_t]:
        """Make self equip item at the given slot."""

        unequipped = self.equipment.insert(slot, item)
        for old_item in unequipped:
            self._unequip(old_item)

        #  add the equip-effects of the item to self
        for effect in item.equip.effects:
            if effect.is_immediate():
                effect.realise(being=self, src=item)

        #  update the inventory
        inv = self.get_inventory()
        inv.del_item(item)
        for old_item in unequipped:
            inv.add_item(old_item)

        return unequipped

    def unequip(self, slot: types.equipment_slot_t) -> None:
        """Make self unequip the item equipped at slot, if any."""
        item = self.equipment.get(slot)
        if item is not None:
            self.get_inventory().add_item(item)
            self._unequip(item)

    def get_image_files(self) -> tp.Iterator[str]:
        """Document this."""
        yield self.get_image_file_()
        yield from self._get_image_files_equipment()

    def _get_image_files_equipment(self) -> tp.Iterator[str]:
        for slot in self.equipment:
            img_file = self.equipment[slot].get_area_image_file(slot)
            if img_file is not None:
                yield img_file

    def get_skill_level(self, skill: types.skill_t) -> int:
        """Get the self's level for skill."""
        return self.skills.get(skill, 0)

    def update_status(self, status: types.status_t, no: int) -> None:
        """Document this."""
        self.statuses[status] = min(
            self.get_base_status(status),
            max(0, self.statuses[status] + no)
        )

    def is_dead(self) -> bool:
        """Check if self is dead using rule #rule-death-check."""
        return bool(Rule.get_('#rule-death-check').eval({'being': self}))

    def get_active_effects(self) -> tp.Iterator[types.being_effect_t]:
        """Document this."""
        for item in self.equipment.values():
            for effect in item.equip.effects:
                yield item, effect
        yield from self.effects

    def add_effect(
            self,
            effect: types.effect_t,
            src: tp.Any = None
    ) -> None:
        """Document this."""
        self.effects.append((src, effect))

    def new_turn(self) -> tp.List[types.effect_t]:
        """Document this."""
        result: tp.List[types.effect_t] = []

        #  put in result immediate active effects
        for _, effect in self.get_active_effects():
            effect.new_turn()
            if effect.is_immediate():
                result.append(effect)

        #  remove done effects from self.effects
        self.effects = [e for e in self.effects if not e[1].is_done()]
        return result

    def knows_spells(self) -> bool:
        """Check if the being knows at least one spell."""
        return self.spells != set()

    def can_equip(self, item: types.item_t) -> bool:
        """Check whether self can equip item.

        All requirements must be fullfiled by the being for item to be
        equipped.

        """
        return all(
            requirement.eval({'being': self})
            for requirement in item.equip.requirements
        )

    def serialise(self) -> types.serialisation_t:
        result = {
            **super().serialise()
        }
        if self.group is not None and self.group.leader == self:
            result = {
                **result,
                'direction': self.direction,
                'inventory': self.group.inventory.serialise(),
                'fellows': [
                    m.serialise() for m in self.get_fellows() if m != self
                ]
            }
        return result

    def unserialise_fellows(self: BT, data: types.serialisation_t) -> None:
        """Document this."""
        from . import Group, Hero, Monster
        fellows = data.get('fellows')
        if fellows is None:
            return
        get = {
            'Hero': Hero.unserialise,
            'Monster': Monster.unserialise
        }[data['type']]
        members: tp.List[BT] = [self, *[get(m) for m in fellows]]
        inventory = Inventory.unserialise(data['inventory'])
        direction = (int(data['direction'][0]), int(data['direction'][1]))
        self.set_direction(direction)
        grp = Group(members=members, inventory=inventory, leader=0)
        self.group = grp

    @classmethod
    def unserialise_being_data(
            cls,
            data: types.serialisation_t
    ) -> tp.Tuple[
        types.being_attrs_t,
        types.equipment_t,
        types.being_skills_t,
        types.being_spells_t,
        types.being_effects_t
    ]:
        """Document this."""
        attrs = {
            BeingAttr.get_(attr): val
            for attr, val in data.get('attrs', dict()).items()
        }
        equipment = Equipment.unserialise(data.get('equipment', dict()))
        skills = {
            Skill.get_(sk): lvl
            for sk, lvl in data.get('skills', dict()).items()
        }
        spells = {
            Spell.get_(s)
            for s in data.get('spells', list())
        }
        effects = [
            (Resource.get(src), Effect.unserialise(**e))
            for src, e in data.get('effects', list())
        ]
        return attrs, equipment, skills, spells, effects

    @abc.abstractmethod
    def get_image_file_(self) -> str:
        """Document this."""

    @abc.abstractmethod
    def get_name(self) -> str:
        """Document this."""

    ###########################################################################

    def gui_draw(self, **kwargs: tp.Any) -> types.pg.surface.Surface:
        from pygrpg.gui import Util, Constants

        result = super().gui_draw(**kwargs)
        kwargs.get('draw_bars', False)
        pos = kwargs.get('pos', (0, 0))
        kwargs.get('size', Constants['icon-size'])
        # if draw_bars:
        #    cb: int = Constants['area-panel-cell-border']
        #    bw: int = Constants['area-panel-hp-bar-width']
        #    x = pos[0] + cb
        #    y = pos[1] + size[1] - (cb + 2 * bw)
        #    for ratio, color in [
        #            (self.hp / self.base_hp(), Constants['color-hp-bar']),
        #            (self.mp / self.base_mp(), Constants['color-mp-bar'])
        #    ]:
        #        bar_size = int((size[0] - 2 * cb) * ratio)
        #        pw.Draw.rectangle(result, color, (x, y, bar_size, bw))
        #        y += bw
        if self.busy:
            img = Util.get_predefined_image('#image-action-delayed')
            if img is not None:
                result.blit(img, pos)
        return result
