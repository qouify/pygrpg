#!/usr/bin/env python3

"""Definition of class DressingSlot."""

from pygrpg.resource import Resource


class DressingSlot(Resource):
    """A DressingSlot is any body slot that can support a Dressing.

    Examples are: head, legs, arms, ...

    """
