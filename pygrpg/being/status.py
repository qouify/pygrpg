#!/usr/bin/env python3

"""Definition of class Status."""

from pygrpg.resource import Resource


class Status(Resource):
    """A Status resource serves to gauge a being state.

    Examples of status are health points, magic points, energy
    points...

    """

    HAS_TEXT_SHORT = True
