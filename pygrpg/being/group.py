#!/usr/bin/env python3

"""Definition of class Group."""

import typing as tp


from pygrpg.typing import types
from pygrpg.item import Inventory

BT = tp.TypeVar('BT', bound=types.being_t)


class Group(tp.Generic[BT]):
    """A Group is gathering of beings of the same Being sub-class."""

    def __init__(
            self,
            members: tp.Iterable[BT],
            inventory: tp.Optional[types.inventory_t] = None,
            leader: int = 0
    ):
        """Initialise self.

        Argument members is the list of members in the group.

        Argument inventory is its inventory.  If None self's inventory
        is initialised to an empty inventory.

        Argument leader is the index in members of the group leader.

        """
        self.members: tp.List[BT] = list(members)
        self.leader = self.members[leader]
        self.inventory = inventory if inventory is not None else Inventory()
        for m in self.members:
            m.group = self

    @property
    def visibility(self) -> int:
        """Document this."""
        return max(m.visibility for m in self.members)

    def remove(self, member: BT) -> None:
        """Remove member from self."""
        if member == self.leader:
            self.leader = self.members[0]
        self.members.remove(member)

    def is_in(self, member: BT) -> bool:
        """Check member belongs to self."""
        return member in self.members

    def is_empty(self) -> bool:
        """Check if self is empty."""
        return self.members == []

    def add_member(self, member: BT) -> None:
        """Add a member to self."""
        self.members.append(member)
        if len(self.members) == 1:
            self.leader = member
        member.group = self
