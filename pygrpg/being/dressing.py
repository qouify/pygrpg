#!/usr/bin/env python3

"""Definition of class Dressing."""

import typing as tp

from pygrpg.resource import Resource
from pygrpg.typing import types


class Dressing(Resource):
    """A Dressing object is anything that can alter an hero appearance.

    Examples are: hairs, shirts, pants...  A Dressing must not be
    confused with an equipment item.  It only alters the hero
    appearance.

    """

    HAS_ICON = True
    HAS_TEXT = False

    def __init__(
            self,
            rid: str,
            slot: types.dressing_slot_t,
            sexs: tp.Set[types.hero_sex_t]
    ):
        """Initialise self.

        Argument slot is the DressingSlot where self can appear.

        Argument sexs is the set of sexs for which self is available.

        """
        Resource.__init__(self, rid)
        self.slot = slot
        self.sexs = sexs

    @classmethod
    def available(
            cls,
            slot: types.dressing_slot_t,
            sex: tp.Optional[types.hero_sex_t] = None
    ) -> tp.Iterator[types.dressing_t]:
        """Generate all Dressings available for a specific DressingSlot.

        If sex is not None, do not generate Dressings that are not
        available for that sex.

        """
        for d in cls.get_all():
            if d.slot == slot and (sex is None or sex in d.sexs):
                yield d
