#!/usr/bin/env python3

"""Definition of class Hero."""

import typing as tp

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.skill import Skill
from . import Being, Dressing, DressingSlot, Race


class Hero(Being):
    """A Hero is a being that is controlled by the player."""

    SEXS: tp.Set[types.hero_sex_t] = {
        'female',
        'male'
    }

    def __init__(
            self,
            attrs: types.being_attrs_t,
            skills: types.being_skills_t,
            name: str,
            race: types.race_t,
            sex: types.hero_sex_t,
            dressings: types.hero_dressings_t,
            equipment: tp.Optional[types.equipment_t] = None,
            spells: tp.Optional[types.being_spells_t] = None,
            effects: tp.Optional[types.being_effects_t] = None,
            xp: tp.Optional[types.hero_xps_t] = None
    ):
        Being.__init__(
            self,
            attrs=attrs,
            skills=skills,
            equipment=equipment,
            spells=spells,
            effects=effects
        )
        self.race: types.race_t = race
        self.name: str = name
        self.sex: types.hero_sex_t = sex
        self.xp: types.hero_xps_t = xp if xp is not None else dict()
        self.dressings: types.hero_dressings_t = dressings
        self.init_status()

    def get_name(self) -> str:
        return self.name

    def get_xp(self, skill: Skill) -> int:
        """Get self's experience points in a specific skill."""
        return self.xp.get(skill, 0)

    def get_sex_text(self) -> str:
        """Return the resource id of self's sex text."""
        return f'#text-ui-word-{self.sex}'

    def get_image_file_(self) -> str:
        return self.race.get_image_sex(self.sex)

    def get_xp_for_next_level(self, skill: types.skill_t) -> int:
        """Return the number of xps required for the next level of skill."""
        return glob.campaign().get_xp_for_level(
            self.get_skill_level(skill) + 1
        )

    def gain_xp(self, xp: types.hero_xps_t) -> tp.Dict[Skill, int]:
        """Make the hero gains experience points.

        Return a Dict[Skill, int] indicating how much levels have been
        gained.

        """
        result = dict()
        for skill, skill_xp in xp.items():
            skill_lvl = self.skills.setdefault(skill, 0)
            if skill in self.xp:
                skill_xp += self.xp[skill]
            old_lvl = skill_lvl
            while skill_xp >= glob.campaign().get_xp_for_level(skill_lvl + 1):
                skill_xp -= glob.campaign().get_xp_for_level(skill_lvl + 1)
                skill_lvl += 1
            if skill_lvl > old_lvl:
                result[skill] = skill_lvl - old_lvl
            self.skills[skill] = skill_lvl
            self.xp[skill] = skill_xp
        return result

    def get_image_files(self) -> tp.Iterator[str]:
        yield self.get_image_file_()
        for d in self.dressings.values():
            if d is not None:
                yield d.get_image_file_()
        yield from self._get_image_files_equipment()

    def serialise(self) -> types.serialisation_t:
        return {
            **super().serialise(),
            'equipment': self.equipment.serialise(),
            'attrs': {
                attr.id: val
                for attr, val in self.attrs.items()
            },
            'skills': {
                skill.id: lvl
                for skill, lvl in self.skills.items()
            },
            'spells': [
                spell.id
                for spell in self.spells
            ],
            'effects': [
                [None if res is None else res.id, eff.serialise()]
                for res, eff in self.effects
            ],
            'race': self.race.id,
            'name': self.name,
            'sex': self.sex,
            'xp': {skill.id: xp for skill, xp in self.xp.items()},
            'dressings': {p.id: d.id for p, d in self.dressings.items()}
        }

    @classmethod
    def unserialise(cls, data: types.serialisation_t) -> types.hero_t:
        being_data = Being.unserialise_being_data(data)
        attrs, equip, skills, spells, effects = being_data
        result = Hero(
            attrs=attrs,
            equipment=equip,
            skills=skills,
            spells=spells,
            effects=effects,
            name=data['name'],
            race=Race.get_(data['race']),
            sex=data['sex'],
            xp={
                Skill.get_(skill): xp
                for skill, xp in data['xp'].items()
            },
            dressings={
                DressingSlot.get_(slot): Dressing.get_(dressing)
                for slot, dressing in data['dressings'].items()
            }
        )
        result.unserialise_fellows(data)
        return result
