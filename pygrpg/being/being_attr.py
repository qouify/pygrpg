#!/usr/bin/env python3

"""Definition of class BeingAttr."""

from pygrpg.attr import Attr


class BeingAttr(Attr):
    """A BeingAttr is a basic characteristic of a being.

    For examples: Strength, Intelligence.

    """

    TEXT_SHORT = True
