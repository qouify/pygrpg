#!/usr/bin/env python3

"""Document this."""

import typing as tp

from pygrpg.typing import types


GAME: tp.Optional[types.game_t] = None
MAIN: tp.Optional['types.main_t[types.area_node_t]'] = None


def game() -> types.game_t:
    """Document this."""
    assert GAME is not None
    return GAME


def main() -> 'types.main_t[types.area_node_t]':
    """Document this."""
    assert MAIN is not None
    return MAIN


def main_game() -> types.main_game_t:
    """Document this."""
    from pygrpg.gui import MainGame
    assert MAIN is not None and isinstance(MAIN, MainGame)
    return MAIN


def ctx() -> tp.Optional[types.context_t]:
    """Document this."""
    if GAME is None:
        return None
    return GAME.ctx


def area_ctx() -> types.area_context_t:
    """Document this."""
    from pygrpg.context import AreaContext
    assert GAME is not None and isinstance(GAME.ctx, AreaContext)
    return GAME.ctx


def fight_ctx() -> types.fight_context_t:
    """Document this."""
    from pygrpg.context import FightContext
    assert GAME is not None and isinstance(GAME.ctx, FightContext)
    return GAME.ctx


def campaign() -> types.campaign_t:
    """Document this."""
    assert GAME is not None
    return GAME.campaign


def area_node() -> types.area_node_t:
    """Document this."""
    assert MAIN is not None
    return MAIN.get_area_node()


def area_game_node() -> types.area_game_node_t:
    """Document this."""
    from pygrpg.gui import AreaGameNode
    result = area_node()
    assert isinstance(result, AreaGameNode)
    return result


def area_panel() -> 'types.area_panel_t[types.area_node_t]':
    """Document this."""
    assert MAIN is not None
    return MAIN.area_panel


def in_unsaved_game() -> bool:
    """Document this."""
    return GAME is not None and GAME.not_saved()


def close_game() -> None:
    """Document this."""
    global GAME  # pylint: disable=W0603

    if GAME is not None:
        GAME.close()
        GAME = None
