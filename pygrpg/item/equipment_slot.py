#!/usr/bin/env python3

"""Definition of class EquipmentSlot."""

from pygrpg.resource import Resource


class EquipmentSlot(Resource):
    """An EquipmentSlot corresponds to a being's part that can hold items.

    For example: chest, right hand, head.

    """

    def __init__(self, rid: str, weapon_slot: bool) -> None:
        """Initialise self.

        weapon_slot is True, if the slot can hold a weapon that can be
        used in attack.

        """
        self.weapon_slot: bool

        Resource.__init__(self, rid)
        self.weapon_slot = weapon_slot
