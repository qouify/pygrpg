#!/usr/bin/env python3

"""Definition of class ItemDefense."""

from .item_characteristics import ItemCharacteristics


class ItemDefense(ItemCharacteristics):
    """An ItemDefense object contains defense attributes of an Item."""
