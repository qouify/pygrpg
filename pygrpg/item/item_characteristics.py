#!/usr/bin/env python3

"""Definition of class ItemCharacteristics."""

import typing as tp

from pygrpg.typing import types


class ItemCharacteristics:
    """ItemCharacteristics encodes Item characteristics in some context.

    A context can be attack (ItemAttack), equip (ItemEquip), ...

    """

    def __init__(
            self,
            effects: tp.Iterable[types.effect_t],
            requirements: tp.Iterable[types.expr_t]
    ):
        self.effects: tp.List[types.effect_t]
        self.requirements: tp.List[types.expr_t]

        self.effects = list(effects)
        self.requirements = list(requirements)
