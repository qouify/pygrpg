#!/usr/bin/env python3

"""Definition of class Inventory."""

import typing as tp

from pygrpg.typing import types
from pygrpg.interfaces import Serialisable, Describable
from . import Item


class Inventory(Serialisable, Describable):
    """Document this."""

    def __init__(
            self,
            items: tp.Optional[tp.List[types.item_t]] = None
    ):
        """Document this."""
        self.items: tp.List[types.item_t]

        self.items = items if items is not None else list()

    def get_image_files(self) -> tp.Iterator[str]:
        """Document this."""
        for item in self.items:
            img_file = item.get_image_file()
            if img_file is not None:
                yield img_file

    def is_empty(self) -> bool:
        """Document this."""
        return self.items == []

    def add_inventory(self, inv: types.inventory_t) -> None:
        """Document this."""
        for item in inv.items:
            self.add_item(item)

    def empty(self) -> None:
        """Document this."""
        self.items = []

    def add_item(self, item: types.item_t) -> None:
        """Document this."""
        pos = None
        for i, it in enumerate(self.items):
            if it.id == item.id:
                if it.count < it.max_count:
                    to_add = it.max_count - it.count
                    to_add = min(to_add, item.count)
                    it.add_count(to_add)
                    if to_add == item.count:
                        return
                    item.rem_count(to_add)
            if item < it:
                pos = i
                break
        if pos is None:
            pos = len(self.items)
        self.items.insert(pos, item)

    def del_item(self, item: types.item_t) -> None:
        """Document this."""
        self.items = [it for it in self.items if it != item]

    def clean(self) -> None:
        """Document this."""
        self.items = [it for it in self.items if it.count > 0]

    def serialise(self) -> types.serialisation_t:
        return {'items': [it.serialise() for it in self.items]}

    @classmethod
    def unserialise(cls, data: types.serialisation_t) -> types.inventory_t:
        items = [Item.unserialise(it) for it in data['items']]
        return Inventory(items)

    ###########################################################################

    def gui_game_tooltip(self, **kwargs: tp.Any) -> tp.Optional[types.pw.Node]:
        from pygrpg.gui import Util
        return Util.resource_description_grid(self.items)

    def gui_editor_tooltip(
            self,
            **kwargs: tp.Any
    ) -> tp.Optional[types.pw.Node]:
        return self.gui_game_tooltip()
