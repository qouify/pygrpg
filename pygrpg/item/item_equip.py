#!/usr/bin/env python3

"""Definition of class ItemEquip."""

import typing as tp

from pygrpg.typing import types
from .item_characteristics import ItemCharacteristics


class ItemEquip(ItemCharacteristics):
    """An ItemEquip object contains characteristics of an equippable Item."""

    def __init__(
            self,
            effects: tp.Iterable[types.effect_t],
            requirements: tp.Iterable[types.expr_t],
            allowed_slots: tp.Iterable[types.equipment_slot_t]
    ):
        self.allowed_slots: tp.List[types.equipment_slot_t]

        super().__init__(effects, requirements)
        self.allowed_slots = list(allowed_slots)
