#!/usr/bin/env python3

"""Definition of class Equipment."""

import typing as tp
import collections

from pygrpg.interfaces import Serialisable
from pygrpg.typing import types
from . import EquipmentSlot, Item


class Equipment(
        Serialisable,
        collections.UserDict[types.equipment_slot_t, types.item_t]
):
    """An Equipment is a dictionary mapping EquipmentSlots to Items."""

    def clean(self) -> None:
        """Remove all items from self that have a 0 count."""
        slots = [slot for slot, item in self.items() if item.count == 0]
        for slot in slots:
            del self[slot]

    def insert(
            self,
            slot: types.equipment_slot_t,
            item: types.item_t
    ) -> tp.List[types.item_t]:
        """Equip item at the given slot of self.

        Returns the list of items that had to be unequipped (i.e.,
        that were previously on the slot(s) that are now occupied by
        the item).

        """
        if slot in self:
            result = [self[slot]]
        else:
            result = []
        self[slot] = item
        return result

    def remove(self, item: types.item_t) -> None:
        """Remove item from self."""
        slots = [slot for slot in self if self[slot] == item]
        for slot in slots:
            del self[slot]

    def serialise(self) -> types.serialisation_t:
        return {
            slot.id: item.serialise()
            for slot, item in self.items()
        }

    @classmethod
    def unserialise(cls, data: types.serialisation_t) -> types.equipment_t:
        def unserialise_item(
                item: tp.Union[str, types.serialisation_t]
        ) -> types.item_t:
            if isinstance(item, str):
                return Item.unserialise({'id': item})
            return Item.unserialise(item)
        return Equipment({
            EquipmentSlot.get_(slot): unserialise_item(item)
            for slot, item in data.items()
        })
