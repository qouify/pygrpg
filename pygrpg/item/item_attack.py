#!/usr/bin/env python3

"""Definition of class ItemAttack."""

import typing as tp

from pygrpg.typing import types
from .item_characteristics import ItemCharacteristics


class ItemAttack(ItemCharacteristics):
    """An ItemAttack object contains attack attributes of a weapon Item."""

    def __init__(
            self,
            effects: tp.Iterable[types.effect_t],
            requirements: tp.Iterable[types.expr_t],
            rng: types.range_t,
            delay: tp.Optional[types.expr_t]
    ):
        self.rng: types.range_t
        self.delay: types.expr_t

        super().__init__(effects, requirements)
        self.rng = rng
        if delay is not None:
            self.delay = delay
