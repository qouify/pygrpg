#!/usr/bin/env python3

"""Definition of class Item."""

import typing as tp

from pygrpg.typing import types
from pygrpg.resource import Resource, Text
from pygrpg.attr import HasAttrs
from pygrpg.interfaces import Instanciable, Serialisable
from pygrpg.util import IntCouple


class Item(
        Resource,
        Instanciable,
        Serialisable,
        HasAttrs[types.item_attr_t, int]
):
    """Document this."""

    HAS_ICON = True

    def __init__(
            self,
            rid: str,
            max_count: int,
            attrs: types.item_attrs_t,
            skill: tp.Optional[types.skill_t],
            attack: tp.Optional[types.item_attack_t],
            defense: tp.Optional[types.item_defense_t],
            equip: tp.Optional[types.item_equip_t],
            use: tp.Optional[types.item_use_t]
    ):
        """Document this."""
        self.skill: types.skill_t
        self.max_count: int
        self.count: int
        self.attack: types.item_attack_t
        self.defense: types.item_defense_t
        self.use: types.item_use_t
        self.equip: types.item_equip_t

        Resource.__init__(self, rid)
        HasAttrs.__init__(self, attrs)
        self.max_count = max_count
        self.count = 1
        if skill is not None:
            self.skill = skill
        if attack is not None:
            self.attack = attack
        if defense is not None:
            self.defense = defense
        if use is not None:
            self.use = use
        if equip is not None:
            self.equip = equip

    def has_skill(self) -> bool:
        """Check if self has an associated skill."""
        return hasattr(self, 'skill')

    def has_use(self) -> bool:
        """Check if self has use characteristics."""
        return hasattr(self, 'use')

    def has_equip(self) -> bool:
        """Check if self has equip characteristics."""
        return hasattr(self, 'equip')

    def has_attack(self) -> bool:
        """Check if self has attack characteristics."""
        return hasattr(self, 'attack')

    def has_defense(self) -> bool:
        """Check if self has defense characteristics."""
        return hasattr(self, 'defense')

    def get_area_image_file(
            self,
            slot: types.equipment_slot_t
    ) -> tp.Optional[str]:
        """Document this."""
        id_slot = slot.get_id_suffix()
        return self.get_image_file(media=f'area_{id_slot}')

    def add_count(self, count: int) -> None:
        """Add some instance to self."""
        self.count += count

    def rem_count(self, count: int) -> None:
        """Remove some instance from self."""
        self.count -= count

    def set_count(self, count: int) -> None:
        """Set self's count."""
        self.count = count

    def serialise(self) -> types.serialisation_t:
        result: types.serialisation_t = {'id': self.id}
        if self.count != 1:
            result['count'] = self.count
        return result

    def get_exprs(self) -> types.resource_exprs_getter_t:
        for c in ('attack', 'defense', 'equip', 'use'):
            if hasattr(self, c):
                char: types.item_characteristics_t = getattr(self, c)
                for requirement in char.requirements:
                    yield requirement, {'item', 'being'}
                for effect in char.effects:
                    for expr in effect.get_exprs():
                        yield expr, {'item', 'being'}

    @classmethod
    def unserialise(cls, data: types.serialisation_t) -> types.item_t:
        result = Item.get_(str(data['id'])).instanciate()
        result.set_count(int(data.get('count', 1)))
        return result

    def instanciate(self) -> types.item_t:
        return Item(
            rid=self.id,
            max_count=self.max_count,
            attrs=self.attrs,
            skill=self.skill if self.has_skill() else None,
            attack=self.attack if self.has_attack() else None,
            defense=self.defense if self.has_defense() else None,
            equip=self.equip if self.has_equip() else None,
            use=self.use if self.has_use() else None
        )

    ###########################################################################

    @classmethod
    def gui_label_style_class(cls) -> tp.Optional[str]:
        return 'item_name'

    def gui_icon(self, **kwargs: tp.Any) -> types.pg.surface.Surface:
        import pygwin as pw
        result = Resource.gui_icon(self, **kwargs)
        count = self.count
        if count > 1:
            lbl = pw.Label(str(count), style={'font-size': 'small'})
            pos = IntCouple.diff(result.get_size(), result.get_size())
            pos = IntCouple.diff(pos, (5, 5))
            lbl.position(pos)
            lbl.draw(result)
        return result

    def _gui_game_tooltip(
            self,
            **kwargs: tp.Any
    ) -> tp.Optional[types.pw.Node]:
        import pygwin as pw
        result = pw.Table()
        centered = {'halign': 'center'}

        #  item name
        lbl = self.gui_label(style=centered)
        result.new_row({0: lbl}, colspan={0: 2})

        #  skill
        if self.skill is not None:
            result.new_row({
                0: pw.Label(Text.val('#text-ui-word-skill')),
                1: self.skill.gui_label()
            })

        #  attack characteristics
        if self.attack is not None:
            rows = list()
            for effect in self.attack.effects:
                descr = effect.gui_description()
                if descr is not None:
                    rows.append({1: pw.Label(descr)})
            if rows != list():
                lbl = pw.Label(
                    Text.val('#text-ui-word-attack'),
                    style=centered
                )
                result.new_row({0: lbl}, colspan={0: 2})
                for row in rows:
                    result.new_row(row)

        #  defense characteristics
        if self.defense is not None:
            rows = list()
            if rows != list():
                lbl = pw.Label(
                    Text.val('#text-ui-word-attack'),
                    style=centered
                )
                result.new_row({0: lbl}, colspan={0: 2})
                for row in rows:
                    result.new_row(row)

            # requirements: tp.Iterable[types.expr_t],
            # rng: types.range_t,
            # delay: tp.Optional[types.expr_t],
            # skill: tp.Optional[types.skill_t]

        #  attrs
        # for attr in self.attrs:
        #     pass
        #     val = ''
        #     result.new_row({
        #         0: pw.Label(Text.val(attr.get_text())),
        #         1: pw.Label(val)
        #     })

        #  requirements
        # for requirement in self.requirements:
        #     continue
        #     result.new_row({
        #         0: pw.Label(Text.val('#text-ui-word-requirement')),
        #         1: pw.Label(requirement.get_game_description())
        #     })

        #  effects
        # Effect.get_game_description_effects(result, item.wear_effects)
        return result
