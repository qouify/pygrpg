#!/usr/bin/env python3

"""Definition of class ItemAttr."""

from pygrpg.attr import Attr


class ItemAttr(Attr):
    """An ItemAttr is an attribute that an Item can have."""
