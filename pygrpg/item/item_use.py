#!/usr/bin/env python3

"""Definition of class ItemUse."""

from .item_characteristics import ItemCharacteristics


class ItemUse(ItemCharacteristics):
    """An ItemUse object encodes characteristics of a usable Item."""
