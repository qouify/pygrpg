#!/usr/bin/env python3

"""Definition of class Ai."""

import typing as tp

from pygrpg import glob
from pygrpg.area import Algo
from pygrpg.action import Attack, Move, Pass, Wait
from pygrpg.util import Directions
from pygrpg.being import Hero
from pygrpg.typing import types


class Ai:
    """This class Ai implements computer's artificial intelligence."""

    @staticmethod
    def enemy_action_choice(
            enemy: types.being_t,
            area: types.area_t
    ) -> types.action_t:
        """Choose and return an action to be performed by enemy."""
        cell = enemy.cell
        result: tp.Optional[types.action_t] = None
        reachable = list()
        for hero in area.get_heroes():
            hero_cell = hero.cell
            if area.is_visible(cell, hero_cell):
                path = Algo.shortest_path(area, cell, hero_cell)
                if path is not None:
                    reachable.append((hero, path))
        reachable.sort(key=lambda hero_path: len(hero_path[1]))
        if reachable != []:
            hero, path = reachable[0]
            direction = path[0]
            cell = Directions.move(cell, direction)
            blocking = area.get_blocking_object(cell)
            if blocking is not None and isinstance(blocking, Hero):
                result = Attack(being=enemy, target=blocking)
            else:
                result = Move(being=enemy, direction=direction)

        #  no suitable action found => pass
        if result is None:
            if glob.fight_ctx().wait_done:
                result = Pass(being=enemy)
            else:
                result = Wait(being=enemy)
        return result
