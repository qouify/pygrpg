#!/usr/bin/env python3

"""Definition of class Range."""

import typing as tp

from pygrpg.typing import types


class Range:
    """Range objects are used to express e.g. attack ranges, spell ranges."""

    def __init__(self, low: types.expr_t, high: types.expr_t):
        self.low: types.expr_t
        self.high: types.expr_t

        self.low = low
        self.high = high

    def eval(self, assign: types.assignment_t) -> tp.Tuple[float, float]:
        """Evaluate self and return a float couple."""
        return (
            float(self.low.eval(assign)),
            float(self.high.eval(assign))
        )
