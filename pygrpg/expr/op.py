#!/usr/bin/env python3

"""Definition of class Op."""

import typing as tp
import functools
import operator

from pygrpg.typing import types
from . import Expr


class Op(Expr):
    """Op expressions are operations."""

    MAP = {
        '<': operator.lt,
        '<=': operator.le,
        '=': operator.eq,
        '>=': operator.ge,
        '>': operator.gt,
        '+': operator.add,
        '-': operator.sub,
        '*': operator.mul,
        '/': operator.truediv
    }

    def __init__(self, **kwargs: tp.Any):
        self.operator: str
        self.operands: tp.List[types.expr_t]

        Expr.__init__(self)
        self.operator = kwargs['operator']
        self.operands = kwargs['operands']

    def __str__(self) -> str:
        operands = [str(op) for op in self.operands]
        return '(' + (' ' + self.operator + ' ').join(operands) + ')'

    def eval(self, assign: types.assignment_t) -> types.expr_result_t:
        return functools.reduce(
            Op.MAP[self.operator],
            [o.eval(assign) for o in self.operands]
        )

    def check(self, var_set: tp.Set[str]) -> tp.Iterator[types.check_msg_t]:
        for op in self.operands:
            yield from op.check(var_set)
