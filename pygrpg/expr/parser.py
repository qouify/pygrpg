#!/usr/bin/env python3

import typing as tp
import ply.lex as lex
import ply.yacc as yacc
import logging

from . import Expr, Const, RuleRef, Var, Op


class Parser:

    class ParserException(Exception):
        pass

    @classmethod
    def parse(cls, string: tp.Any) -> Expr:
        # pylint: disable=unused-variable

        tokens = [
            'LPAREN',
            'RPAREN',
            'INT',
            'FLOAT',
            'ID',
            'PLUS',
            'MINUS',
            'TIMES',
            'EQUAL',
            'GREATER',
            'GREATER_EQUAL',
            'LESS',
            'LESS_EQUAL',
            'DIV',
            'COMMA'
        ]
        t_LPAREN = r'\('
        t_RPAREN = r'\)'
        t_INT = r'\d+'
        t_FLOAT = r'\d+\.\d*'
        t_PLUS = r'\+'
        t_MINUS = '-'
        t_TIMES = r'\*'
        t_DIV = '/'
        t_EQUAL = r'\='
        t_GREATER = '>'
        t_GREATER_EQUAL = '>='
        t_LESS = '<'
        t_LESS_EQUAL = '<='
        t_ID = r'[a-zA-Z][\-\_a-zA-Z]*'
        t_COMMA = r'\,'

        t_ignore = ' \t'

        precedence = (
            ('left', 'EQUAL'),
            ('left', 'GREATER', 'GREATER_EQUAL'),
            ('left', 'LESS', 'LESS_EQUAL'),
            ('left', 'PLUS', 'MINUS'),
            ('left', 'DIV', 'TIMES'),
        )

        def t_error(t: tp.Any) -> None:
            print(t)
            assert False

        def p_expr_int(p: tp.Any) -> None:
            'expr : INT'
            p[0] = Const(const=int(p[1]))

        def p_expr_float(p: tp.Any) -> None:
            'expr : FLOAT'
            p[0] = Const(const=float(p[1]))

        def p_expr_var(p: tp.Any) -> None:
            'expr : var'
            p[0] = p[1]

        def p_expr_paren(p: tp.Any) -> None:
            'expr : LPAREN expr RPAREN'
            p[0] = p[2]

        def p_expr_rule_args(p: tp.Any) -> None:
            '''expr : ID LPAREN RPAREN
                    | ID LPAREN args RPAREN'''
            if len(p) == 4:
                p[0] = RuleRef(rule_id=p[1], args=dict())
            else:
                p[0] = RuleRef(rule_id=p[1], args=p[3])

        def p_expr_op(p: tp.Any) -> None:
            '''expr : expr PLUS expr
                    | expr MINUS expr
                    | expr TIMES expr
                    | expr DIV expr
                    | expr EQUAL expr
                    | expr GREATER expr
                    | expr GREATER_EQUAL expr
                    | expr LESS expr
                    | expr LESS_EQUAL expr'''
            p[0] = Op(operator=p[2], operands=[p[1], p[3]])

        def p_args(p: tp.Any) -> None:
            '''args : ID EQUAL expr
                    | ID EQUAL expr COMMA args'''
            if len(p) == 4:
                p[0] = {p[1]: p[3]}
            else:
                p[0] = {p[1]: p[3], **p[5]}

        def p_var_id(p: tp.Any) -> None:
            'var : ID'
            p[0] = Var(var=p[1])

        def p_error(p: tp.Any) -> None:
            raise Parser.ParserException()

        lex.lex()
        parser = yacc.yacc()
        try:
            result = parser.parse(str(string))
            assert isinstance(result, Expr)
        except Parser.ParserException:
            msg = f'Cannot parse expression "{string}"'
            logging.critical(msg)
            raise ValueError
        return result
