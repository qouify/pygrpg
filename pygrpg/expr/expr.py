#!/usr/bin/env python3

"""Definition of class Expr."""

import typing as tp
import abc

from pygrpg.typing import types


class Expr(abc.ABC):
    """An Expr is an expression that can appear in a game rules."""

    def get_game_description(self) -> str:
        """Document this."""
        return self._get_game_description()

    def _get_game_description(self) -> str:
        return str(self)

    @abc.abstractmethod
    def eval(self, assign: types.assignment_t) -> types.expr_result_t:
        """Evaluate self with variable assignment assign."""

    def check(self, var_set: tp.Set[str]) -> tp.Iterator[types.check_msg_t]:
        """Check self and generate error or warning message."""
        yield from []
