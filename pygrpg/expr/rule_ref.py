#!/usr/bin/env python3

"""Definition of class RuleRef."""

import typing as tp

from pygrpg.typing import types
from . import Expr


class RuleRef(Expr):
    """Document this."""

    def __init__(self, **kwargs: tp.Any):
        """Document this."""
        self.rule_id: str
        self.args: tp.Dict[str, types.expr_t]

        self.rule_id = 'rule-' + kwargs['rule_id']
        self.args = kwargs.get('args', {})

    def __str__(self) -> str:
        #  remove "rule-" from the id
        result = self.rule_id[5:]
        if self.args != {}:
            args = ','.join(
                [arg + "=" + str(self.args[arg]) for arg in self.args]
            )
            result += '(' + args + ')'
        return result

    def eval(self, assign: types.assignment_t) -> types.expr_result_t:
        from pygrpg.rule import Rule
        assign = {
            **{arg: self.args[arg].eval(assign) for arg in self.args},
            **assign
        }
        return Rule.get_(self.rule_id).eval(assign)

    def _get_game_description(self) -> str:
        from pygrpg.rule import Rule
        rule = Rule.get_(self.rule_id)
        args = {arg: self.args[arg].eval({}) for arg in self.args}
        return rule.get_game_description()

    def check(self, var_set: tp.Set[str]) -> tp.Iterator[types.check_msg_t]:
        from pygrpg.rule import Rule
        rule = Rule.get_(self.rule_id)
        for arg, val in self.args.items():
            if arg not in rule.args:
                yield 'error', f'{arg} is not an argument of {self.rule_id}'
            yield from val.check(var_set)
