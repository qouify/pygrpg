#!/usr/bin/env python3

"""Definition of class Var."""

import typing as tp

from pygrpg.typing import types
from pygrpg.resource import Resource
from . import Expr


class Var(Expr):
    """A Var expression is a reference to a rule parameter or a resource."""

    def __init__(self, **kwargs: tp.Any):
        self.var: str

        Expr.__init__(self)
        self.var = kwargs['var']

    def __str__(self) -> str:
        return self.var

    def eval(self, assign: types.assignment_t) -> types.expr_result_t:
        if self.var in assign:
            return assign[self.var]
        return Resource.get_(self.var)

    def check(self, var_set: tp.Set[str]) -> tp.Iterator[types.check_msg_t]:
        if self.var not in var_set and not Resource.exists(self.var):
            yield 'error', f'{self.var} is undefined'

    def _get_game_description(self) -> str:
        return ''
        # if Resource.exists(self.var):
        # result = Text.val(Resource.get(self.var).text_short)
        # else:
        # result = self.var
        # return result
