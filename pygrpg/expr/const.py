#!/usr/bin/env python3

"""Definition of class Const."""

import typing as tp

from pygrpg.typing import types
from . import Expr


class Const(Expr):
    """A Const expression is any constant."""

    def __init__(self, **kwargs: tp.Any):
        self.const: tp.Any

        Expr.__init__(self)
        self.const = kwargs['const']

    def __str__(self) -> str:
        return str(self.const)

    def eval(self, assign: types.assignment_t) -> types.expr_result_t:
        return self.const
