#!/usr/bin/env python3

"""Definition of class Timing."""

import typing as tp

from pygrpg.config import GameConfig as Cfg


class Timing:
    """Class Timing defines some helper methods to manage animations speed."""

    FIGHT_ANIMATION_CLOCK = 1000
    SPELL_EFFECT_TIME = 100

    @staticmethod
    def get_time(time: int) -> int:
        """Get the time modified according to the speed config. parameter."""
        return int(time * 100 / Cfg['speed'])

    @staticmethod
    def sample(values: tp.List[tp.Any], freq: int) -> tp.List[tp.Any]:
        """Return a list containing every freq^th element of values.

        freq is adjusted according to the speed configuration
        parameter.  For example if speed=200, sample(list(range(100),
        2) = [0, 4, 8, ..., 96, 99].  The first and last items of
        values are always in the resulting list.

        """
        freq = max(1, int(freq * Cfg['speed'] / 100))
        try:
            return values[0:-2][::freq] + [values[-1]]
        except IndexError:  # list too short (1 or 0 item)
            return []
