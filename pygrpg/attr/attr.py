#!/usr/bin/env python3

"""Definition of class Attr."""

from pygrpg.resource import Resource


class Attr(Resource):
    """An Attr is an attribute that can be associated to an HasAttr object."""
