#!/usr/bin/env python3

"""Definition of class HasAttrs."""

import typing as tp

from pygrpg.rule import Rule
from . import Attr

AT = tp.TypeVar('AT', bound=Attr)  # attribute type
VT = tp.TypeVar('VT')  # value type


class HasAttrs(tp.Generic[AT, VT]):
    """An HasAttrs is any object that can be associated attributes.

    Examples: Being, Item.

    """

    def __init__(self, attrs: tp.Dict[AT, VT]):
        self.attrs: tp.Dict[AT, VT]

        self.attrs = attrs

    def get_base_attr(self, attr: AT) -> VT:
        """Get self's base attribute value for attr."""
        return self.attrs[attr]

    def get_attr(self, attr: AT) -> VT:
        """Get self's current value for attr.

        The value returned by get_attr depends on any modifier
        currently applied on the attribute.

        """
        return tp.cast(VT, Rule.get_('attr').eval({'of': self, 'attr': attr}))
