#!/usr/bin/env python3

"""Document this."""

from .attr import Attr
from .has_attrs import HasAttrs

__all__ = [
    'Attr',
    'HasAttrs'
]
