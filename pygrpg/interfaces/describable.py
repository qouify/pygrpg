#!/usr/bin/env python3

"""Definition of class Describable."""

import typing as tp
import pygwin as pw


class Describable:
    """Document this."""

    def gui_game_tooltip(self, **kwargs: tp.Any) -> tp.Optional[pw.Node]:
        """Get a pygwin Node describing self (game mode)."""
        return self._mk_tooltip(self._gui_game_tooltip, **kwargs)

    def gui_editor_tooltip(self, **kwargs: tp.Any) -> tp.Optional[pw.Node]:
        """Get a pygwin Node describing self (editor mode)."""
        return self._mk_tooltip(self._gui_editor_tooltip, **kwargs)

    def _gui_game_tooltip(self, **kwargs: tp.Any) -> tp.Optional[pw.Node]:
        return None

    def _gui_editor_tooltip(self, **kwargs: tp.Any) -> tp.Optional[pw.Node]:
        return None

    def _mk_tooltip(
            self,
            fun: tp.Callable[..., tp.Optional[pw.Node]],
            **kwargs: tp.Any
    ) -> tp.Optional[pw.Node]:
        tooltip = fun(**kwargs)
        if tooltip is not None and kwargs.get('boxify', True):
            return pw.Box(tooltip, stc='tooltip')
        return tooltip
