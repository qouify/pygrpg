#!/usr/bin/env python3

"""The interfaces module defines a number of ABC."""

from .describable import Describable
from .instanciable import Instanciable
from .serialisable import Serialisable


__all__ = [
    'Describable',
    'Instanciable',
    'Serialisable'
]
