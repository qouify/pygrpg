#!/usr/bin/env python3

"""Definition of class Serialisable."""

import typing as tp
import abc

from pygrpg.typing import types

ST = tp.TypeVar('ST', bound='Serialisable')


class Serialisable(abc.ABC):
    """Document this."""

    @abc.abstractmethod
    def serialise(self) -> types.serialisation_t:
        """Document this."""
        ...

    @classmethod
    @abc.abstractmethod
    def unserialise(cls: tp.Type[ST], data: types.serialisation_t) -> ST:
        """Document this."""
        ...
