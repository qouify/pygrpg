#!/usr/bin/env python3

"""Definition of class Instanciable."""

import typing as tp
import abc

IT = tp.TypeVar('IT', bound='Instanciable')


class Instanciable(abc.ABC):
    """Document this."""

    @abc.abstractmethod
    def instanciate(self: IT) -> IT:
        """Document this."""
        ...
