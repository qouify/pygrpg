#!/usr/bin/env python3

"""Definition of class SpellAttr."""

from pygrpg.attr import Attr


class SpellAttr(Attr):
    """A SpellAttr is an attribute that a Spell can have."""
