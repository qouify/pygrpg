#!/usr/bin/env python3

"""Definition of class Spell."""

import typing as tp

from pygrpg.resource import Resource, Text
from pygrpg.rule import Rule
from pygrpg.typing import types
from pygrpg.attr import HasAttrs


class Spell(Resource, HasAttrs[types.spell_attr_t, int]):
    """This class encode Spells that can be cast by beings."""

    HAS_ICON = True

    def __init__(
            self,
            rid: str,
            skill: types.skill_t,
            attrs: types.spell_attrs_t,
            caster_effects: types.spell_effects_t,
            target_effects: types.spell_effects_t,
            area: types.expr_t,
            rng: types.range_t,
            where: types.spell_where_t,
            when: types.spell_when_t,
            target_type: types.spell_target_t,
            card: types.spell_card_t,
            delay: tp.Optional[types.expr_t],
            requirements: types.spell_requirements_t
    ):
        """Initialise a Spell.

        Argument rid is self's id.

        Argument skill is the Skill used to cast self.  The caster
        gains XP in that skill when it casts self.

        Argument attrs is the list of self attributes.

        Argument rng is the area range in which self can be cast.

        Argument where specifies where self can be cast: 'area' or
        'any'.

        Argument when specifies when self can be cast: 'fight',
        'exploration', or 'any'.

        Argument caster_effects is the list of effects affecting the
        self's caster.

        Argument target_effects is the list of effects affecting the
        self's targets.

        Argument target_type specifies which type of being can be
        targeted by self: 'enemy', 'hero' or 'any'.

        Argument card ('all' or 'one') specifies how much enemy, or
        heroes are affected by self.

        Argument delay specifies how much turns it takes to cast self.

        Argument requirements is the list of requirements the caster
        must fulfill to be able to cast self.

        """
        Resource.__init__(self, rid)
        HasAttrs.__init__(self, attrs)
        self.skill = skill
        self.area = area
        self.rng = rng
        self.where = where
        self.when = when
        self.target_type = target_type
        self.card = card
        self.delay = delay
        self.caster_effects = caster_effects
        self.target_effects = target_effects
        self.requirements = requirements

    def __lt__(self, other: tp.Any) -> bool:
        if isinstance(other, Spell):
            left = self.skill, self.id
            right = other.skill, other.id
            return left < right
        return Resource.__lt__(self, other)

    def is_castable_by(self, being: types.being_t) -> bool:
        """Check if being satisfy all requirements to cast self."""
        params = {
            'being': being,
            'spell': self
        }
        return all(bool(r.eval(params)) for r in self.requirements)

    def get_exprs(self) -> types.resource_exprs_getter_t:
        for requirement in self.requirements:
            yield requirement, {'being', 'spell'}
        for effect in self.caster_effects + self.target_effects:
            for expr in effect.get_exprs():
                yield expr, {'being', 'spell'}

    ###########################################################################

    @classmethod
    def gui_label_style_class(cls) -> tp.Optional[str]:
        return 'spell_name'

    def _gui_game_tooltip(
            self,
            **kwargs: tp.Any
    ) -> tp.Optional[types.pw.Node]:
        def new_row(text: str, value: str) -> None:
            result.new_row({
                0: pw.Label(Text.val(text)),
                1: pw.Label(value)
            })

        import pygwin as pw
        from pygrpg.gui import Util

        result = pw.Table()
        lbl = self.gui_label(style={'halign': 'center'})
        result.new_row({0: lbl}, colspan={0: 2})
        result.new_row({
            0: pw.Label(Text.val('#text-ui-word-skill')),
            1: self.skill.gui_label()
        })
        hero = kwargs.get('hero', None)
        if hero is not None:
            prob = Rule.get_('#rule-probability-success-cast').eval(
                {'being': hero, 'spell': self}
            )
            new_row('#text-ui-word-probability', f'{prob} %')
        if self.where == 'area':
            rng = self.rng.eval({'hero': hero})
            area = int(self.area.eval({'hero': hero}))
            new_row('#text-ui-word-range', Util.get_range(rng))
            if area > 0:
                new_row('#text-ui-word-diameter', str(area))

        #  delay
        if self.delay is not None:
            delay = self.delay.eval({'hero': hero})
            if delay > 0:
                txt_word_turns = Text.val('#text-ui-word-turns')
                turns = f'{delay} {txt_word_turns}'
                new_row('#text-ui-word-preparation', turns)
        # Effect.get_game_description_effects(result, self.effects)
        return result
