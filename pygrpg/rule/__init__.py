#!/usr/bin/env python3

"""Document this."""

import typing as tp
import random

from pygrpg.resource import Text
from pygrpg.typing import types
from .rule import Rule

__all__ = [
    'Rule'
]

###
#  rule-attr
Rule.add(Rule(
    rid='rule-attr',
    args=['obj', 'attr'],
    fun=lambda assign: assign['obj'].get_base_attr(assign['attr']),
    descr=lambda assign: Text.val(assign['attr'].text_short)
))

###
#  rule-dice
RND = random.Random()
Rule.add(Rule(
    rid='rule-dice',
    args=['num', 'faces'],
    fun=lambda assign: sum(
        RND.randint(1, int(assign['faces']))
        for _ in range(int(assign['num']))
    )
))

###
#  rule-item-skill
Rule.add(Rule(
    rid='rule-item-skill',
    args=['item'],
    fun=lambda assign: assign['item'].skill
))

###
#  rule-being-skill
Rule.add(Rule(
    rid='rule-being-skill',
    args=['being', 'skill'],
    fun=lambda assign: assign['being'].get_skill_level(assign['skill'])
))

###
#  rule-being-status
Rule.add(Rule(
    rid='rule-being-status',
    args=['being', 'status'],
    fun=lambda assign: assign['being'].get_status(assign['status'])
))

###
#  rule-being-weapon
Rule.add(Rule(
    rid='rule-being-weapon',
    args=['being'],
    fun=lambda assign: assign['being'].get_weapon()
))
