#!/usr/bin/env python3

"""Definition of class Rule."""

import typing as tp
import sys

from pygrpg.typing import types
from pygrpg.paths import Paths
from pygrpg.resource import Resource


class Rule(Resource):
    """Document this."""

    HAS_TEXT = False

    modifier_t = tp.Tuple[
        tp.Any, types.assignment_t, types.expr_eval_fun_t
    ]
    MODIFIERS: tp.Dict[str, tp.List[modifier_t]] = {}

    def __init__(
            self,
            rid: str,
            expr: tp.Optional[types.expr_t] = None,
            fun: tp.Optional[types.expr_eval_fun_t] = None,
            args: tp.Optional[tp.List[str]] = None,
            descr: tp.Optional[tp.Callable[[tp.Any], str]] = None
    ):
        """Document this."""

        self.args: tp.List[str]
        self.descr: tp.Callable[[tp.Any], str]
        self.expr: types.expr_t
        self.fun: types.expr_eval_fun_t

        super().__init__(rid)
        self.args = list() if args is None else args
        if descr is not None:
            self.descr = descr
        if expr is not None:
            self.expr = expr
        if fun is not None:
            self.fun = fun

    def has_expr(self) -> bool:
        """Check if self has an expression."""
        return hasattr(self, 'expr')

    def has_descr(self) -> bool:
        """Check if self has a description string."""
        return hasattr(self, 'descr')

    def eval(self, assign: types.assignment_t) -> types.expr_result_t:
        """Evaluate self with variable assignment assign and get the result."""
        if self.has_expr():
            result = self.expr.eval(assign)
        else:
            result = self.fun(assign)
        for _, args, modifier in Rule.MODIFIERS.get(self.id, []):
            if assign == args:
                result = modifier(result)
        return result

    @classmethod
    def add_modifier(
            cls,
            rule_id: str,
            src: tp.Any,
            args: types.assignment_t,
            modifier: tp.Any
    ) -> None:
        """Document this."""
        if rule_id not in Rule.MODIFIERS:
            Rule.MODIFIERS[rule_id] = []
        Rule.MODIFIERS[rule_id].append((src, args, modifier))

    @classmethod
    def del_modifier(
            cls,
            rule_id: str,
            src: tp.Any,
            args: types.assignment_t
    ) -> None:
        """Document this."""
        Rule.MODIFIERS[rule_id] = [
            sam
            for sam in Rule.MODIFIERS[rule_id]
            if sam[0] != src or sam[1] != args
        ]

    def get_exprs(self) -> types.resource_exprs_getter_t:
        if self.has_expr():
            yield self.expr, set(self.args)

    @classmethod
    def get_(
            cls,
            r: tp.Union[str, types.rule_t]
    ) -> types.rule_t:
        if isinstance(r, Rule):
            return r
        if not Rule.exists(r):
            if Paths['dir-src'] not in sys.path:
                sys.path.insert(0, Paths['dir-src'])
            try:
                import rules  # type: ignore # pylint: disable=import-error
                fun = r.replace('-', '_')
                if hasattr(rules, fun):
                    new = Rule(rid=r, fun=getattr(rules, fun))
                    Rule.add(new)
            except ModuleNotFoundError:
                pass
        return super().get_(r)

    ###########################################################################

    def get_game_description(self, **kwargs: tp.Any) -> str:
        """Document this."""
        if self.has_descr():
            return self.descr(kwargs)
        if self.has_expr():
            return self.expr.get_game_description()
        assert False, f'no description for rule {self.id}'
        return ''
