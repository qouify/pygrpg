#!/usr/bin/env python3

"""Definition of class Move."""

import typing as tp

from pygrpg import glob
from pygrpg.util import Directions
from pygrpg.typing import types
from .directed_action import DirectedAction


class Move(DirectedAction):
    """A Move action makes a being move on the area."""

    def do(self) -> None:

        direction = self.direction
        ctx = glob.area_ctx()

        #  move and then check if the destination cell targets a location
        ctx.move_being(self.being, direction)
        target_location = ctx.area.get_cell_target(self.being.cell)
        if target_location is not None:
            area_id, location = target_location
            if area_id != ctx.area.id:
                game = glob.game()
                game.leave_area()
                game.enter_area(area_id, location=location)
                ctx.set_area(game.area)
            else:
                cell = ctx.area.get_location_cell(location)
                assert cell is not None
                ctx.area.del_object(self.being)
                ctx.area.add_object(cell, self.being)
        glob.area_node().center_on_cell(self.being.cell)
        ctx.update_visibility()
        self.being.set_direction(direction)

    ###########################################################################

    def gui_animation(self) -> tp.Optional[types.pw.Animation]:
        from pygrpg.gui.animations.action import MoveAnimation
        return MoveAnimation(
            self.being,
            Directions.move(self.being.cell, self.direction)
        )
