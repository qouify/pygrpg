#!/usr/bin/env python3

"""Definition of class Open."""

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.util import IntCouple
from .directed_action import DirectedAction


class Open(DirectedAction):
    """An Open action opens an openable object (door, chest)."""

    def do(self) -> None:
        cell = self.get_dest_cell()
        ctx = glob.area_ctx()
        obj = ctx.area.get_openable_object(cell)
        assert obj is not None
        obj.change_state()
        ctx.update_visibility()

    ###########################################################################

    def gui_complete(self, pgevt: types.pg.event.Event) -> bool:
        def on_target_valid() -> None:
            cell = self.being.cell
            area_ctx = glob.area_ctx()
            assert cell is not None and area_ctx.target is not None
            direction = IntCouple.diff(area_ctx.target, cell)
            self.direction = direction
        return self.gui_complete_target_change(pgevt, on_target_valid)
