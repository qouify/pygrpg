#!/usr/bin/env python3

"""Definition of class DirectedAction."""

import typing as tp

from pygrpg.util import Directions
from pygrpg.typing import types
from . import Action


class DirectedAction(Action):
    """Document this."""

    def __init__(self, **kwargs: tp.Any):
        """Document this."""
        self.direction: types.direction_t

        Action.__init__(self, **kwargs)
        if 'direction' in kwargs:
            self.direction = kwargs['direction']

    def is_complete(self) -> bool:
        return hasattr(self, 'direction')

    def get_dest_cell(self) -> types.cell_t:
        """Document this."""
        return Directions.move(self.being.cell, self.direction)
