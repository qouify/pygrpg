#!/usr/bin/env python3

"""Definition of class Attack."""

import typing as tp

from pygrpg.typing import types
from pygrpg.resource import Text
from pygrpg.rule import Rule
from pygrpg.being import Monster
from pygrpg import glob
from . import Action


class Attack(Action):
    """Document this."""

    def __init__(self, **kwargs: tp.Any):
        """Document this."""
        self.target: types.being_t

        Action.__init__(self, **kwargs)
        if 'target' in kwargs:
            self.target = kwargs['target']

    def get_target(self) -> types.being_t:
        assert self.target is not None
        return self.target

    def get_skill(self) -> types.skill_t:
        result = self.being.get_weapon().skill
        assert result is not None
        return result

    def is_complete(self) -> bool:
        return hasattr(self, 'target')

    def get_delay(self) -> int:
        delay = self.being.get_weapon().attack.delay
        if delay is None:
            return 0
        return int(delay.eval(self.get_rule_vars()))

    def _compute_effects(self) -> None:
        self.add_effects(
            self.get_target(),
            self.being.get_weapon().attack.effects
        )

    def get_rule_vars(self) -> types.assignment_t:
        return {
            **super().get_rule_vars(),
            'weapon': self.being.get_weapon()
        }

    def get_success_probability(self) -> int:

        #  compute attack score
        attack = int(Rule.get_('#rule-fight-attack-score').eval(
            self.get_rule_vars()
        ))

        #  compute defense score (0 if the target has no weapon)
        target = self.get_target()
        target_weapon = target.get_weapon()
        if target_weapon is None:
            defense = 0
        else:
            defense = int(Rule.get_('#rule-fight-defense-score').eval({
                'being': target,
                'weapon': target_weapon
            }))

        return 50 + attack - defense

    ###########################################################################

    def gui_description(self) -> tp.Optional[str]:
        return Text.val('#text-ui-action-attack', data=self.being.get_name())

    def gui_complete(self, pgevt: types.pg.event.Event) -> bool:
        def on_target_valid() -> None:
            ctx = glob.area_ctx()
            assert ctx.target is not None
            target = ctx.area.get_being(ctx.target)
            if target is not None and isinstance(target, Monster):
                self.target = target
        return self.gui_complete_target_change(pgevt, on_target_valid)

    def gui_reschedule(self) -> None:
        glob.fight_ctx().set_attack_target_cells(self.being)

    def gui_animation(self) -> tp.Optional[types.pw.Animation]:
        from pygrpg.gui.animations.action import AttackAnimation
        return AttackAnimation(self)
