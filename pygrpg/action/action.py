#!/usr/bin/env python3

"""Definition of class Action."""

import typing as tp

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.rule import Rule
from pygrpg.resource import Text
from pygrpg.config import GameConfig as Cfg
from pygrpg.util import Directions


class Action:
    """An Action encodes any action that can be done by a being."""

    def __init__(self, **kwargs: tp.Any):
        """Initialise self.

        kwarg being is the being performing the action.  kwarg success
        (default = True) is True if the action is a success (e.g. the
        attack succeeded).

        """
        self.being: types.being_t
        self.effects: tp.List[types.action_effect_t]
        self.success: bool

        self.being = kwargs['being']
        self.effects = list()
        self.success = kwargs.get('success', True)

    def get_target(self) -> tp.Any:
        """Get self's target."""
        return None

    def get_skill(self) -> tp.Optional[types.skill_t]:
        """Get the skill used to perform self."""
        return None

    def has_delay(self) -> bool:
        """Check if self has a delay."""
        return self.get_delay() > 0

    def _add_effect(
            self,
            being: types.being_t,
            effect: types.effect_t
    ) -> None:
        self.effects.append((being, effect))

    def add_effects(
            self,
            being: types.being_t,
            effects: tp.List[types.effect_t]
    ) -> None:
        """Add effects on being to self's effects."""
        for effect in effects:
            if effect.is_immediate():
                gen = effect.generate(self.get_rule_vars())
                self._add_effect(being, gen)
            if effect.is_future():
                gen = effect.copy()
                self._add_effect(being, gen)

    def do(self) -> None:
        """Realise the effects of the action."""
        for target, effect in self.effects:
            if effect.is_immediate():
                effect.realise(
                    being=target,
                    area=None if glob.ctx() is None else glob.area_ctx().area
                )
            if effect.is_future():
                target.add_effect(effect)

    def compute(self) -> None:
        """Compute effects of the action."""
        self._compute_effects()

    def get_rule_vars(self) -> types.assignment_t:
        """Document this."""
        result = {'being': self.being}
        tgt = self.get_target()
        if tgt is not None:
            result['target'] = tgt
        return result

    def evaluate(self) -> None:
        """Document this."""
        prob = self.get_success_probability()
        self.success = (
            prob is None or
            Rule.get_('dice').eval({'num': 1, 'faces': 100}) <= prob
        )

    def get_xp(self) -> int:
        """Get the number of xp gained by executing self."""
        if self.get_skill() is None:
            return 0
        rule = '#rule-xp-action-'
        if self.success:
            rule += 'success'
        else:
            rule += 'failure'
        return int(Rule.get_(rule).eval(self.get_rule_vars()))

    def get_delay(self) -> int:
        """Get (in turns) the delay required to perform the action."""
        return 0

    def is_complete(self) -> bool:
        """Check if action is complete.

        An action is complete if all its mandatory attributes (e.g.,
        the spell and its target(s) for a cast-spell action) have been
        set.

        """
        return True

    def is_wait(self) -> bool:
        """Document this."""
        return False

    def does_update_visibility(self) -> bool:
        """Check if the action updates the area visibility."""
        return True

    def get_success_probability(self) -> int:
        """Get the success probability of self."""
        return 100

    def cancel(self) -> None:
        """Document this."""

    def _compute_effects(self) -> None:
        pass

    ###########################################################################

    def gui_description(self) -> tp.Optional[str]:
        """Return a string description for self."""
        return None

    def gui_complete_target_change(
            self,
            pgevt: types.pg.event.Event,
            on_target_valid: tp.Callable[[], None]
    ) -> bool:
        """Document this."""
        import pygame as pg

        key = pgevt.key
        ctx = glob.area_ctx()
        move_keys = Cfg.get_move_keys()
        glob.area_node()
        if key == pg.K_RETURN and ctx.is_target_valid():
            on_target_valid()
        elif key in move_keys:
            move = Cfg.move_action_to_direction(move_keys[key])
            new_cell = Directions.move(ctx.target, move)
            ctx.set_target(new_cell)
            glob.area_node().center_on_cell(new_cell)
        else:
            return False
        return True

    def gui_icon(
            self,
            size: tp.Optional[types.size_t] = None,
            surface: tp.Optional[types.pg.surface.Surface] = None
    ) -> tp.Optional[types.pg.surface.Surface]:
        """Get an icon (a pygame surface) for self.

        None is returned if the action does not have an icon.

        """
        resource = self.gui_icon_resource()
        if resource is None:
            return None
        return resource.gui_icon(size=size, surface=surface)

    def gui_icon_resource(self) -> tp.Optional[types.resource_t]:
        """Get the resource of which the icon is used for self."""
        return None

    def gui_complete(self, pgevt: types.pg.event.Event) -> bool:
        """Complete self according to the key-event pgevt.

        Return True if self has been completed, False otherwise.

        """
        return False

    def gui_reschedule(self) -> None:
        """Document this."""

    def gui_animation(self) -> tp.Optional[types.pw.Animation]:
        """Get an animation for self."""
        return None

    def gui_update(self) -> None:
        """Update the gui according to the self's current state."""

    @classmethod
    def _alert(
            cls,
            text_id: str,
            being: tp.Optional[types.being_t] = None
    ) -> None:
        if being is None:
            alert = Text.val(text_id)
        else:
            alert = Text.val(text_id, data=being.get_name())
        glob.main().push_alert(alert)

    @classmethod
    def gui_init(cls, pgevt: types.pg.event.Event) -> bool:
        """Initialise an action according to the key pressed event pgevt.

        Return True if an action has been initialised, False
        otherwise.  If an action has been initialised, the current
        context action is set to this one.

        """
        from . import Pass

        move_keys = Cfg.get_move_keys()
        result = True
        ctx = glob.area_ctx()
        fight = glob.game().in_fight()
        being = ctx.active

        #  pass
        if fight and pgevt.key == Cfg['key-pass']:
            ctx.action = Pass(being=being)

        #  open
        elif not fight and pgevt.key == Cfg['key-open']:
            cls._gui_init_open(pgevt)

        #  move
        elif pgevt.key in move_keys:
            cls._gui_init_move(pgevt)

        #  attack
        elif fight and pgevt.key == Cfg['key-attack']:
            cls._gui_init_attack(pgevt)

        #  use
        elif fight and pgevt.key == Cfg['key-use']:
            cls._gui_init_use(pgevt)

        #  wait
        elif fight and pgevt.key == Cfg['key-wait']:
            cls._gui_init_wait(pgevt)

        #  cast
        elif fight and pgevt.key == Cfg['key-cast']:
            cls._gui_init_cast(pgevt)

        else:
            result = False

        return result

    @classmethod
    def _gui_init_open(cls, pgevt: types.pg.event.Event) -> None:
        from . import Open

        def check_cell(d: types.direction_t) -> bool:
            cell = Directions.move(being.cell, d)
            return ctx.area.get_openable_object(cell) is not None
        ctx = glob.area_ctx()
        being = ctx.active
        directions = [d for d in Directions.ADJACENT if check_cell(d)]

        #  no adjacent object to open
        if directions == list():
            return

        #  at least one adjacent object to open
        if len(directions) == 1:
            args = {'direction': directions[0]}
        else:
            args = {}
            cells = {Directions.move(being.cell, d) for d in directions}
            ctx.reset_targets()
            ctx.action_area = 0
            ctx.set_targetable(cells)
            for cell in cells:
                ctx.set_target(cell)
                break
        ctx.action = Open(being=being, **args)

    @classmethod
    def _gui_init_move(cls, pgevt: types.pg.event.Event) -> None:
        from . import Move

        move_keys = Cfg.get_move_keys()
        ctx = glob.area_ctx()
        fight = glob.game().in_fight()
        being = ctx.active
        direction = Cfg.move_action_to_direction(move_keys[pgevt.key])
        dst_cell = Directions.move(being.cell, direction)
        block = ctx.area.get_blocking_object(dst_cell)
        if not fight:

            #  exploration context => if a blocking object is on the
            #  destination cell then it must be an opened chest
            chest = ctx.area.get_chest_object(dst_cell)
            if block is None:
                ctx.action = Move(being=being, direction=direction)
            elif chest is not None and chest.is_opened():
                def on_closed(_: types.pg.event.Event) -> bool:
                    glob.area_node().set_updated()
                    return True
                from pygrpg.gui import PickupWindow
                inv = chest.inventory
                assert inv is not None
                win = PickupWindow('chest', being, inv)
                win.add_processor('on-close', on_closed)
                win.open()

        elif block is None:

            #  fight context => the hero may not leave the area
            if ctx.area.get_cell_target(dst_cell) is not None:
                cls._alert('#text-ui-fight-cannot-leave-area')
            else:
                ctx.action = Move(being=being, direction=direction)

    @classmethod
    def _gui_init_cast(cls, pgevt: types.pg.event.Event) -> None:
        from . import Cast

        ctx = glob.area_ctx()
        panel = glob.area_panel()
        being = ctx.active

        #  check the being knows spells
        if not being.knows_spells():
            cls._alert('#text-ui-err-hero-does-not-know-spells', being)
            return

        #  callback called when a spell has been selected from the list
        def callback(resource: types.resource_t) -> None:
            spell = tp.cast(types.spell_t, resource)
            act = tp.cast(types.cast_t, ctx.action)
            if spell.when not in ('fight', 'any'):
                cls._alert('#text-ui-err-cannot-cast-spell-now', being)
            elif not spell.is_castable_by(act.being):
                cls._alert('#text-ui-err-cannot-cast-spell-now', being)
            else:
                glob.main().area_panel.reset_selection_list()
                act.spell = spell
                act.gui_update()

        ctx.action = Cast(being=being)
        panel.set_selection_list(being.spells, callback)

    @classmethod
    def _gui_init_use(cls, pgevt: types.pg.event.Event) -> None:
        from . import Use

        ctx = glob.area_ctx()
        being = ctx.active
        panel = glob.area_panel()
        items = [item for item in being.equipment.values() if item.has_use()]

        #  check the being has usable items
        if items == []:
            cls._alert('#text-ui-err-hero-does-not-have-usable', being)
            return

        #  callback called when an item has been selected from the list
        def callback(resource: types.resource_t) -> None:
            item = tp.cast(types.item_t, resource)
            act = tp.cast(types.use_t, ctx.action)
            act.item = item

        ctx.action = Use(being=being)
        panel.set_selection_list(items, callback)

    @classmethod
    def _gui_init_wait(cls, pgevt: types.pg.event.Event) -> None:
        from . import Wait

        ctx = glob.fight_ctx()
        if ctx.wait_done:
            cls._alert('#text-ui-err-cannot-wait')
        else:
            ctx.action = Wait(being=ctx.active)

    @classmethod
    def _gui_init_attack(cls, pgevt: types.pg.event.Event) -> None:
        from . import Attack

        ctx = glob.fight_ctx()
        being = ctx.active
        being.get_weapon()
        ctx.action = Attack(being=being)
        ctx.set_attack_target_cells(being)
