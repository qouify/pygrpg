#!/usr/bin/env python3

"""Definition of class Wait."""

import typing as tp

from pygrpg import glob
from pygrpg.resource import Text
from . import Action


class Wait(Action):
    """Document this."""

    def is_wait(self) -> bool:
        return True

    def do(self) -> None:
        glob.fight_ctx().push_waiting(self.being)

    ###########################################################################

    def gui_description(self) -> tp.Optional[str]:
        assert self.being is not None
        return Text.val('#text-ui-action-wait', data=self.being.get_name())
