#!/usr/bin/env python3

"""Definition of class Cast."""

import typing as tp

from pygrpg import glob
from pygrpg.typing import types
from pygrpg.resource import Text
from pygrpg.rule import Rule
from pygrpg.being import Hero, Monster
from . import Action


class Cast(Action):
    """A Cast is the action of throwing a spell."""

    def __init__(self, **kwargs: tp.Any):
        self.area: types.cast_area_t
        self.cell: types.cell_t
        self.spell: types.spell_t
        self.targets: types.cast_targets_t

        Action.__init__(self, **kwargs)
        if 'spell' in kwargs:
            self.spell = kwargs['spell']
        if 'targets' in kwargs:
            self.targets = kwargs['targets']
        if 'cell' in kwargs:
            self.cell = kwargs['cell']
        if 'area' in kwargs:
            self.area = kwargs['area']

    def get_skill(self) -> types.skill_t:
        return self.spell.skill

    def is_complete(self) -> bool:
        return (
            self.spell is not None and
            self.targets is not None
        )

    def _compute_effects(self) -> None:
        self.add_effects(self.being, self.spell.caster_effects)
        for target in self.targets:
            self.add_effects(target, self.spell.target_effects)

    def cancel(self) -> None:
        self.do()

    def get_rule_vars(self) -> types.assignment_t:
        return {
            **Action.get_rule_vars(self),
            'spell': self.spell
        }

    def get_success_probability(self) -> int:
        return int(Rule.get_('#rule-probability-success-cast').eval(
            self.get_rule_vars()
        ))

    ###########################################################################

    def gui_description(self) -> tp.Optional[str]:
        text = '#text-ui-action-cast'
        data = (self.being.get_name(), self.spell.get_text_val())
        return Text.val(text, data)

    def gui_icon_resource(self) -> tp.Optional[types.resource_t]:
        return self.spell

    def gui_update(self) -> None:
        def possible_targets() -> tp.Set[types.being_t]:
            # @MATCH@
            if spell.target_type == 'hero':
                return set(ctx.get_heroes())
            if spell.target_type == 'enemy':
                return set(ctx.get_monsters())
            return set(
                tp.cast(tp.List[types.being_t], ctx.get_heroes()) +
                tp.cast(tp.List[types.being_t], ctx.get_monsters())
            )
        if not (hasattr(self, 'spell') and hasattr(self, 'targets')):
            return
        ctx = glob.fight_ctx()
        spell = self.spell
        if spell.where == 'area':
            ctx.set_spell_target_cells(self.being, spell)
        elif spell.card == 'all':
            self.targets = possible_targets()
        else:
            cells = {being.cell for being in possible_targets()}
            ctx.reset_targets()
            ctx.action_area = 0
            ctx.set_targetable(cells)
            if cells != set():
                cell = next(iter(cells))
                ctx.set_target(cell)
                glob.area_node().center_on_cell(cell)

    def gui_complete(self, pgevt: types.pg.event.Event) -> bool:
        def complete() -> None:
            ctx = glob.area_ctx()
            area = ctx.area
            target_area = ctx.target_area
            spell_tgt = self.spell.target_type
            targets: tp.Set[types.being_t] = set()
            assert target_area is not None
            for cell in target_area:
                b = area.get_being(cell)
                if b is not None:
                    if spell_tgt == 'hero':
                        if isinstance(b, Hero):
                            targets.add(b)
                    elif spell_tgt == 'enemy':
                        if isinstance(b, Monster):
                            targets.add(b)
                    else:
                        targets.add(b)
            self.targets = targets
            self.cell = ctx.target
            self.area = ctx.target_area

        return self.gui_complete_target_change(pgevt, complete)

    def gui_reschedule(self) -> None:
        self.gui_update()

    def gui_animation(self) -> tp.Optional[types.pw.Animation]:
        from pygrpg.gui.animations.action import CastAnimation
        return CastAnimation(self)
