#!/usr/bin/env python3

"""Definition of class Pass."""

import typing as tp

from pygrpg.resource import Text
from . import Action


class Pass(Action):
    """Document this."""

    def gui_description(self) -> tp.Optional[str]:
        return Text.val('#text-ui-action-pass', data=self.being.get_name())
