#!/usr/bin/env python3

"""Definition of class Use."""

import typing as tp

from pygrpg.typing import types
from . import Action


class Use(Action):
    """A Use action is performed by a being to use an item."""

    def __init__(self, **kwargs: tp.Any):
        """Initialise self.

        Kwarg src (in set {'equipment', 'inventory'}) specifies where
        the item comes from (default: 'equipment').  Kwarg item is the
        item used.

        """
        self.src: types.use_src_t
        self.item: types.item_t

        Action.__init__(self, **kwargs)
        self.src = kwargs.get('src', 'equipment')
        if 'item' in kwargs:
            self.item = kwargs['item']

    def is_complete(self) -> bool:
        return hasattr(self, 'item')

    def _compute_effects(self) -> None:
        assert self.item is not None
        self.add_effects(self.being, self.item.use.effects)

    def do(self) -> None:
        Action.do(self)
        assert self.item is not None
        self.item.rem_count(1)
        if self.src == 'equipment':
            self.being.equipment.clean()
        else:
            self.being.get_inventory().clean()

    ###########################################################################

    def gui_icon_resource(self) -> tp.Optional[types.resource_t]:
        return self.item
