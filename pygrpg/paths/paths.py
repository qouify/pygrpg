#!/usr/bin/env python3

"""
Definition of class Paths.

Directory structure of game data.

~/.pygrpg                   dir-home
|-activity.log              file-log
|-run                       dir-run
| --12123                   dir-run-pid
|   |-areas                 dir-run-areas
--games                     dir-games
  |-some_game               dir-game
  | |-mdata.json            file-game-mdata
  | |-content               dir-content
  | |-resources             dir-resources
  | |-media                 dir-resources-media
  | |-saves                 dir-saves
  | |-config                dir-config
  |   |-fonts.json          file-fonts
  |   |-media               dir-config-media
  |   |-styles              dir-styles
  |   |-game                dir-game-config
  |   | |-config.json       file-game-config
  |   | |-gui.json          file-game-gui
  |   | |-styles            dir-game-styles
  |   |-editor              dir-editor-config
  |     |-config.json       file-editor-config
  |     |-gui.json          file-editor-gui
  |     |-styles            dir-editor-styles
  |-some_other_game
    ...
"""

import os
import typing as tp
import pygwin as pw
import pkg_resources

from pygrpg.typing import types
from pygrpg.util import Io


class Paths(metaclass=pw.SubscriptableType):
    """Class Paths contains method to handle pygrpg directory structure."""

    dir_user = os.path.expanduser('~')
    dir_home = os.path.join(dir_user, '.local', 'share', 'pygrpg')
    pid = str(os.getpid())

    ALL = {
        'dir-home': dir_home,
        'file-log': os.path.join(dir_home, 'activity.log'),
        'dir-run': os.path.join(dir_home, 'run'),
        'dir-run-pid': os.path.join(dir_home, 'run', pid),
        'dir-run-areas': os.path.join(dir_home, 'run', pid, 'areas'),
        'dir-games': os.path.join(dir_home, 'games'),
        'dir-game': None,
        'file-game-mdata': None,
        'dir-src': None,
        'dir-resources': None,
        'dir-resources-media': None,
        'dir-content': None,
        'dir-config': None,
        'dir-saves': None,
        'dir-styles': None,
        'file-fonts': None,
        'dir-game-config': None,
        'dir-game-styles': None,
        'file-game-gui': None,
        'file-game-config': None,
        'dir-editor-config': None,
        'dir-editor-styles': None,
        'file-editor-gui': None,
        'file-editor-config': None,
        'dir-config-media': None
    }

    @classmethod
    def init(cls) -> None:
        """Create all directories."""
        for item, path in Paths.ALL.items():
            if path is not None and item.startswith('dir-'):
                Io.mkdir_if_missing(path)

    @classmethod
    def set_game(
            cls,
            game: tp.Optional[str] = None,
            game_dir: tp.Optional[str] = None
    ) -> None:
        """Set the game by initialising all values of Paths.ALL.

        Either the game name can be provided, or either the
        game directory.

        """
        if game is None:
            if game_dir is None:
                raise ValueError('game and game_dir cannot be both None')
            gdir = game_dir
        else:
            gdir = os.path.join(Paths['dir-games'], game)
        Paths['dir-game'] = gdir

        #  game
        Paths['file-game-mdata'] = os.path.join(gdir, 'mdata.json')
        Paths['dir-saves'] = os.path.join(gdir, 'saves')
        Paths['dir-resources'] = os.path.join(gdir, 'resources')
        Paths['dir-content'] = os.path.join(gdir, 'content')
        Paths['dir-resources-media'] = os.path.join(gdir, 'media')

        #  game/config
        cdir = os.path.join(gdir, 'config')
        Paths['dir-config'] = cdir
        Paths['dir-config-media'] = os.path.join(cdir, 'media')
        Paths['file-fonts'] = os.path.join(cdir, 'fonts.json')
        Paths['dir-styles'] = os.path.join(cdir, 'styles')
        for mode in ['game', 'editor']:
            base = os.path.join(cdir, mode)
            Paths[f'dir-{mode}-config'] = base
            Paths[f'dir-{mode}-styles'] = os.path.join(base, 'styles')
            Paths[f'file-{mode}-config'] = os.path.join(base, 'config.json')
            Paths[f'file-{mode}-gui'] = os.path.join(base, 'gui.json')

    @classmethod
    def list_games(cls) -> tp.Iterator[str]:
        """List all available games."""
        if os.path.isdir(Paths['dir-games']):
            for game_dir in os.listdir(Paths['dir-games']):
                if os.path.isdir(os.path.join(Paths['dir-games'], game_dir)):
                    yield game_dir

    @classmethod
    def init_game(cls, game_dir: str) -> None:
        """Initialise an empty game and its directory structure."""
        template_data_dir = pkg_resources.resource_filename(
            'pygrpg', 'template'
        )
        Io.cptree(template_data_dir, game_dir)

    @classmethod
    def is_game_dir(cls, game_dir: str) -> bool:
        """Check if game_dir contains a game."""
        return (
            os.path.isdir(game_dir) and
            os.path.isfile(os.path.join(game_dir, 'mdata.json'))
        )

    @classmethod
    def get_game_meta_data(cls) -> types.serialisation_t:
        """Get the meta data of the game set via Paths.set_game."""
        assert Paths['file-game-mdata'] is not None
        result = pw.Util.load_json_file(Paths['file-game-mdata'])
        return tp.cast(types.serialisation_t, result)
