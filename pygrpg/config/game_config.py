#!/usr/bin/env python3

"""Definition of class GameConfig."""

import typing as tp
import pygame as pg

from pygrpg.util import Directions
from pygrpg.resource import Text
from pygrpg.typing import types
from . import Config


class GameConfig(Config):
    """Document this."""

    CONFIG_FILE = 'file-game-config'

    DEFAULT = {
        'autosave-after-area-change': True,
        'autosave-after-fight': True,
        'move-camera-animations': True,
        'key-attack': pg.K_s,
        'key-cast': pg.K_c,
        'key-center-axis': pg.K_ASTERISK,
        'key-char': pg.K_TAB,
        'key-map': pg.K_m,
        'key-move-east': pg.K_d,
        'key-move-north-east': pg.K_e,
        'key-move-north': pg.K_w,
        'key-move-north-west': pg.K_q,
        'key-move-south-east': pg.K_c,
        'key-move-south': pg.K_x,
        'key-move-south-west': pg.K_z,
        'key-move-west': pg.K_a,
        'key-open': pg.K_o,
        'key-pass': pg.K_SPACE,
        'key-pickup': pg.K_p,
        'key-use': pg.K_u,
        'key-wait': pg.K_t,
        'language': 'en',
        'music-level': 100,
        'sounds-level': 100,
        'speed': 100
    }

    ALL = {
        **DEFAULT
    }
    ALL_KEYS = [
        param for param in ALL if param.startswith('key-')
    ]

    AVAILABLE = {
        'language': ['en', 'fr'],
        'speed': list(range(50, 501, 50))
    }

    @classmethod
    def is_key_allowed(cls, key: int) -> bool:
        """Document this."""
        return key in [
            pg.K_ASTERISK,
            pg.K_SPACE,
            pg.K_TAB
        ] + list(range(pg.K_a, pg.K_z + 1))

    @classmethod
    def move_action_to_direction(cls, action: str) -> types.direction_t:
        """Document this."""
        return {
            'key-move-north': Directions.NORTH,
            'key-move-south': Directions.SOUTH,
            'key-move-east': Directions.EAST,
            'key-move-west': Directions.WEST,
            'key-move-north-east': Directions.NORTH_EAST,
            'key-move-south-east': Directions.SOUTH_EAST,
            'key-move-south-west': Directions.SOUTH_WEST,
            'key-move-north-west': Directions.NORTH_WEST
        }[action]

    @classmethod
    def key_param_to_text(cls, param: str) -> str:
        """Document this."""
        return Text.val({
            'key-move-north': '#text-ui-conf-move-north',
            'key-move-south': '#text-ui-conf-move-south',
            'key-move-east': '#text-ui-conf-move-east',
            'key-move-west': '#text-ui-conf-move-west',
            'key-move-north-east': '#text-ui-conf-move-north-east',
            'key-move-south-east': '#text-ui-conf-move-south-east',
            'key-move-south-west': '#text-ui-conf-move-south-west',
            'key-move-north-west': '#text-ui-conf-move-north-west',
            'key-attack': '#text-ui-conf-attack',
            'key-cast': '#text-ui-conf-cast',
            'key-center-axis': '#text-ui-conf-center-axis',
            'key-char': '#text-ui-conf-char',
            'key-open': '#text-ui-conf-open',
            'key-use': '#text-ui-conf-use',
            'key-map': '#text-ui-conf-map',
            'key-pass': '#text-ui-conf-pass',
            'key-pickup': '#text-ui-conf-pickup',
            'key-wait': '#text-ui-conf-wait'
        }[param])

    @classmethod
    def reset_keys_to_default(cls) -> None:
        """Document this."""
        for param in GameConfig.ALL_KEYS:
            GameConfig[param] = GameConfig.DEFAULT[param]

    @classmethod
    def fix_keys(cls, param: str) -> None:
        """Document this."""
        for oparam in GameConfig.ALL_KEYS:
            if oparam != param and GameConfig[param] == GameConfig[oparam]:
                GameConfig[oparam] = None

    @classmethod
    def get_move_keys(cls) -> tp.Dict[str, str]:
        """Document this."""
        return {
            GameConfig[param]: param
            for param in GameConfig.ALL_KEYS
            if param.startswith('key-move-')
        }
