#!/usr/bin/env python3

"""Definition of class Config."""

import typing as tp

import os
import logging
import pygwin as pw

from pygrpg.paths import Paths


class Config(metaclass=pw.SubscriptableType):
    """Document this."""

    CONFIG_FILE: str = ''
    DEFAULT: tp.Dict[str, tp.Any] = {}
    ALL: tp.Dict[str, tp.Any] = {}

    @classmethod
    def load(cls) -> None:
        """Document this."""
        if not os.path.exists(Paths[cls.CONFIG_FILE]):
            cls.ALL = {**cls.DEFAULT}
            cls.save()
        else:
            cfg_file: str = Paths[cls.CONFIG_FILE]
            logging.info('load config file %s', cfg_file)
            pw.SubscriptableType.load(cls, cfg_file)

    @classmethod
    def save(cls) -> None:
        """Document this."""
        cfg_file: str = Paths[cls.CONFIG_FILE]
        logging.info('write config file %s', cfg_file)
        pw.SubscriptableType.save(cls, cfg_file)
