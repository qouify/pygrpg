#!/usr/bin/env python3

"""Document this."""

from .config import Config
from .editor_config import EditorConfig
from .game_config import GameConfig

__all__ = [
    'Config',
    'EditorConfig',
    'GameConfig'
]
