#!/usr/bin/env python3

"""Definition of class EditorConfig."""

import typing as tp

from . import Config


class EditorConfig(Config):
    """Document this."""

    CONFIG_FILE = 'file-editor-config'

    DEFAULT: tp.Dict[str, tp.Any] = {
    }

    ALL = {
        **DEFAULT
    }
