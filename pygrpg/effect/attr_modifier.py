#!/usr/bin/env python3

"""Definition of class AttrModifier."""

import typing as tp

from pygrpg.resource import Text
from pygrpg.expr import Op
from pygrpg.rule import Rule
from pygrpg.being import BeingAttr
from pygrpg.typing import types
from . import Effect


class AttrModifier(Effect):
    """An AttrModifier effect modifies some status being.

    For examples: Strength + 10, Intelligence / 2.

    """

    def __init__(self, **kwargs: tp.Any):
        """Document this."""
        self.attr: types.being_attr_t
        self.modifier: str

        Effect.__init__(self, **kwargs)
        self.attr = BeingAttr.get_(kwargs['attr'])
        self.modifier = str(kwargs['modifier'])

    def get_text(self) -> str:
        return Text.val('#text-ui-word-modifier')

    def realise(self, **kwargs: tp.Any) -> None:
        def modify(value: int) -> int:
            result = Op.MAP[self.modifier](
                value,
                self.expr.eval(args)
            )
            return int(result)
        super().realise(**kwargs)
        args = {
            'being': kwargs['being'],
            'attr': self.attr
        }
        Rule.add_modifier('rule-attr', kwargs['src'], args, modify)

    def unrealise(self, **kwargs: tp.Any) -> None:
        super().unrealise(**kwargs)
        args = {
            'being': kwargs['being'],
            'attr': self.attr
        }
        Rule.del_modifier('rule-attr', kwargs['src'], args)

    def serialise(self) -> types.serialisation_t:
        return {
            **super().serialise(),
            'attr': self.attr.id,
            'modifier': self.modifier
        }

    ###########################################################################

    def gui_description(self) -> tp.Optional[str]:
        txt_attr_short = self.attr.get_text_short_val()
        txt_modifier = Text.val(self.modifier)
        txt_expr = self.expr.get_game_description()
        return f'{txt_attr_short} {txt_modifier}{txt_expr}'

    def gui_realisation_description(
            self,
            assign: types.assignment_t
    ) -> tp.Optional[str]:
        assert False
