#!/usr/bin/env python3

"""Definition of class Death."""

import typing as tp

from pygrpg import glob
from pygrpg.resource import Text
from pygrpg.typing import types
from . import Effect


class Death(Effect):
    """A Death effect causes the death of a being."""

    def realise(self, **kwargs: tp.Any) -> None:
        super().realise(**kwargs)
        being = kwargs['being']
        area = kwargs.get('area')
        if area is not None:
            area.del_object(being)
        ctx = glob.fight_ctx()
        if ctx is not None:
            ctx.on_death(being)

    ###########################################################################

    def gui_description(self) -> str:
        return Text.val('#text-ui-word-death')

    def gui_realisation_description(
            self,
            assign: types.assignment_t
    ) -> tp.Optional[str]:
        assert False
