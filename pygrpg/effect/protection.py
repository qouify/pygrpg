#!/usr/bin/env python3

"""Definition of class Protection."""

import typing as tp

from pygrpg.resource import Text
from pygrpg.typing import types
from . import Effect, StatusChangeType


class Protection(Effect):
    """A Protection effect reduces a StatusChange effect."""

    def __init__(self, **kwargs: tp.Any):
        """Document this."""
        self.types: tp.List[types.status_change_type_t]

        Effect.__init__(self, **kwargs)
        self.types = [StatusChangeType.get_(dt) for dt in kwargs['ctypes']]

    def get_text(self) -> str:
        return Text.val('#text-ui-word-protection')

    def affect(self, effect: types.effect_t) -> None:
        """
        if effect.is_damage():
            damage = tp.cast(types.damage_t, effect)
            if any(
                    damage.damage_type.is_of_type(dt)
                    for dt in self.damage_types
            ):
                assert self.expr is not None
                damage.reduce(self.expr)
        """

    def serialise(self) -> types.serialisation_t:
        return {
            **super().serialise(),
            'types': [dt.id for dt in self.types]
        }

    ###########################################################################

    def gui_description(self) -> tp.Optional[str]:
        assert False
        return None

    def gui_realisation_description(
            self,
            assign: types.assignment_t
    ) -> tp.Optional[str]:
        assert False
