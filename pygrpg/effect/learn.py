#!/usr/bin/env python3

"""Definition of class Learn."""

import typing as tp

from pygrpg.resource import Text
from pygrpg.typing import types
from pygrpg.spell import Spell
from . import Effect


class Learn(Effect):
    """A Learn effect allows a being to learn a spell."""

    def __init__(self, **kwargs: tp.Any):
        """Document this."""
        self.spell: types.spell_t

        Effect.__init__(self, **kwargs)
        self.spell = Spell.get_(kwargs['spell'])

    def realise(self, **kwargs: tp.Any) -> tp.Any:
        super().realise(**kwargs)
        kwargs['being'].spells.add(self.spell)

    def serialise(self) -> types.serialisation_t:
        return {
            **super().serialise(),
            'spell': self.spell.id
        }

    ###########################################################################

    def gui_description(self) -> tp.Optional[str]:
        txt = Text.val('#text-ui-word-learn')
        spell = self.spell.get_text_val()
        return f'{txt} ({spell}))'

    def gui_realisation_description(
            self,
            assign: types.assignment_t
    ) -> tp.Optional[str]:
        assert False
