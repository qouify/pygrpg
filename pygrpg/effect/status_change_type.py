#!/usr/bin/env python3

"""Definition of class StatusChangeType."""


from pygrpg.typing import types
from pygrpg.resource import Resource


class StatusChangeType(Resource):
    """Document this."""

    HAS_ICON = True

    def __lt__(self, other: Resource) -> bool:
        if isinstance(other, StatusChangeType):
            if self.is_of_type(other):
                return False
            if other.is_of_type(self):
                return True
        return Resource.__lt__(self, other)

    def is_of_type(self, ctype: types.status_change_type_t) -> bool:
        """Document this."""
        return self is ctype or self.is_child(ctype)
