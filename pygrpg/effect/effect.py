#!/usr/bin/env python3

"""Definition of class Effect."""

import typing as tp
import math

from pygrpg import glob
from pygrpg.expr import Expr, Parser
from pygrpg.interfaces import Serialisable
from pygrpg.typing import types

ET = tp.TypeVar('ET', bound='Effect')


class Effect(Serialisable):
    """An Effect is anything that can alter a being (status, position, ...)."""

    PATTERNS = {
        'permanent': {
            'delay': 0,
            'occurences': 2,
            'period': math.inf
        },
        'once-immediate': {
            'delay': 0,
            'occurences': 1,
            'period': 1
        }
    }

    delay: int
    expr: types.expr_t
    occurences: int
    period: int
    result: tp.Any
    src: tp.Any

    def __init__(self, **kwargs: tp.Any):
        """Document this."""
        Serialisable.__init__(self)
        self.src = kwargs.get('src')
        self.delay = kwargs.get('delay', 0)
        self.period = kwargs.get('period', 1)
        self.occurences = kwargs.get('occurences', 1)
        if 'expr' in kwargs:
            expr = kwargs['expr']
            if isinstance(expr, Expr):
                self.expr = expr
            else:
                self.expr = Parser.parse(expr)
        self.result = kwargs.get('result')
        pattern = kwargs.get('pattern')
        if pattern is not None and pattern in Effect.PATTERNS:
            pattern = Effect.PATTERNS[pattern]
            self.delay = pattern.get('delay', self.delay)
            self.occurences = pattern.get('occurences', self.occurences)
            self.period = pattern.get('period', self.period)

    def __str__(self) -> str:
        return str(self.serialise())

    def copy(self: ET) -> ET:
        """Document this."""
        return self.__class__(**self.serialise())

    def generate(self: ET, assign: types.assignment_t) -> ET:
        """Document this."""
        result = self.copy()
        if self.expr is not None:
            result.result = self.expr.eval(assign)
        return result

    def get_text(self) -> str:
        """Document this."""
        return ''

    def is_damage(self) -> bool:
        """Document this."""
        return False

    def is_done(self) -> bool:
        """Document this."""
        return self.occurences == 0

    def is_immediate(self) -> bool:
        """Check if self occurs now."""
        return self.occurences >= 1 and self.delay == 0

    def is_future(self) -> bool:
        """Check if self will occur in the future but not now."""
        return self.occurences > 1 or self.delay > 0

    def is_permanent(self) -> bool:
        """Check if self is permanent."""
        return (
            self.occurences > 1 and self.period == math.inf
            or
            self.occurences == 1 and self.delay == math.inf
        )

    def new_turn(self) -> None:
        """Document this."""
        if self.delay == 0:
            self.occurences -= 1
        self.delay = self.period - 1

    def realise(self, **kwargs: tp.Any) -> None:
        """Document this."""
        #  active effects on the being may affect this effect
        being = kwargs['being']
        for _, active in being.get_active_effects():
            active.affect(self)

    def unrealise(self, **kwargs: tp.Any) -> None:
        """Document this."""

    def affect(self, effect: types.effect_t) -> None:
        """Document this."""

    def serialise(self) -> types.serialisation_t:
        result = {
            'type': type(self).__name__,
            'delay': self.delay,
            'period': self.period,
            'occurences': self.occurences
        }
        if self.expr is not None:
            result['expr'] = str(self.expr)
        return result

    def get_exprs(self) -> tp.Iterator[types.expr_t]:
        """Iterate on the Expr appearing in self."""
        if self.expr is not None:
            yield self.expr

    @classmethod
    def unserialise(cls, data: types.serialisation_t) -> types.effect_t:
        from . import AttrModifier, Death, Learn, Protection, StatusChange
        return {
            'AttrModifier': AttrModifier,
            'Death': Death,
            'Learn': Learn,
            'Protection': Protection,
            'StatusChange': StatusChange
        }[data['type']](**data)

    ###########################################################################

    def gui_description(self) -> tp.Optional[str]:
        """Get a string for self that can be used to describe it in a tooltip.

        For examples:" "HP - 2D6", "Learn fireball".  Return None if
        the effect does not have a tooltip description.

        """
        return None

    def gui_realisation_description(
            self,
            assign: types.assignment_t
    ) -> tp.Optional[str]:
        """Get an str for self that describes a realisation of it.

        For examples: "bob loses 10 HP", "marta learns spell
        fireball".  Return None if the effect does not have a
        realisation description.

        """
        return None

    def gui_animation(
            self,
            being: types.being_t
    ) -> tp.Optional[types.pw.Animation]:
        """Get an animation for self."""
        return None

    @classmethod
    def gui_animation_effects(
            cls,
            effects: tp.Sequence[tp.Tuple[types.being_t, types.effect_t]]
    ) -> tp.Optional[types.pw.animations.AnimationSequence]:
        """Get an animation sequence for the effect sequence."""
        import pygwin as pw
        anims: tp.List[types.pw.Animation] = list()
        for being, effect in effects:
            if effect.is_immediate():
                anim = effect.gui_animation(being)
                if anim is not None:
                    anims.append(anim)
        if anims == list():
            return None
        return pw.animations.AnimationSequence(anims, glob.area_panel())
