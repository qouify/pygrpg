#!/usr/bin/env python3

"""Definition of class StatusChange."""

import typing as tp

from pygrpg.typing import types
from pygrpg.being import Status
from . import Effect, StatusChangeType


class StatusChange(Effect):
    """A StatusChange add/removes being's status points."""

    def __init__(self, **kwargs: tp.Any):
        self.ctype: types.status_change_type_t
        self.op: types.status_change_op_t
        self.status: types.status_t

        super().__init__(**kwargs)
        self.status = Status.get_(kwargs['status'])
        self.op = tp.cast(types.status_change_op_t, kwargs.get('op', 'rem'))
        if 'ctype' in kwargs and kwargs['ctype'] is not None:
            self.ctype = StatusChangeType.get_(kwargs['ctype'])

    def get_points(self) -> int:
        """Document this."""
        return int(self.result)

    def realise(self, **kwargs: tp.Any) -> None:
        super().realise(**kwargs)
        # @MATCH@
        if self.op == 'add':
            kwargs['being'].update_status(self.status, self.get_points())
            assert 0
        elif self.op == 'rem':
            kwargs['being'].update_status(self.status, - self.get_points())

    def serialise(self) -> types.serialisation_t:
        return {
            **super().serialise(),
            'status': self.status.id,
            'op': self.op,
            'ctype': None if self.ctype is None else self.ctype.id
        }

    ###########################################################################

    OP_DESCRIPTION = {
        'add': '+',
        'rem': '-'
    }

    def gui_description(self) -> tp.Optional[str]:
        txt_status = self.status.get_text_short_val()
        op = self.OP_DESCRIPTION[self.op]
        expr = self.expr.get_game_description()
        result = f'{txt_status} {op} {expr}'
        return result

    def gui_realisation_description(
            self,
            assign: types.assignment_t
    ) -> tp.Optional[str]:
        op = self.OP_DESCRIPTION[self.op]
        name = assign['being'].get_name()
        txt_status = self.status.get_text_short_val()
        return f'{name}: {txt_status}{op}{self.get_points()}'

    def gui_animation(
            self,
            being: types.being_t
    ) -> tp.Optional[types.pw.Animation]:
        """Get an animation for self."""
        from pygrpg.gui.animations.effect import StatusChangeAnimation
        if self.ctype is None:
            return None
        return StatusChangeAnimation(self, being)
