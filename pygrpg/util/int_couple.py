#!/usr/bin/env python3

"""Definition of class IntCouple."""

import typing as tp

from pygrpg.typing import types


class IntCouple:
    """Document this."""

    @classmethod
    def diff(
            cls,
            c1: types.int_couple_t,
            c2: types.int_couple_t
    ) -> types.int_couple_t:
        """Document this."""
        return c1[0] - c2[0], c1[1] - c2[1]

    @classmethod
    def sum(
            cls,
            c1: types.int_couple_t,
            c2: types.int_couple_t
    ) -> types.int_couple_t:
        """Document this."""
        return c1[0] + c2[0], c1[1] + c2[1]

    @classmethod
    def prod(
            cls,
            mult: tp.Union[float, int],
            c: types.int_couple_t
    ) -> types.int_couple_t:
        """Document this."""
        return int(c[0] * mult), int(c[1] * mult)
