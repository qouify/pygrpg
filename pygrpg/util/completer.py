#!/usr/bin/env python3

"""Definition of class Completer."""

from __future__ import annotations
import typing as tp

from pygrpg.typing import types


class Node:
    """Document this."""

    def __init__(
            self,
            name: str,
            parent: tp.Optional[Node] = None,
            data: tp.Any = None
    ):
        """Document this."""
        self.children: tp.List[Node]
        self.data: tp.Any
        self.name: str
        self.parent: Node

        self.name = name
        self.children = list()
        self.data = data
        if parent is not None:
            self.set_parent(parent)

    def set_parent(self, parent: Node) -> None:
        """Document this."""
        self.parent = parent
        parent.add_child(self)

    def add_child(self, child: Node) -> None:
        """Document this."""
        if child not in self.children:
            self.children.append(child)
        if child.parent != self:
            child.set_parent(self)

    def print_tree(self) -> None:
        """Document this."""
        def print_rec(node: Node, depth: int) -> None:
            print(depth * '  ' + node.name)
            for child in node.children:
                print_rec(child, depth + 1)
        print_rec(self, 0)


class Completer:
    """Document this."""

    def __init__(
            self,
            custom_completer: tp.Optional[types.custom_completer_t] = None
    ):
        """Document this."""
        self.root = Node('')
        self.custom_completer = custom_completer

    @classmethod
    def match(cls, node: str, string: str) -> bool:
        """Document this."""
        if node == '%int':
            try:
                int(string)
                return True
            except ValueError:
                return False
        if node == '%string':
            return string != ' '
        if node[0] == '%':
            return string != ' '
        return node == string

    @classmethod
    def _split(
            cls,
            sentence: str,
            empty: bool = False
    ) -> tp.Iterator[str]:
        word = ''
        start_string = None
        for letter in sentence:
            if start_string is not None:
                if letter == start_string:
                    yield word
                    start_string = None
                    word = ''
                else:
                    word += letter
            else:
                if letter in ['\'', '"']:
                    if empty or word != '':
                        yield word
                    start_string = letter
                    word = ''
                elif letter == ' ':
                    if empty or word != '':
                        yield word
                    word = ''
                else:
                    word += letter
        if empty or word != '':
            yield word

    def add_path(self, path: str, data: tp.Any) -> None:
        """Document this."""
        cur = self.root
        for word in Completer._split(path):
            child = next((c for c in cur.children if c.name == word), None)
            if child is not None:
                cur = child
            else:
                cur = Node(word, cur)
        cur.data = data

    def get_path_data(
            self,
            path: str
    ) -> tp.Optional[tp.Tuple[tp.Any, tp.List[str], bool]]:
        """Document this."""
        cur = self.root
        values = []
        for word in Completer._split(path):
            child = next(
                (c for c in cur.children if Completer.match(c.name, word)),
                None
            )
            if child is None:
                return None
            cur = child
            if child.name[0] == '%':
                values.append(word)
        assert cur is not None
        return cur.data, values, cur.data is None and len(cur.children) > 0

    def complete(self, sentence: str) -> tp.List[str]:
        """Document this."""
        def complete_propositions(
                prop: tp.List[str],
                last: str
        ) -> tp.List[str]:
            if len(prop) == 1:
                if prop[0] == last:
                    return [' ']
                return [prop[0][len(last):] + ' ']
            if len(prop) > 1:

                #  compute the shortest common prefix of all words
                shortest_prefix = prop[0]
                for word in prop:
                    pref = ''
                    for letter in shortest_prefix:
                        if not word.startswith(pref + letter):
                            break
                        pref += letter
                    shortest_prefix = pref
                if shortest_prefix not in ['', last]:
                    return [shortest_prefix[len(last):]]
            return prop
        cur = self.root
        word_list = list(Completer._split(sentence, empty=True))
        last = word_list[-1]
        for word in word_list[:-1]:
            next_child = next(
                (c for c in cur.children if Completer.match(c.name, word)),
                None
            )
            if next_child is None:
                return []
            cur = next_child
        prop = []
        for child in cur.children:
            if child.name == '%string':
                return []
            if child.name == '%int':
                return list('0123456789')
            if child.name[0] == '%':
                if self.custom_completer is None:
                    return []
                for word in list(self.custom_completer(child.name, last)):
                    if word.startswith(last):
                        prop.append(word)
            elif child.name.startswith(last):
                prop.append(child.name)
        return complete_propositions(prop, last)
