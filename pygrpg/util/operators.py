#!/usr/bin/env python3

"""Definition of class Operators."""

import typing as tp

import operator


class Operators:
    """Document this."""

    __COMP = {
        '<': operator.lt,
        '<=': operator.le,
        '==': operator.eq,
        '>=': operator.ge,
        '>': operator.gt,
        '+': operator.add,
        '-': operator.sub,
        '*': operator.mul,
        '/': operator.truediv
    }

    @classmethod
    def op(cls, left: tp.Any, op: str, right: tp.Any) -> tp.Any:
        """Document this."""
        return Operators.__COMP[op](left, right)
