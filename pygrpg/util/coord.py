#!/usr/bin/env python3

"""Definition of class Coord."""

import typing as tp

from pygrpg.typing import types


class Coord:
    """Document this."""

    @classmethod
    def points_inside(
            cls,
            pt1: types.point_t,
            pt2: types.point_t
    ) -> tp.Iterator[types.point_t]:
        """Document this."""
        x1, y1 = pt1
        x2, y2 = pt2
        top_left = x1 if x1 < x2 else x2, y1 if y1 < y2 else y2
        bottom_right = x1 if x1 > x2 else x2, y1 if y1 > y2 else y2
        x1, y1 = top_left
        x2, y2 = bottom_right
        for x in range(x1, x2 + 1):
            for y in range(y1, y2 + 1):
                yield (x, y)
