#!/usr/bin/env python3

"""Definition of class Directions."""

import math

from pygrpg.typing import types
from . import IntCouple


class Directions:
    """Document this."""

    NORTH = (0, -1)
    NORTH_EAST = (+1, -1)
    EAST = (+1, 0)
    SOUTH_EAST = (+1, +1)
    SOUTH = (0, +1)
    SOUTH_WEST = (-1, +1)
    WEST = (-1, 0)
    NORTH_WEST = (-1, -1)
    NONE = (0, 0)

    ADJACENT = [
        NORTH,
        EAST,
        SOUTH,
        WEST
    ]

    DISTANCE = {
        NORTH: 1,
        EAST: 1,
        SOUTH: 1,
        WEST: 1,
        NONE: 0,
        NORTH_EAST: math.sqrt(2),
        SOUTH_EAST: math.sqrt(2),
        NORTH_WEST: math.sqrt(2),
        SOUTH_WEST: math.sqrt(2)
    }

    @classmethod
    def move(
            cls,
            cell: types.cell_t,
            direction: types.direction_t
    ) -> types.cell_t:
        """Document this."""
        return IntCouple.sum(cell, direction)

    @classmethod
    def distance(cls, cell_a: types.cell_t, cell_b: types.cell_t) -> float:
        """Document this."""
        i, j = IntCouple.diff(cell_a, cell_b)
        return math.sqrt(abs(i) ** 2 + abs(j) ** 2)
