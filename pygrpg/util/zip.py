#!/usr/bin/env python3

"""Definition of class Zip."""

import os
import zipfile
import typing as tp


class Zip:
    """Class Zip contains helper methods to zip/unzip files."""

    @staticmethod
    def zip_directory(
            zip_file: str,
            dir_path: str,
            base: tp.Optional[str] = None
    ) -> None:
        """Zip directory dir_path in zip_file_path."""
        with zipfile.ZipFile(zip_file, 'w', zipfile.ZIP_DEFLATED) as zipf:
            for root, _, files in os.walk(dir_path):
                for f in files:
                    rel = os.path.relpath(os.path.join(root, f), dir_path)
                    src = os.path.join(root, f)
                    dst = os.path.join(
                        os.path.basename(dir_path) if base is None else base,
                        rel
                    )
                    zipf.write(src, dst)

    @staticmethod
    def unzip_file(zip_file: str, target_dir: str) -> None:
        """Unzip file zip_file in directory target_dir."""
        with zipfile.ZipFile(zip_file, 'r') as zipf:
            zipf.extractall(target_dir)
