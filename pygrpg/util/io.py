#!/usr/bin/env python3

"""Definition of class Io."""

import os
import shutil
import logging
import pathlib


class Io:
    """Class Io defines some basic IO primitives."""

    @staticmethod
    def mkpath_of_file(filepath: str) -> None:
        """Execute: mkdir -p $(dirname filepath)."""
        logging.info('mkpath_of_file(%s)', filepath)
        dirpath = os.path.dirname(filepath)
        pathlib.Path(dirpath).mkdir(parents=True, exist_ok=True)

    @staticmethod
    def mkdir_if_missing(dirpath: str) -> None:
        """Execute: [ ! -d dirpath ] && mkdir dirpath."""
        logging.info('mkdir_if_missing(%s)', dirpath)
        if not os.path.exists(dirpath):
            os.mkdir(dirpath)

    @staticmethod
    def rm(filepath: str) -> None:
        """Execute: rm filepath."""
        logging.info('rm(%s)', filepath)
        os.remove(filepath)

    @staticmethod
    def rmtree(dirpath: str) -> None:
        """Execute: rm -r dirpath."""
        logging.info('rmtree(%s)', dirpath)
        if os.path.exists(dirpath):
            shutil.rmtree(dirpath)

    @staticmethod
    def cp(src: str, dst: str) -> None:
        """Execute: cp src dst."""
        logging.info('cp(%s, %s)', src, dst)
        shutil.copyfile(src, dst)

    @staticmethod
    def cptree(src: str, dst: str) -> None:
        """Execute: cp -r src dst."""
        logging.info('cptree(%s, %s)', src, dst)
        if os.path.exists(src):
            shutil.copytree(src, dst)
