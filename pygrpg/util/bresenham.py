#!/usr/bin/env python3

"""Definition of bresenham function to compute points between two points.

The code is a slight modification from bresenham module.  It has been
copied here also to be typed.

"""

import typing as tp

from pygrpg.typing import types


class Bresenham:
    """Document this."""

    @classmethod
    def compute(
            cls,
            src: types.pos_t,
            dst: types.pos_t
    ) -> tp.Iterator[types.point_t]:
        """Yield integer coordinates on the line from src to dst.

        Input coordinates should be integers.

        The result will contain both the start and the end point.

        """
        x0, y0 = src
        x1, y1 = dst

        dx = x1 - x0
        dy = y1 - y0

        xsign = 1 if dx > 0 else -1
        ysign = 1 if dy > 0 else -1

        dx = abs(dx)
        dy = abs(dy)

        if dx > dy:
            xx, xy, yx, yy = xsign, 0, 0, ysign
        else:
            dx, dy = dy, dx
            xx, xy, yx, yy = 0, ysign, xsign, 0

        d = 2 * dy - dx
        y = 0

        for x in range(dx + 1):
            yield x0 + x * xx + y * yx, y0 + x * xy + y * yy
            if d >= 0:
                y += 1
                d -= 2 * dx
            d += 2 * dy
