#!/usr/bin/env python3

"""Declaration of class PreFightContext."""

import typing as tp

from pygrpg import glob
from pygrpg.area import Algo
from pygrpg.typing import types
from pygrpg.util import Directions
from pygrpg.config import GameConfig as Cfg
from pygrpg.rule import Rule
from . import FightContext, AreaContext


class PreFightContext(AreaContext):
    """A PreFightContext is a context that prepares a fight.

    The PreFightContext allows the player too choose starting
    positions of its heroes before a fight begins.

    """

    def __init__(
            self,
            area: types.area_t,
            enemy: types.monster_t,
            **kwargs: tp.Any
    ):
        self.current_hero: types.hero_t
        self.direction: types.direction_t
        self.done: bool
        self.enemy: types.monster_t
        self.error_cell: types.cell_t
        self.init_pos: tp.Dict[types.being_t, types.cell_t]
        self.possible_cells: tp.Set[types.cell_t]
        self.queue: tp.List[types.hero_t]
        self.selected_cell: types.cell_t
        self.valid_position: bool

        AreaContext.__init__(self, area, **kwargs)
        self.init_pos = dict()
        self.possible_cells = set()
        self.enemy = enemy
        self.queue = list()
        for hero in list(self.area.get_heroes()):
            self.queue.insert(0, hero)
            for fellow in hero.get_fellows():
                self.queue.insert(0, fellow)
        self.current_hero = self.queue.pop()
        self.direction = self.current_hero.direction
        self.done = False
        self.valid_position = False

    def open(self) -> None:
        from pygrpg.gui.animations.misc import MoveCameraAnimation

        AreaContext.open(self)

        heroes = list(self.area.get_heroes())
        for hero in heroes:
            cell = hero.cell
            self.area.del_object(hero)
            for h in hero.get_fellows():
                self.init_pos[h] = cell
        self._pick_next_hero_for_positioning()

        animation = MoveCameraAnimation(self.enemy.cell)
        animation.start()

    def close(self) -> None:
        AreaContext.close(self)
        glob.area_node().clear_selected()

    def _move_hero(self, move: types.direction_t) -> None:
        assert self.selected_cell is not None
        area = self.area
        new_cell = Directions.move(self.selected_cell, move)
        valid = (
            new_cell in self.possible_cells
            and area.get_blocking_object(new_cell) is None
        )
        if self.valid_position:
            area.del_object(self.current_hero)
        self.valid_position = valid
        if valid:
            self.current_hero.set_direction(self.direction)
            area.add_object(new_cell, self.current_hero)
        self._select_cell(new_cell)

    def _validate_position(self) -> bool:
        if self.valid_position:
            self._pick_next_hero_for_positioning()

            #  all heroes positioned => start the fight
            if self.done:
                glob.game().switch_context(self._fight_context())
            return True
        return False

    def on_key(self, pgevt: types.pg.event.Event) -> bool:
        import pygame as pg

        if self.done:
            glob.game().switch_context(self._fight_context())
            return True
        key = pgevt.key
        move_keys = Cfg.get_move_keys()
        if key in move_keys:
            self._move_hero(Cfg.move_action_to_direction(move_keys[key]))
            return True
        if key == pg.K_RETURN:
            return self._validate_position()
        return super().on_key(pgevt)

    def _position_enemies(self) -> None:
        cell = self.enemy.cell
        direction = self.enemy.direction
        self.area.del_object(self.enemy)
        for enemy in self.enemy.get_fellows():
            enemy_cell = Algo.find_closest_cell_around(
                cell, self._is_valid_cell_for_positioning
            )
            assert enemy_cell is not None
            self.area.add_object(enemy_cell, enemy)
            enemy.set_direction(direction)
            self.init_pos[enemy] = cell

    def _select_cell(self, cell: tp.Optional[types.cell_t]) -> None:
        from pygrpg.gui import Constants as GuiConstants
        if cell is None:
            del self.selected_cell
        else:
            self.selected_cell = cell
        if self.error_cell is not None:
            glob.area_node().unselect_cell(self.error_cell)
        if self.selected_cell is not None:
            if self.valid_position:
                del self.error_cell
            else:
                glob.area_node().select_cell(
                    self.selected_cell,
                    GuiConstants['color-target-error-cell']
                )
                self.error_cell = self.selected_cell
            glob.area_node().center_on_cell(self.selected_cell)

    def _select_area(self, cells: tp.Set[types.cell_t]) -> None:
        from pygrpg.gui import Constants as GuiConstants
        glob.area_node().clear_selected()
        self.possible_cells = cells
        for cell in cells:
            glob.area_node().select_cell(
                cell, GuiConstants['color-pre-fight-cell-selection']
            )

    def _pick_next_hero_for_positioning(self) -> None:
        def cells_available() -> tp.Set[types.cell_t]:
            assert hero is not None
            party_cell = self.init_pos[hero]
            enemy_cell = self.enemy.cell
            distance = Directions.distance(enemy_cell, party_cell)
            cells = set(
                Algo.accessible_cells(
                    self.area, party_cell,
                    int(Rule.get_('fight-max-distance-from-party').eval({}))
                )
            )
            return {
                cell for cell in cells
                if Directions.distance(cell, enemy_cell) >= distance
            }

        try:
            hero: tp.Optional[types.hero_t] = self.queue.pop()
        except IndexError:
            hero = None

        #  no more hero to position
        if hero is None:
            self._select_cell(None)
            self._select_area(set())
            self._position_enemies()
            self.done = True
            return

        #  pick the next hero to position and give him a position
        init_cell = self.init_pos[hero]
        around = cells_available()
        hero_cell = Algo.find_closest_cell_around(
            init_cell,
            lambda cell: self._is_valid_cell_for_positioning(cell)
            and cell in around
        )
        assert hero_cell is not None
        self.valid_position = True
        self.current_hero = hero
        self.current_hero.set_direction(self.direction)
        self.possible_cells = around
        self._select_area(self.possible_cells)
        self._select_cell(hero_cell)
        self.area.add_object(hero_cell, self.current_hero)

    def _is_valid_cell_for_positioning(self, cell: types.cell_t) -> bool:
        return (
            self.area.has_objects(cell)
            and self.area.get_blocking_object(cell) is None
        )

    def _fight_context(self) -> types.fight_context_t:
        return FightContext(self.area, self.init_pos)
