#!/usr/bin/env python3

"""Definition of class ExplorationContext."""

import typing as tp

from pygrpg import glob
from pygrpg.util import Directions
from pygrpg.typing import types
from pygrpg.action import Action
from pygrpg.rule import Rule
from pygrpg.config import GameConfig as Cfg
from . import AreaContext, PreFightContext


class ExplorationContext(AreaContext):
    """Document this."""

    def __init__(self, area: types.area_t, **kwargs: tp.Any):
        """Document this."""
        AreaContext.__init__(self, area, **kwargs)
        self.being = next(area.get_heroes())
        self.add_eyes(self.being)

    def open(self) -> None:
        AreaContext.open(self)
        self.set_active(self.being)
        self.update_visibility()

    def on_key(self, pgevt: types.pg.event.Event) -> bool:
        import pygame as pg
        area_node = glob.area_game_node()

        if pgevt.key == Cfg['key-pickup']:
            def closed(_: pg.event.Event) -> bool:
                inv = win.inventory
                if inv.is_empty():
                    assert item_obj is not None
                    ctx.area.del_object(item_obj, being.cell)
                glob.area_node().set_updated()
                return True
            from pygrpg.gui import PickupWindow
            being = self.active
            ctx = glob.area_ctx()
            cell = being.cell
            item_obj = ctx.area.get_item_object(cell)
            if item_obj is None:
                item_obj = ctx.area.add_item_object(cell)
            inv = item_obj.inventory
            win = PickupWindow('ground', being, inv)
            win.add_processor('on-close', closed)
            win.open()
            result = True

        elif not self.has_action():
            #  no current action
            result = Action.gui_init(pgevt)
        else:
            if pgevt.key != pg.K_ESCAPE:
                #  incomplete action
                result = self.action.gui_complete(pgevt)
            else:
                area_node.center_on_object(self.active)
                self.reset_action()
                self.reset_targets()
                result = True

        result = result or super().on_key(pgevt)
        if result:
            glob.area_node().set_updated()
        return result

    def update(self) -> bool:
        if self.has_action() and self.action.is_complete():
            self.action.do()
            self.reset_action()
            self._fight_begins()
            return True
        return False

    def _fight_begins(self) -> None:
        party_cell = self.active.cell

        #  check if an enemy sees the party that just moved and is
        #  close enough to engage fight.  if so, we switch to a
        #  pre-fight-context.  the visible cells of the current
        #  exploration-context are exported to this new context
        for monster in self.area.get_monsters():
            dist = min(
                int(Rule.get_('fight-start-distance').eval({})),
                monster.visibility
            )
            if (
                    Directions.distance(monster.cell, party_cell) <= dist and
                    self.area.is_visible(monster.cell, party_cell)
            ):
                ctx = PreFightContext(
                    self.area, monster,
                    visible=self.visible,
                    freeze_visibility=True
                )
                glob.game().switch_context(ctx)
                break
