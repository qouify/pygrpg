#!/usr/bin/env python3

"""Definition of class AreaContext."""

import typing as tp
import logging
import pygame as pg

from pygrpg import glob
from pygrpg.config import GameConfig as Cfg
from pygrpg.area import Algo
from pygrpg.typing import types
from . import Context


class AreaContext(Context):
    """Document this."""

    action_area: int
    action: types.action_t
    active: types.being_t
    area: types.area_t
    eyes: tp.Set[types.being_t] = set()
    freeze_visibility: bool
    targetable: tp.Set[types.cell_t]
    target_area: tp.Set[types.cell_t]
    target: types.cell_t
    visible: tp.Set[types.cell_t]

    def __init__(self, area: types.area_t, **kwargs: tp.Any):
        """Document this."""
        Context.__init__(self)
        self.area = area
        self.visible = kwargs.get('visible', set())
        self.freeze_visibility = kwargs.get('freeze_visibility', False)
        self.eyes = set()
        self.targetable = set()
        self.target_area = set()
        self.action_area = 0

    def has_active(self) -> bool:
        """Check if self currently has an active being."""
        return hasattr(self, 'active')

    def has_action(self) -> bool:
        """Check if an action is currently being initiated for self."""
        return hasattr(self, 'action')

    def reset_action(self) -> None:
        """Reset the action being initiated for self."""
        if hasattr(self, 'action'):
            del self.action

    def add_eyes(self, eyes: types.being_t) -> None:
        """Add eyes on self."""
        self.eyes.add(eyes)

    def del_eyes(self, eyes: types.being_t) -> None:
        """Remove some eyes from self."""
        self.eyes.remove(eyes)

    def set_targetable(self, targetable: tp.Set[types.cell_t]) -> None:
        """Set self's targetable cells."""
        from pygrpg.gui import Constants
        self.targetable = targetable
        for cell in targetable:
            glob.area_node().select_cell(
                cell, Constants['color-targetable-cell']
            )

    def set_area(self, area: types.area_t) -> None:
        """Document this."""
        if self.area != area:
            self.area = area
            glob.area_node().set_area(area)

    def open(self) -> None:
        Context.open(self)
        glob.area_node().visibility = self.is_cell_visible
        glob.area_node().known = self.is_cell_known
        glob.area_node().set_updated()
        self.update_visibility()

    def close(self) -> None:
        Context.close(self)
        glob.area_node().set_updated()

    def set_active(
            self,
            being: tp.Optional[types.being_t],
            center: bool = True
    ) -> None:
        """Set self's active being."""
        if being is not None:
            logging.info('%s becomes active', being.get_name())
        if not self.has_active() or being != self.active:
            if being is None:
                del self.active
            else:
                self.active = being
            if center and being is not None:
                glob.area_node().center_on_object(being)

    def is_cell_visible(self, cell: types.cell_t) -> bool:
        """Check if cell is visible by the party."""
        return cell in self.visible

    def is_cell_known(self, cell: types.cell_t) -> bool:
        """Check if cell is know by the party."""
        return glob.game().is_cell_known(cell)

    def is_target_valid(self) -> bool:
        """Check if self's current target is in self's targetable cells set."""
        return self.target in self.targetable

    def reset_targets(self) -> None:
        """Reset all self's target."""
        del self.target
        self.targetable = set()
        self.target_area = set()
        self.action_area = 0
        glob.area_node().clear_selected()

    def set_target(self, target: tp.Optional[types.cell_t]) -> None:
        """Document this."""
        from pygrpg.gui import Constants
        for cell in self.target_area:
            glob.area_node().unselect_cell(cell)

        #  if no target set, target becomes the active object cell
        if target is None:
            target = self.active.cell
        self.target = target

        color: types.color_t
        if target in self.targetable:
            color = Constants['color-target-cell']
            self.target_area = set(
                Algo.cells_around(target, (0, self.action_area))
            )
        else:
            color = Constants['color-target-error-cell']
            self.target_area = {target}

        for cell in self.target_area:
            glob.area_node().select_cell(cell, color)

    def update_visibility(self) -> None:
        """Update the visibility of the area.

        Visible cells become those that can be seen by all eyes.

        """
        def check(other_cell: types.cell_t) -> bool:
            return self.area.is_visible(cell, other_cell)
        if self.freeze_visibility:
            return
        self.visible = set()
        for being in self.eyes:
            cell = being.cell
            visibility = being.visibility
            self.visible = self.visible.union(
                set(Algo.cells_around(cell, (0, visibility), check=check))
            )
        for cell in self.visible:
            glob.game().add_known_cell(cell)

    def move_being(
            self,
            being: types.being_t,
            move: types.direction_t
    ) -> None:
        """Move being on the area."""
        self.area.move_being(being, move)
        being.set_direction(move)

    def on_key(self, pgevt: pg.event.Event) -> bool:
        """Process the key event pgevt.

        Return True if the event has been processed, False otherwise.

        """
        key = pgevt.key

        #  open the hero window
        if key == Cfg['key-char']:
            act = self.active
            if act is not None and not glob.main().window_opened():
                hero = tp.cast(types.hero_t, act)
                glob.main_game().open_hero_window(hero)

        #  center on the active being
        elif key == Cfg['key-center-axis']:
            if self.active is not None:
                glob.area_node().center_on_object(self.active)

        else:
            return False
        return True

    def update(self) -> bool:
        """Document this."""
        return False
