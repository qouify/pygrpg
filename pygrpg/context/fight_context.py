#!/usr/bin/env python3

"""Definition of class FightContext."""

import logging
import typing as tp

from pygrpg import glob
from pygrpg.area import Algo
from pygrpg.rule import Rule
from pygrpg.ai import Ai
from pygrpg.effect import Effect, Death
from pygrpg.typing import types
from pygrpg.being import Hero, Monster
from pygrpg.action import Action
from . import AreaContext


class FightContext(AreaContext):
    """Document this."""

    def __init__(
            self,
            area: types.area_t,
            beings: tp.Dict[types.being_t, types.pos_t],
            **kwargs: tp.Any
    ):
        """Document this."""
        self.aps: tp.Dict[types.being_t, int]
        self.beings: tp.Dict[types.being_t, types.pos_t]
        self.dead: tp.Set[types.being_t]
        self.delayed: tp.List[tp.Tuple[types.being_t, types.action_t, int]]
        self.pending: tp.List[tp.Tuple[types.being_t, types.action_t]]
        self.queue: tp.List[types.being_t]
        self.rescheduled: bool
        self.turn_ended: bool
        self.turn: int
        self.wait_done: bool
        self.wait: tp.List[types.being_t]
        self.xp: types.fight_xps_t

        super().__init__(area, **kwargs)
        self.wait_done = False
        self.turn_ended = False
        self.rescheduled = False
        self.delayed = list()
        self.pending = list()
        self.queue = list()
        self.wait = list()
        self.dead = set()
        self.beings = beings
        self.turn = 0
        self.aps = {b: 0 for b in beings}
        self.xp = {b: dict() for b in self.aps if isinstance(b, Hero)}
        for hero in self.xp:
            self.add_eyes(hero)

    def get_monsters(self) -> tp.List[types.monster_t]:
        """Get self's (living) monsters."""
        return [b for b in self.aps if isinstance(b, Monster)]

    def get_heroes(self) -> tp.List[types.hero_t]:
        """Get self's (living) heroes."""
        return [b for b in self.aps if isinstance(b, Hero)]

    def open(self) -> None:
        super().open()
        glob.main().win_sys.frozen = True
        self._next_turn()

    def is_saveable(self) -> bool:
        return False

    def on_key(self, pgevt: types.pg.event.Event) -> bool:
        import pygame as pg
        if self.action is None:
            result = Action.gui_init(pgevt)
        else:
            if pgevt.key != pg.K_ESCAPE:
                result = self.action.gui_complete(pgevt)
            else:
                if self.rescheduled:
                    self._action_rescheduled_cancelled()
                else:
                    self._reset_action()
                result = True
        result = result or super().on_key(pgevt)
        if result:
            glob.area_node().set_updated()
        return result

    def update(self) -> bool:
        act = self.action
        if act is not None:
            if not self.rescheduled and act.has_delay():
                self._action_delayed()
                return True
            if act.is_complete():
                self._action_validated(act)
                return True
        return False

    def set_attack_target_cells(self, being: types.being_t) -> None:
        """Document this."""
        # self._set_target_cells(being, being.get_weapon().range)
        rng = being.get_weapon().attack.rng.eval({'being': being})
        self._set_target_cells(being, rng)

    def set_spell_target_cells(
            self,
            being: types.being_t,
            spell: types.spell_t
    ) -> None:
        """Document this."""
        area = int(spell.area.eval({'being': being}))
        rng = spell.rng.eval({'being': being})
        self.action_area = area
        self._set_target_cells(being, rng)

    def push_waiting(self, being: types.being_t) -> None:
        """Document this."""
        self.wait.insert(0, being)

    def on_death(self, dead: types.being_t) -> None:
        """Method that must be called on self when beind dead dies."""
        del self.aps[dead]
        self.dead.add(dead)
        self.queue = [o for o in self.queue if o != dead]
        self.wait = [o for o in self.wait if o != dead]
        if isinstance(dead, Hero):
            self.del_eyes(dead)
            self.update_visibility()

    def _action_delayed(self) -> None:
        active = self.active
        logging.info('pend action of %s', active.get_name())
        delay = (active, self.action, self.action.get_delay())
        active.set_busy(True)
        self.delayed.append(delay)
        self.queue = self.queue[1:]
        self._reset_action()
        self._next_being()

    def _action_rescheduled_cancelled(self) -> None:
        self.action.cancel()
        self._reset_action()
        self._next_being()

    def _action_validated(self, act: types.action_t) -> None:

        #  function called when all animations are done => realise the
        #  action
        def effects_animation_done() -> None:
            if act.success:
                act.do()

            #  the action may have caused deaths
            self._check_deaths()

            #  if the action has been done by a player hero => gain xp
            being = act.being
            skill = act.get_skill()
            if isinstance(being, Hero) and skill is not None:
                xp = act.get_xp()
                if xp > 0:
                    if skill not in self.xp[being]:
                        self.xp[being][skill] = 0
                    self.xp[being][skill] += xp

            #  remove the being that acted from the action queue and
            #  decrease its APs.  an action that has been rescheduled
            #  is not in the queue and the APs of the corresponding
            #  character have already been decreased
            if not self.rescheduled:
                self.queue = self.queue[1:]
                if not act.is_wait():
                    active = self.active
                    self.aps[active] -= self._ap_for_action()
                    if self.aps[active] >= self._ap_for_action():
                        self.queue.append(active)

            self._reset_action()
            if not self._check_fight_over():
                self._next_being()

            #  gui stuff
            glob.area_game_node().enable_cursor()

        #  function called when action animation is done => launch
        #  action effects animations
        def action_animation_done() -> None:
            anim = Effect.gui_animation_effects(act.effects)
            if anim is None:
                effects_animation_done()
            else:
                anim.callback = effects_animation_done
                anim.start()

        #  evaluate action success and compute its effects
        act.evaluate()
        if act.success:
            act.compute()

        main = glob.main()
        descr = act.gui_description()
        if descr is not None:
            main.push_alert(descr)
        animation = act.gui_animation()
        self.reset_targets()
        if animation is None:
            action_animation_done()
        else:
            glob.area_game_node().disable_cursor()
            animation.callback = action_animation_done
            main.win_sys.frozen = True
            animation.start()

    def _set_target_cells(
            self,
            being: types.being_t,
            rng: tp.Tuple[tp.Union[float, int], tp.Union[float, int]]
    ) -> None:
        def is_targetable(c: types.cell_t) -> bool:
            return (
                self.area.is_visible(being.cell, c)
                and (
                    self.area.get_blocking_object(c) is None
                    or self.area.get_being(c) is not None
                )
            )
        rng_min, rng_max = rng
        rng_min = max(rng_min, being.visibility)
        rng_max = min(rng_max, being.visibility)
        cells = set(Algo.cells_around(being.cell, rng, check=is_targetable))
        self.set_targetable(cells)
        self.set_target(None)

    def _ap_for_action(self) -> int:
        return int(Rule.get_('#rule-fight-aps-to-act').eval({}))

    def _ap_per_turn(self, being: types.being_t) -> int:
        assign = {'being': being}
        return int(Rule.get_('#rule-fight-aps-per-turn').eval(assign))

    def _fight_over(self, won: bool) -> None:

        assert False

        #  fight lost => game ends
        # if not won:
        #     glob.main_game().end_game()
        #     return

        #  remove beings from the area and put back groups
        # for being in self.aps:
        #     self.area.del_object(being)
        # for grp in self.groups:
        #     self.area.add_object(self.groups[grp], grp)

        # for grp in self.groups:

        #  remove dead beings from the group and put their
        #  equipment on the floor
        #    for dead in [d for d in self.dead if grp.is_in(d)]:
        #        grp.remove(dead)
        #        equip = dead.equipment
        #        for slot in equip:
        #            self.area.put_item(self.groups[grp], equip[slot])

        #  group has become empty => remove it from the area and
        #  put its inventory on the floor
        #    if grp.is_empty():
        #        self.area.del_object(grp)
        #        inv = grp.inventory
        #        self.area.put_inventory(self.groups[grp], inv)

        #  switch back to an exploration context
        # from . import ExplorationContext
        # glob.game().switch_context(ExplorationContext(self.area))

    def _next_turn(self) -> None:

        self.turn += 1
        logging.info('turn %s starts', self.turn)

        #  check for active effects on beings that may now occur
        effects = [
            eff.generate({'being': being})
            for being in self.aps
            for eff in being.new_turn()
        ]
        for eff in effects:
            eff.realise(area=self.area)

        #  effects that occured may have caused deaths
        self._check_deaths()

        #  check for delayed actions
        self.delayed = [(b, a, d - 1) for (b, a, d) in self.delayed]
        now = [bad for bad in self.delayed if bad[2] == 0]
        self.delayed = [d for d in self.delayed if d not in now]
        self.pending = []
        for being, action, _ in now:
            being.set_busy(False)
            self.aps[being] -= self._ap_for_action()
            self.pending.append((being, action))

        #  every non busy character gain a number of APs defined by
        #  the rules.  those that have enough aps to act are put in
        #  the queue
        self.wait_done = False
        for being in filter(lambda b: not b.busy, self.aps):
            aps = self._ap_per_turn(being)
            aps_now = self.aps[being]
            logging.info(
                '%s gains %s aps (%s => %s)',
                being.get_name(), aps, aps_now, aps_now + aps
            )
            self.aps[being] += aps
            if self.aps[being] >= self._ap_for_action():
                self.queue.append(being)

        #  move to the next (first) char
        self._next_being()

    def _next_being(self) -> None:
        from pygrpg.gui.animations.misc import MoveCameraAnimation

        self.rescheduled = False

        #  there is some pending action
        if self.pending != []:
            self.turn_ended = False
            being, action = self.pending[0]
            self.set_active(being)
            self.pending = self.pending[1:]
            self.rescheduled = True
            self.action = action
            self.action.gui_reschedule()
            logging.info('take pending action of %s', being.get_name())
            return

        #  no more beings in the queue.  if some have waited we put
        #  them back in the queue.  otherwise the turn ends
        if self.queue == []:
            if self.wait != []:
                self.queue = self.wait
                self.wait = []
                self.wait_done = True

        #  there are still some beings in the queue
        if self.queue != []:
            self.turn_ended = False
            self.queue.sort(key=lambda being: -self.aps[being])
            being = self.queue[0]

            def animation_done() -> None:
                self.set_active(being)
                if isinstance(being, Monster):
                    act = Ai.enemy_action_choice(being, self.area)
                    self._action_validated(act)
                else:
                    glob.main().win_sys.frozen = False
            animation = MoveCameraAnimation(
                being.cell,
                back=False
            )
            animation.callback = animation_done
            animation.start()
            return

        #  if we're here then there's no more being that can act in
        #  this turn => turn ends
        logging.info('turn %s ends', self.turn)
        self.set_active(None)
        self.turn_ended = True
        self._next_turn()

    def _check_deaths(self) -> None:
        #  generate a Death effect for dead beings
        new_dead = [being for being in self.aps if being.is_dead()]
        for dead in new_dead:
            eff = Death()
            eff.realise(being=dead, area=self.area)

    def _check_fight_over(self) -> bool:
        from pygrpg.gui import FightReviewWindow

        #  fight over if all beings in aps are either cpu or either
        #  player beings
        result = (
            all(not isinstance(being, Monster) for being in self.aps) or
            all(not isinstance(being, Hero) for being in self.aps)
        )

        #  the fight is over => compute xp gained by heroes and open
        #  the fight-review window.  when this one is closed we call
        #  self._fight_over
        if result:
            def close(_: types.pg.event.Event) -> bool:
                self._fight_over(won)
                return True
            if self.aps == dict():
                won = True
            else:
                won = isinstance(next(being for being in self.aps), Hero)
            lvl = dict()
            for hero in self.xp:
                lvl[hero] = hero.gain_xp(self.xp[hero])
            win = FightReviewWindow(won, self.xp, lvl)
            win.add_processor('on-close', close)
            win.open()

        return result

    def _reset_action(self) -> None:
        glob.main().area_panel.reset_selection_list()
        if self.active is not None:
            glob.area_node().center_on_object(self.active)
        self.reset_action()
        self.reset_targets()
