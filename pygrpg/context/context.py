#!/usr/bin/env python3

"""Definition of class Context."""

import logging


class Context:
    """Document this."""

    def is_saveable(self) -> bool:
        """Check if game can be saved in this context."""
        return True

    def open(self) -> None:
        """Open self."""
        logging.info('opening context %s', self)

    def close(self) -> None:
        """Close self."""
        logging.info('closing context %s', self)
