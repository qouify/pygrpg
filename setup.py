#!/usr/bin/env python3

"""Setup script for pygrpg."""

import os
import typing
from setuptools import setup, find_packages

import pygrpg


def package_files(directory: str) -> typing.Iterator[str]:
    """Get a list of all files in directory (and all its sub directories)."""
    for path, directories, filenames in os.walk(directory):
        for filename in filenames:
            yield os.path.join('..', path, filename)


setup(
    name='pygrpg',
    version=pygrpg.VERSION,
    description='Pygame RPG',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    classifiers=[
        'Programming Language :: Python',
        'Operating System :: POSIX :: Linux',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)'
    ],
    packages=find_packages('.'),
    package_dir={'': '.'},
    scripts=[
        'scripts/pygrpg'
    ],
    install_requires=[
        'pygame',
        'pygwin'
    ],
    package_data={
        'pygrpg': list(package_files(os.path.join('pygrpg', 'template')))
    }
)
