# Coding rules

* 80 chars max per line

* all class attributes are private

* for imports in the beginning of a file: always put imports of system
  package first, then a blank line, then imports of my packages

* unused variables should always be called '_'.  for example when
  getting items from a tuple if you don't care of the last item :
  a, b, c, _ = myTuple

* run tools/clean-code and tools/check-code before pushing updates